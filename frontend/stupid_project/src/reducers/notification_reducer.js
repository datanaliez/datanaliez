const INITIAL_STATE = {
  notification_list: [],
  loading: false,
  error:false,
  err_msg:""
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'FETCH_NOTIFICATION_PENDING':
      return { ...state, loading: true }
    case 'FETCH_NOTIFICATION_FULFILLED':
      return {...state, loading: false, error: false, notification_list:action.payload.data}
    case 'FETCH_NOTIFICATION_REJECTED':
      return {...state, err_msg:"Can't fetch notification, try again.", error: true, loading:false}
    default:
      return state
  }
}
