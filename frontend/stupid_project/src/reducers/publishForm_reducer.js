const INITIAL_STATE = {
  loadingState:[],
  error:false,
  err_msg:"",
  currentFormInfo:null,
  isSubmitCompleted:false,
}

export default (state = INITIAL_STATE, action) => {
  const pendingPATTERN = new RegExp(".*FORM.*_PENDING$")
  const successPATTERN = new RegExp(".*FORM.*_FULFILLED$")
  const failurePATTERN = new RegExp(".*FORM.*_REJECTED$")
  var nextState = state
  if(pendingPATTERN.test(action.type)){
    nextState.loadingState = state.loadingState.concat(action.type.replace(/_PENDING$/,"")).map((item)=>(item))
    nextState.error = false
    nextState.err_msg = ""
  }
  if(successPATTERN.test(action.type)){
    nextState.loadingState = state.loadingState.delete(action.type.replace(/_FULFILLED$/,"")).map((item)=>(item))
    nextState.error = false
    nextState.err_msg = ""
  }
  if(failurePATTERN.test(action.type)){
    nextState.loadingState = state.loadingState.delete(action.type.replace(/_REJECTED$/,"")).map((item)=>(item))
    nextState.error = true
    nextState.err_msg = `Can't ${action.type.replace(/_REJECTED$/,"")}, please try again.`
  }
  switch (action.type) {
    case "FETCH_FORM_INFO_FULFILLED":
      return {...nextState, currentFormInfo:action.payload.data.result}
    case "SUBMIT_FORM_FULFILLED":
      return {...nextState, isSubmitCompleted:action.payload.data.success}
    default:
      return {...nextState}
  }
}
