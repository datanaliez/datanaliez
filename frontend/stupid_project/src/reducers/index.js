import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'
import { routerReducer } from 'react-router-redux'
import applicationReducer from './application_reducer'
import notificationReducer from './notification_reducer'
import adminReducer from './admin_reducer'
import centralDataReducer from './centralData_reducer'
import overviewReducer from './overview_reducer'
import analyzeReducer from './analyze_reducer'
import formControlReducer from './formControll_reducer'
import publishFormReducer from './publishForm_reducer'
export default combineReducers({
  routing:        routerReducer,
  form:           formReducer,
  application:    applicationReducer,
  notification:   notificationReducer,
  administration: adminReducer,
  centralData: centralDataReducer,
  dataOverview: overviewReducer,
  dataAnalyze: analyzeReducer,
  formControll: formControlReducer,
  publishForm: publishFormReducer,
})
