import Moment from "moment";
const INITIAL_STATE = {
  TableForm:[],
  newFormID:"",
  loadingState:[],
  error:false,
  err_msg:"",
  currentFormInfo:{}
}

export default (state = INITIAL_STATE, action) => {
  const pendingPATTERN = new RegExp(".*FORM.*_PENDING$")
  const successPATTERN = new RegExp(".*FORM.*_FULFILLED$")
  const failurePATTERN = new RegExp(".*FORM.*_REJECTED$")
  var nextState = state
  if(pendingPATTERN.test(action.type)){
    nextState.loadingState = state.loadingState.concat(action.type.replace(/_PENDING$/,"")).map((item)=>(item))
    nextState.error = false
    nextState.err_msg = ""
  }
  if(successPATTERN.test(action.type)){
    nextState.loadingState = state.loadingState.delete(action.type.replace(/_FULFILLED$/,"")).map((item)=>(item))
    nextState.error = false
    nextState.err_msg = ""
  }
  if(failurePATTERN.test(action.type)){
    nextState.loadingState = state.loadingState.delete(action.type.replace(/_REJECTED$/,"")).map((item)=>(item))
    nextState.error = true
    nextState.err_msg = `Can't ${action.type.replace(/_REJECTED$/,"")}, try again.`
  }
  switch (action.type) {
    case "FETCH_FORM_IN_TABLE_FULFILLED":
      return {...nextState, TableForm:action.payload.data.result || []}
    case "CREATE_FORM_FULFILLED":
      return {...nextState, newFormID:action.payload.data.formID}
    case "SWITCH_FORM_FULFILLED":
      return action.payload.data.status == "success"? ({...nextState, currentFormInfo:{...state.currentFormInfo, disabled:action.payload.data.formDisabled}}):
      ({...nextState, error: true, err_msg:"Switching Form failed"})
    case "FETCH_FORM_INFO_FULFILLED":
      const formInfo = action.payload.data.result
      const tranformFormInfo = {...formInfo, autoClose:{
        includeDuration:formInfo.closeOn.datetime !== undefined,
        includeRegistLimit: formInfo.closeOn.registered !== undefined,
        duration:formInfo.closeOn.datetime?{
          start:Moment(formInfo.closeOn.datetime[0]),
          stop:Moment(formInfo.closeOn.datetime[1]),
        }:undefined,
        registLimit:formInfo.closeOn.registered || undefined
      },fieldList:formInfo.fieldList.map((field)=>(["checkbox","choice"].includes(field.type)?({...field, option:field.option.map((opt)=>(opt.value))}):field))}
      return {...nextState, currentFormInfo:tranformFormInfo}
    default:
      return {...nextState}
  }
}
