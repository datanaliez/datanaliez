const INITIAL_STATE = {
  currentPage: 1,
  sortby: "",
  orderby: "",
  mustUpdateData:false
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'SET_TABLEPROP':
      return { ...state, ...action.payload, mustUpdateData: true }
    case 'OVERVIEW_UPDATED_DATA':
      return {...state, mustUpdateData: false}
    case 'MUST_UPDATE_DATA':
      return {...state, mustUpdateData: action.payload, currentPage:1}
    case 'FETCH_TABLELIST_PENDING': // Table changed
      return {...INITIAL_STATE, mustUpdateData:false}
    default:
      return {...state}
  }
}
