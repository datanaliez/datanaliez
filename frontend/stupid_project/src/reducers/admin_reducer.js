const INITIAL_STATE = {
  notification_list: [],
  loading: false,
  error:false,
  err_msg:""
}
export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'DELETE_NOTIFICATION_FULFILLED':
      return {...state, loading: false, error: false}
    case 'DELETE_NOTIFICATION_REJECTED':
      return {...state, err_msg:`Can't ${action.type.replace("REJECTED","")}, try again.`, error: true, loading:false}
    case 'DELETE_NOTIFICATION_PENDING':
      return { ...state, loading: true }
    case 'EDIT_NOTIFICATION_FULFILLED':
      return {...state, loading: false, error: false}
    case 'EDIT_NOTIFICATION_REJECTED':
      return {...state, err_msg:`Can't ${action.type.replace("REJECTED","")}, try again.`, error: true, loading:false}
    case 'EDIT_NOTIFICATION_PENDING':
      return { ...state, loading: true }
    case 'NEW_NOTIFICATION_FULFILLED':
      return {...state, loading: false, error: false}
    case 'NEW_NOTIFICATION_REJECTED':
      return {...state, err_msg:`Can't ${action.type.replace("REJECTED","")}, try again.`, error: true, loading:false}
    case 'NEW_NOTIFICATION_PENDING':
      return { ...state, loading: true }
/* --------------------------------- USER REDUCER ---------------------------------*/
    case 'FETCH_USER_PENDING':
      return { ...state, loading: true }
    case 'FETCH_USER_FULFILLED':
      return {...state, loading: false, error: false, user_list:action.payload.data}
    case 'FETCH_USER_REJECTED':
      return {...state, err_msg:"Can't fetch notification, try again.", error: true, loading:false}
    case 'DELETE_USER_FULFILLED':
      return {...state, loading: false, error: false}
    case 'DELETE_USER_REJECTED':
      return {...state, err_msg:`Can't ${action.type.replace("REJECTED","")}, try again.`, error: true, loading:false}
    case 'DELETE_USER_PENDING':
      return { ...state, loading: true }
    case 'EDIT_USER_FULFILLED':
      return {...state, loading: false, error: false}
    case 'EDIT_USER_REJECTED':
      return {...state, err_msg:`Can't ${action.type.replace("REJECTED","")}, try again.`, error: true, loading:false}
    case 'EDIT_USER_PENDING':
      return { ...state, loading: true }
    case 'NEW_USER_FULFILLED':
      return {...state, loading: false, error: false}
    case 'NEW_USER_REJECTED':
      return {...state, err_msg:`Can't ${action.type.replace("REJECTED","")}, try again.`, error: true, loading:false}
    case 'NEW_USER_PENDING':
      return { ...state, loading: true }
    default:
      return state
  }
}
