const INITIAL_STATE = {
  currentUsername:"",
  currentPermission:"",
  LoggedIn: false,
  loading: false,
  challenge:"",
  error:false,
  err_msg:""
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'LOGIN_PENDING':
      return { ...state, loading: true, LoggedIn: false }
    case 'LOGIN_FULFILLED':
      return action.payload.data.permission == -1 ? {...state, err_msg:"Login failed.", error: true, loading:false, LoggedIn: false} : {...state, LoggedIn: true, loading: false, error: false, currentPermission: action.payload.data.permission, currentUsername: action.payload.data.username}
    case 'LOGIN_REJECTED':
      return {...state, err_msg:"Can't connect to server, try again.", error: true, loading:false}
    case 'LOGIN_STATUS_PENDING':
      return { ...state, loading: true }
    case 'LOGIN_STATUS_FULFILLED':
      return action.payload.data.permission === null ? {...state, err_msg:"Not logged in.", error: true, loading:false, LoggedIn: false, currentUsername:"", currentPermission:""} :
      {...state, LoggedIn: true, loading: false, error: false, currentPermission: action.payload.data.permission, currentUsername: action.payload.data.username}
    case 'LOGIN_STATUS_REJECTED':
      return {...state, err_msg:"Can't connect to server, try again.", error: true, loading:false}
      case 'LOGOUT_PENDING':
      return { ...state, loading: true, LoggedIn: true }
    case 'LOGOUT_FULFILLED':
      return {...state, LoggedIn: false, loading: false, error: false, currentPermission: "", currentUsername: ""}
    case 'LOGOUT_REJECTED':
      return {...state, err_msg:"Can't connect to server, try again.", error: true, loading:false}
    case 'GREETING_PENDING':
      return {...state, loading: true}
    case 'GREETING_FULFILLED':
      return {...state, challenge: action.payload.data.challenge, loading: false}
    case 'GREETING_REJECTED':
      return {...state, err_msg:"Can't greeting server, try again.", error: true, loading:false}
    default:
      return state
  }
}
