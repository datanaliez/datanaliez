const INITIAL_STATE = {
  columnName:"",
  minBound:0,
  interval:0,
  displayChartType:["spline","column"],
  mustUpdateData:false
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'ANALYZE_UPDATED_DATA':
      return {...state, mustUpdateData: false}
    case 'SAVE_ANALYZE_PARAM_STATE':
      return {...state, ...action.payload}
    case 'MUST_UPDATE_DATA':
      return {...state, mustUpdateData: action.payload}
    case 'FETCH_TABLELIST_PENDING': // Table changed
    return {...INITIAL_STATE, mustUpdateData:false}
    default:
      return {...state}
  }
}
