import { decamelize, camelize } from 'humps'
import FileDownload from "js-file-download";
const INITIAL_STATE = {
  tableList: [],
  loading: true,
  error:false,
  err_msg:"",
  data:{},
  histogramData:[],
  allPossibleColumn:[],
  columns:[],
  dataCount:0,
  isTableExist:false,
  existLoading: false,
  currentColumnType:undefined,
  loadingState:[]
}

Array.prototype.delete = function (targetItem) {
  var isDeleted = false
  return this.filter((item)=>{
	const result = isDeleted || (item != targetItem)
    isDeleted = isDeleted || (item == targetItem)
    return result
  })
}

export default (state = INITIAL_STATE, action) => {
  const pendingPATTERN = new RegExp("_PENDING$")
  const completePATTERN = new RegExp("_FULFILLED$|_REJECTED$")
  var nextState = state
  if(pendingPATTERN.test(action.type)){
    nextState.loadingState = state.loadingState.concat(action.type.replace(pendingPATTERN,"")).map((item)=>(item))
  }
  if(completePATTERN.test(action.type)){
    nextState.loadingState = state.loadingState.delete(action.type.replace(completePATTERN,"")).map((item)=>(item))
  }
  switch (action.type) {
    case 'MUST_UPDATE_DATA':
    return {...nextState, data:{}, histogramData:[]}
    case 'FETCH_TABLELIST_PENDING':
      return { ...nextState, loading: true}
    case 'FETCH_TABLELIST_FULFILLED':
      return {...nextState, loading: false, error: false, tableList:action.payload.data.tableList || []}
    case 'FETCH_TABLELIST_REJECTED':
      return {...nextState, err_msg:"Can't fetch table list, try again.", error: true, loading:false}
    case 'FETCH_COLUMN_NAME_IN_TABLE_PENDING':
      return { ...nextState, loading: true }
    case 'FETCH_COLUMN_NAME_IN_TABLE_FULFILLED':
      return {...nextState,
        loading: false,
        error: false,
        allPossibleColumn:action.payload.data.result ? action.payload.data.result.sort() : []}
    case 'FETCH_COLUMN_NAME_IN_TABLE_REJECTED':
      return {...nextState, err_msg:"Can't FETCH_COLUMN_NAME_IN_TABLE, try again.", error: true, loading:false}
    case 'FETCH_DATA_IN_TABLE_PENDING':
      return { ...nextState, loading: true }
    case 'FETCH_DATA_IN_TABLE_FULFILLED':
      return {...nextState,
        loading: false,
        error: false,
        data:action.payload.data.body || [],
        columns:action.payload.data.body ? action.payload.data.body.map((obj)=>Object.keys(obj)).reduce((row1,row2)=>[...new Set([...row1, ...row2])]).sort() : [],
        dataCount:action.payload.data.count || []}
    case 'FETCH_DATA_IN_TABLE_REJECTED':
      return {...nextState, err_msg:"Can't FETCH_DATA_IN_TABLE, try again.", error: true, loading:false}
    case 'CLEAR_HISTOGRAM_DATA':
        return{ ...nextState, histogramData:[], currentColumnType:undefined }
    case 'FETCH_HISTOGRAM_DATA_PENDING':
      return { ...nextState, loading: true }
    case 'FETCH_HISTOGRAM_DATA_FULFILLED':
      return {...nextState, loading: false, error: false, histogramData: action.payload.data.histogram !== null ? action.payload.data.histogram.sort((obj1,obj2)=>(obj1.id)-(obj2.id)).filter((obj)=>obj.id != null) : []}
    case 'FETCH_HISTOGRAM_DATA_REJECTED':
      return {...nextState, err_msg:"Can't FETCH_HISTOGRAM_DATA, try again.", error: true, loading:false}
    case 'EDIT_CHECKED_DATA_REJECTED':
      return {...nextState, err_msg:"Can't EDIT_CHECKED_DATA, try again.", error: true, loading:false}
    case 'EDIT_SELECT_DATA_REJECTED':
      return {...nextState, err_msg:"Can't EDIT_SELECT_DATA, try again.", error: true, loading:false}
    case 'SET_FILTER_PENDING':
      return { ...nextState, loading: true }
    case 'SET_FILTER_FULFILLED':
      return {...nextState, loading: false, error: false}
    case 'SET_FILTER_REJECTED':
      return {...nextState, err_msg:"Can't SET_FILTER, try again.", error: true, loading:false}
    case 'SET_SELECTED_COLUMN_PENDING':
      return { ...nextState, loading: true }
    case 'SET_SELECTED_COLUMN_FULFILLED':
      return {...nextState, loading: false, error: false}
    case 'SET_SELECTED_COLUMN_REJECTED':
      return {...nextState, err_msg:"Can't SET_SELECTED_COLUMN, try again.", error: true, loading:false}
    case 'DELETE_TABLE_PENDING':
      return { ...nextState, loading: true }
    case 'DELETE_TABLE_FULFILLED':
      return {...nextState, loading: false, error: false}
    case 'DELETE_TABLE_REJECTED':
      return {...nextState, err_msg:"Can't DELETE_TABLE, try again.", error: true, loading:false}
    case 'DELETE_SELECT_DATA_REJECTED':
      return {...nextState, err_msg:"Can't DELETE_SELECT_DATA, try again.", error: true, loading:false}
    case 'DELETE_CHECKED_DATA_REJECTED':
      return {...nextState, err_msg:"Can't DELETE_CHECKED_DATA, try again.", error: true, loading:false}
    case 'FETCH_IS_TABLE_EXIST_PENDING':
      return { ...nextState, existLoading: true }
    case 'FETCH_IS_TABLE_EXIST_FULFILLED':
      return {...nextState, existLoading: false, error: false, isTableExist:action.payload.data.exist == 1?true:false}
    case 'FETCH_IS_TABLE_EXIST_REJECTED':
      return {...nextState, err_msg:"Can't FETCH_IS_TABLE_EXIST, try again.", error: true, existLoading:false}
    case 'FETCH_COLUMN_TYPE_FULFILLED':
      return {...nextState, error: false, currentColumnType:action.payload.data.type}
    case 'FETCH_COLUMN_TYPE_REJECTED':
      return {...nextState, err_msg:"Can't FETCH_COLUMN_TYPE, try again.", error: true}
    case 'CREATE_NEW_TABLE_PENDING':
      return { ...nextState, loading: true }
    case 'CREATE_NEW_TABLE_FULFILLED':
      return {...nextState, loading: false, error:action.payload.data.result == "OK"?true:false, tableList:action.payload.data.result == "OK"?nextState.tableList.concat(action.payload.config.url.split("/").pop()):false }
    case 'CREATE_NEW_TABLE_REJECTED':
      return {...nextState, err_msg:"Can't CREATE_NEW_TABLE, try again.", error: true, loading:false}
    case 'DOWNLOAD_XLSX_PENDING':
      return { ...nextState, loading: true }
    case 'DOWNLOAD_XLSX_FULFILLED':
      FileDownload(action.payload.data, action.payload.headers.contentDisposition.replace(/attachment; filename=/,""))
      return {...nextState, loading: false, error:false }
    case 'DOWNLOAD_XLSX_REJECTED':
      return {...nextState, err_msg:"Can't DOWNLOAD_XLSX, try again.", error: true, loading:false}
    default:
      return nextState
  }
}
//FETCH_DATA_IN_TABLE
