
import { decamelize, camelize, decamelizeKeys } from 'humps'
import { isEmpty } from 'lodash'
import moment from 'moment'
import $ from 'jquery'

export function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms))
}

export function camelizer(str) {
  return camelize(str)
}

export function decamelizer(str) {
  return decamelize(str)
}

export function modifyUri(path, params = {}) {
  const newParams = decamelizeKeys(params)
  for (var p in newParams) {
    if (newParams[p] === null || newParams[p] === undefined) {
      delete newParams[p]
    }
  }

  return isEmpty(newParams) ? path : `${path}?` + $.param(newParams)
}

export function mergeErrors(errors = []) {
  let errorMessage = ''

  errors.map((error, index) => {
    if (errors.length > index + 1)
      errorMessage += `${error.field} ${error.message}, `
    else
      errorMessage += `${error.field} ${error.message}`
  })
  return errorMessage
}

export function monthAgo(date, n) {
  const currDate = date.getDate()
  const currMonth = date.getMonth()
  date.setMonth(currMonth - n)

  if (date.getMonth() === currMonth) {
    const newDate = date.getDate()
    date.setDate(currDate - newDate)
  }

  return date
}

export function timeAgo(date, days) {
  return date.setDate(date.getDate() - days)
}

export function formatShortDateTh(date, withYear = true) {
  if (withYear) {
    return date ? `${date.getDate()} ${monthShortName(date.getMonth() + 1)} ${date.getFullYear() + 543}` : ''
  } else {
    return date ? `${date.getDate()} ${monthShortName(date.getMonth() + 1)}` : ''
  }
}

export function formatShortDate(date) {
  return date ? `${date.getDate()}-${date.getMonth() + 1}-${date.getFullYear()}` : ''
}

export function formatShortTime(date) {
  return date ? `${date.getHours()}:${date.getMinutes()}` : ''
}

export function formatShortDateAndTime(date) {
  return date ? `${formatShortDate(date)} เมื่อ ${formatShortTime(date)} น.` : ''
}

export function formatShortDateAndTimeAgo(date) {
  if (date < timeAgo(new Date(), 1)) {
    return date ? `${formatShortDate(date)} เมื่อ ${formatShortTime(date)} น.` : ''
  } else {
    return date ? `${formatShortDate(date)} เมื่อ ${moment(date).locale('th').fromNow()}` : ''
  }
}

export function timeFormat(time, format = 'DD/MM/YYYY HH:mm:ss') {
  return time ? moment(time).format(format) : ''
}

export function dateFormat(time, format = 'DD/MM/YYYY') {
  return time ? moment(time).format(format) : ''
}

export function apiError(response, values = {}) {
  let error = {}
  if (response.data.code === 'validate_failed') {
    const { field, message } = response.data.errors[0]
    error = { [camelize(field)]: message }
  } else {
    error = { _error: response.data.message }
  }

  return error
}
