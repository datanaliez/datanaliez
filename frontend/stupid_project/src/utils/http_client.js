import axios from 'axios'
import humps from 'humps'

function deepFormat(data) {
  if (data instanceof Array) {
    return data.map(d => deepFormat(d))
  }
  if (data instanceof Object) {
    const formatData = {}

    Object.keys(data).forEach(key => (
      formatData[key] = deepFormat(data[key])
    ))
    return formatData
  }
  if (/^\d{4}-\d{2}-\d{2}/.test(data)) {
    return new Date(data)
  }
  return data
}
function decamelizeKeys(hash) {
  return (hash instanceof FormData) ? hash : humps.decamelizeKeys(hash)
}
function authorizationHeader() {
  return {
    headers: {
      // 'Authorization': `Bearer ${localStorage.getItem('authorizationToken')}`
    }
  }
}
function modifyUrl(url) {
  return `/api/v1/${url}`
}
function modifyMockUrl(url) {
  return `/api/mock/${url}`
}
function customCamelizeKeys(object) {
  return humps.camelizeKeys(object, (key, convert) => {
    const dummyConfigName = /^(\w*)#(-\d+)$/.exec(key)

    if (dummyConfigName) {
      const name = dummyConfigName[1]
      const sequenceId = dummyConfigName[2]

      return `${humps.camelize(name)}#${sequenceId}`
    }

    return convert(key)
  })
}

axios.interceptors.response.use(
  (rawResponse) => {
    if(rawResponse.config.responseType == "blob"){
      const response = customCamelizeKeys(rawResponse)
      response.data = rawResponse.data
      return response
    }else{
      const response = customCamelizeKeys(rawResponse)
      response.data.data = deepFormat(response.data.data)
      return response
    }
  },
  error => Promise.reject(error)
)

export function httpGet(url, params = {}, headers = {}) {
  return axios.get(
    modifyUrl(url),
    { params: decamelizeKeys(params), ...authorizationHeader(), ...headers }
  )
}
export function httpPost(url, props) {
  return axios.post(
    modifyUrl(url),
    decamelizeKeys(props),
    authorizationHeader()
  )
}
export function httpPostWithoutCamelizeKey(url, props) {
  return axios.post(
    modifyUrl(url),
    props,
    authorizationHeader()
  )
}
export function httpPostDownload(url, props) {
  return axios.post(
    modifyUrl(url),
    decamelizeKeys(props),
    {...authorizationHeader(), responseType:"blob"}
  )
}
export function httpPut(url, props) {
  return axios.put(
    modifyUrl(url),
    decamelizeKeys(props),
    authorizationHeader()
  )
}
export function httpPatch(url, props) {
  return axios.patch(
    modifyUrl(url),
    decamelizeKeys(props),
    authorizationHeader()
  )
}
export function httpDelete(url) {
  return axios.delete(
    modifyUrl(url),
    authorizationHeader()
  )
}
export function mockHttpGet(url, params = {}, headers = {}) {
  return axios.get(
    modifyMockUrl(url),
    { params: decamelizeKeys(params), ...authorizationHeader(), ...headers }
  )
}
export function mockHttpPost(url, props) {
  return axios.post(
    modifyMockUrl(url),
    decamelizeKeys(props),
    authorizationHeader()
  )
}
export function mockHttpPut(url, props) {
  return axios.put(
    modifyMockUrl(url),
    decamelizeKeys(props),
    authorizationHeader()
  )
}
export function mockHttpPatch(url, props) {
  return axios.patch(
    modifyMockUrl(url),
    decamelizeKeys(props),
    authorizationHeader()
  )
}
export function mockHttpDelete(url) {
  return axios.delete(
    modifyMockUrl(url),
    authorizationHeader()
  )
}
