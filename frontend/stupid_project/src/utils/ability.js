import { store } from '../application'
import React from 'react'

export function Authorized(props) {
  const { permissionAction, permissionSubject, children } = props
  if(can(permissionAction, permissionSubject)) {
    return children
  } else {
    return <div className='hide'/>
  }
}

export function can(requestAction, requestSubject) {
  const { currentUser: { permissions } } = getState()
  if (!permissions) return false
  for (const permission of permissions) {
    const subjects = permission.subjects
    let matchedSubject = null
    matchedSubject = subjects.find((subject) => subject === requestSubject)

    if (matchedSubject != null ) {
      let matchAction = null
      if (permission.actions == null) {
        //manage (can have all action)
        matchAction = requestAction
      } else {
        //specific action
        matchAction = permission.actions.find((action) => action === requestAction || action === 'manage')
      }
      if (matchAction != null && permission.can == true) return true
    }
  }
  return false

}

export function getState() {
  return store.getState()
}
