import { httpPost, httpGet } from '../utils/http_client'

export const Login = (params) => (
  dispatch => (
    dispatch({
      type: 'LOGIN',
      payload: {
        promise: httpPost('login', params)
      }
    })
  )
)
export const Logout = (params) => (
  dispatch => (
    dispatch({
      type: 'LOGOUT',
      payload: {
        promise: httpGet('logout')
      }
    })
  )
)
export const ServerGreeting = () => (
  dispatch => (
    dispatch({
      type: 'GREETING',
      payload: {
        promise: httpGet('greeting')
      }
    })
  )
)
export const CheckLoginStatus = () => (
  dispatch => (
    dispatch({
      type: 'LOGIN_STATUS',
      payload: {
        promise: httpGet('loginStatus')
      }
    })
  )
)
