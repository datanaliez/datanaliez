import { httpGet, httpPost } from '../utils/http_client'

export const deleteNotification = (ID) => (
  dispatch => (
    dispatch({
      type: 'DELETE_NOTIFICATION',
      payload: {
        promise: httpGet(`admin/delete/notification/${ID}`)
      }
    })
  )
)
export const editNotification = (ID, params) => (
  dispatch => (
    dispatch({
      type: 'EDIT_NOTIFICATION',
      payload: {
        promise: httpPost(`admin/edit/notification/${ID}`,params)
      }
    })
  )
)
export const newNotification = (params) => (
  dispatch => (
    dispatch({
      type: 'NEW_NOTIFICATION',
      payload: {
        promise: httpPost(`admin/new/notification`,params)
      }
    })
  )
)
export const fetchUser = () => (
  dispatch => (
    dispatch({
      type: 'FETCH_USER',
      payload: {
        promise: httpGet('admin/get/user')
      }
    })
  )
)
export const deleteUser = (ID) => (
  dispatch => (
    dispatch({
      type: 'DELETE_USER',
      payload: {
        promise: httpGet(`admin/delete/user/${ID}`)
      }
    })
  )
)
export const editUser = (ID, params) => (
  dispatch => (
    dispatch({
      type: 'EDIT_USER',
      payload: {
        promise: httpPost(`admin/edit/user/${ID}`,params)
      }
    })
  )
)
export const newUser = (params) => (
  dispatch => (
    dispatch({
      type: 'NEW_USER',
      payload: {
        promise: httpPost(`admin/new/user`,params)
      }
    })
  )
)
