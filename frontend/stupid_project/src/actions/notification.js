import { httpGet } from '../utils/http_client'

export const fetchNotification = () => (
  dispatch => (
    dispatch({
      type: 'FETCH_NOTIFICATION',
      payload: {
        promise: httpGet('get/notification')
      }
    })
  )
)
