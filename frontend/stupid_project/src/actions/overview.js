import { httpGet } from '../utils/http_client'

export const setTableProp = (state) => (
  dispatch => (
    dispatch({
      type: 'SET_TABLEPROP',
      payload: state
    })
  )
)
export const UpdatedData = () => (
  dispatch => (
    dispatch({
      type: 'OVERVIEW_UPDATED_DATA'
    })
  )
)
