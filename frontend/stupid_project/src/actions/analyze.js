export const saveState = (state) => (
  dispatch => (
    dispatch({
      type: 'SAVE_ANALYZE_PARAM_STATE',
      payload: state
    })
  )
)
export const clearAllHistogramData = () => (
  dispatch => (
    dispatch({
      type: 'CLEAR_HISTOGRAM_DATA'
    })
  )
)
export const UpdatedData = () => (
  dispatch => (
    dispatch({
      type: 'ANALYZE_UPDATED_DATA'
    })
  )
)
