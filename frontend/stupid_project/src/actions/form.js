import { httpGet, httpPost, httpPostWithoutCamelizeKey } from '../utils/http_client'

export const fetchTableForm = (tableName) => (
  dispatch => (
    dispatch({
      type: 'FETCH_FORM_IN_TABLE',
      payload: {
        promise: httpGet(`get/tableForm/${tableName}`)
      }
    })
  )
)

export const createForm = (tableName, params) => (
  dispatch => (
    dispatch({
      type: 'CREATE_FORM',
      payload: {
        promise: httpPost(`new/createForm/${tableName}`, params)
      }
    })
  )
)
export const editForm = (formID, params) => (
  dispatch => (
    dispatch({
      type: 'EDIT_FORM',
      payload: {
        promise: httpPost(`edit/form/${formID}`, params)
      }
    })
  )
)
export const SwitchTable = (formID, flag) => (
  dispatch => (
    dispatch({
      type: 'SWITCH_FORM',
      payload: {
        promise: httpGet(`edit/formSwitch/${formID}/${flag}`)
      }
    })
  )
)

export const getFormData = (formID) => (
  dispatch => (
    dispatch({
      type: 'FETCH_FORM_INFO',
      payload: {
        promise: httpGet(`form/info/${formID}`)
      }
    })
  )
)

export const FormSubmission = (formID, params) => (
  dispatch => (
    dispatch({
      type: 'SUBMIT_FORM',
      payload: {
        promise: httpPostWithoutCamelizeKey(`form/submit/${formID}`, params)
      }
    })
  )
)

export const deleteForm = (formID) => (
  dispatch => (
    dispatch({
      type: 'DELETE_FORM',
      payload: {
        promise: httpGet(`delete/form/${formID}`)
      }
    })
  )
)
export const flushStat = (formID, statType) => (
  dispatch => (
    dispatch({
      type: 'FLUSH_STAT',
      payload: {
        promise: httpGet(`edit/flushFormStat/${formID}/${statType}`)
      }
    })
  )
)
