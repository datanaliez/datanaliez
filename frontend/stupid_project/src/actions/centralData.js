import { httpGet, httpPost, httpPostDownload } from '../utils/http_client'

export const reset = () => (
  dispatch => (
    dispatch({
      type: 'RESET'
    })
  )
)
export const fetchTableList = () => (
  dispatch => (
    dispatch({
      type: 'FETCH_TABLELIST',
      payload: {
        promise: httpGet('get/tableList')
      }
    })
  )
)
export const createNewTable = (tableName) => (
  dispatch => (
    dispatch({
      type: 'CREATE_NEW_TABLE',
      payload: {
        promise: httpGet(`new/createTable/${tableName}`)
      }
    })
  )
)
export const DropTable = (tableName) => (
  dispatch => (
    dispatch({
      type: 'DELETE_TABLE',
      payload: {
        promise: httpGet(`delete/deleteTable/${tableName}`)
      }
    })
  )
)
// selected data frame
export const DeleteSelectData = (tableName) => (
  dispatch => (
    dispatch({
      type: 'DELETE_SELECT_DATA',
      payload: {
        promise: httpGet(`delete/deleteSelectData/${tableName}`)
      }
    })
  )
)

// checked data
export const DeleteCheckedData = (tableName,idList) => (
  dispatch => (
    dispatch({
      type: 'DELETE_CHECKED_DATA',
      payload: {
        promise: httpGet(`delete/deleteCheckedData/${tableName}/${idList}`)
      }
    })
  )
)

export const isTableExist = (tableName) => (
  dispatch => (
    dispatch({
      type: 'FETCH_IS_TABLE_EXIST',
      payload: {
        promise: httpGet(`get/isTableExist/${tableName}`)
      }
    })
  )
)
export const fetchDataInTable = (tableName, pageNo, params) => (
  dispatch => (
    dispatch({
      type: 'FETCH_DATA_IN_TABLE',
      payload: {
        promise: httpPost(`get/data/${tableName}/${pageNo}`,params)
      }
    })
  )
)
export const exportCurrentDataToXLSX = (tableName, params) => (
  dispatch => (
    dispatch({
      type: 'DOWNLOAD_XLSX',
      payload: {
        promise: httpPostDownload(`get/export/${tableName}`,params)
      }
    })
  )
)
export const fetchColumnNameInTable = (tableName) => (
  dispatch => (
    dispatch({
      type: 'FETCH_COLUMN_NAME_IN_TABLE',
      payload: {
        promise: httpGet(`get/allColumnName/${tableName}`)
      }
    })
  )
)

// checked data edit
export const EditCheckedData = (tableName,data) => (
  dispatch => (
    dispatch({
      type: 'EDIT_CHECKED_DATA',
      payload: {
        promise: httpPost(`edit/editCheckedData/${tableName}/${data}`)
      }
    })
  )
)
// data frame edit
export const EditSelectData = (tableName,data) => (
  dispatch => (
    dispatch({
      type: 'EDIT_SELECT_DATA',
      payload: {
        promise: httpPost(`edit/editSelectData/${tableName}/${data}`)
      }
    })
  )
)

export const setFilter = (tableName, params) => (
  dispatch => (
    dispatch({
      type: 'SET_FILTER',
      payload: {
        promise: httpPost(`get/setFilter/${tableName}`,params)
      }
    })
  )
)
export const setSelectedColumn = (tableName, params) => (
  dispatch => (
    dispatch({
      type: 'SET_SELECTED_COLUMN',
      payload: {
        promise: httpPost(`get/setSelectedColumn/${tableName}`,params)
      }
    })
  )
)
export const setTableName = (newTableName) => (
  dispatch => (
    dispatch({
      type: 'SET_TABLE_NAME',
      payload: newTableName
    })
  )
)
export const MustUpdateData = (flag) => (
  dispatch => (
    dispatch({
      type: 'MUST_UPDATE_DATA',
      payload: flag ? flag : true
    })
  )
)
export const getHistogramData = (tableName, params) => (
  dispatch => (
    dispatch({
      type: 'FETCH_HISTOGRAM_DATA',
      payload: {
        promise: httpPost(`get/histogram/${tableName}`,params)
      }
    })
  )
)
export const checkTypeOfColumn = (tableName, columnName) => (
  dispatch => (
    dispatch({
      type: 'FETCH_COLUMN_TYPE',
      payload: {
        promise: httpGet(`get/columnType/${tableName}/${columnName}`)
      }
    })
  )
)
