import React, { Component } from 'react'
import { connect } from 'react-redux'
import { push } from 'react-router-redux'

import { fetchNotification } from '../../actions/notification'
import '../../css/main.css'
import { Collapse, Row, Col, Spin } from 'antd';

const Panel = Collapse.Panel
class Notifications extends Component {
  componentWillMount(){
    this.props.fetchNotification()
  }
  render() {
    const content = (<Row justify="center" type="flex" style={{marginTop:100}}>
        <Col span={16}>
          <h1>Notifications</h1>
          <hr/>
          <Collapse style={{marginTop:30}}>
            {
              this.props.notification.notification_list.map((info)=>(<Panel style={{background:`linear-gradient(to right, ${info.priority == "2"?"#ffd9ce":info.priority == "1"?"#fcf9d6":"#cbffc4"}, #ffffff)`, fontFamily:"'Wire One', sans-serif"}} header={<div dangerouslySetInnerHTML={{__html:`<${info.publicDate}> ${info.headline}`}}/>}><div dangerouslySetInnerHTML={{__html:info.body}} /></Panel>))
            }
          </Collapse>
        </Col>
      </Row>)
     return this.props.notification.loading?(
      <Spin tip="Loading..." disabled>
        {content}
      </Spin>
    ):(content)
  }
}

const mapStateToProps = state => ({...state})
export default connect(mapStateToProps,
  {
    push,
    fetchNotification
  })(Notifications)
