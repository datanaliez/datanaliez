import React, { Component } from 'react'
import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import BurgerMenu from 'react-burger-menu'
import { Link } from 'react-router'
import { Icon, Row, Col } from 'antd'

import { CheckLoginStatus } from '../actions/login'
import '../css/main.css'
import Logo from '../media/DatanaliEZ_logo.png'
const initMenuList = [
  {name:"Notification", path:"notify", iconType:"notification"},
  {name:"Overview", path:"data/overview", iconType:"layout"},
  {name:"Analysis", path:"data/analyze", iconType:"bar-chart"},
  {name:"Magician", path:"data/magic", iconType:"tool"},
]
const permissionCode = {
  0:"Guest",
  1:"Users",
  2:"Administrator"
}
const Menu = BurgerMenu["slide"]
class mainApp extends Component {
  state={
    isOpenMenu:false,
    activeMenuList:[]
  }
  componentWillMount(){
    this.props.CheckLoginStatus()
  }
  componentWillUpdate(){
  }
  componentWillReceiveProps(newProps){
    let activeMenuList = initMenuList.map((value)=>(value))
    if(!newProps.application.LoggedIn && !newProps.application.loading){
      newProps.push("/")
    }else if(newProps.application.currentPermission == "2"){
      const menuCount = initMenuList.length
      activeMenuList[menuCount] = activeMenuList[menuCount - 1]
      activeMenuList[menuCount - 1] = {name:"Administration", path:"admin/notify", iconType:"usergroup-add"}
      this.state.activeMenuList = activeMenuList
    }else{
      this.state.activeMenuList = initMenuList
    }
  }
  render() {
    return (
      <div id="page">
        <Row justify="start" type="flex" >
          <Col span={2}>
            <Menu id="AppMenu" pageWrapId="" outerContainerId="page" left isOpen={this.state.isOpenMenu}>
              {
                this.state.activeMenuList.map((menu) => (<Link className="whiteConsole" to={`/${menu.path}`} style={{paddingBottom:30, fontSize: 20}} key={Math.random().toString()} onClick={() => this.setState({isOpenMenu:false})} ><Icon type={menu.iconType}/><br/>{`    ${menu.name}`}</Link>))
              }
            </Menu>
          </Col>
          <Col span={22}>
            <div style={{paddingTop:20, paddingBottom: 20, paddingLeft:20, background:"linear-gradient(to right, white, rgba(5, 117, 230, 0.78))"}}>
              <Row justify="space-between" align="center" type="flex">
                <Col>
                  <Row justify="start" align="center" type="flex">
                    <img src={`/${Logo.replace(/^\//,"")}`} style={{objectFit:"contain", height:40, marginRight:20}}/>
                    <h1 style={{fontFamily: "'Wire One', sans-serif", color:"#3205e6"}} className="white">DatanaliEZ&reg; dashboard</h1>
                  </Row>
                </Col>
                <Col>
                  <strong className="whiteConsole" style={{fontSize:15, color:"#505050"}}>{`${this.props.application.currentUsername} (${permissionCode[this.props.application.currentPermission]})`}</strong>
                  <Link className="whiteConsole" to="/logout" style={{margin:"0px 20px", color:"#505050"}}><Icon type="logout"/>Logout</Link>
                </Col>
              </Row>
            </div>
            {this.props.children}
          </Col>
        </Row>
      </div>
    )
  }
}

const mapStateToProps = state => ({...state})
export default connect(mapStateToProps,
  {
    push,
    CheckLoginStatus
  })(mainApp)
