import React from 'react'
import { Route, Router, IndexRedirect, IndexRoute } from 'react-router'
import { syncHistoryWithStore } from 'react-router-redux'

import Login from './login'
import mainApp from './mainApp'
import Notifications from './notification'
import Loggingout from "./logout";
import Admin from "./admin"
import NotificationControll from "./admin/notification"
import UserControll from "./admin/user"
import DataController from "./components/dataController"
import DataOverview from "./overview"
import DataAnalyze from "./analyze"
import NewForm from "./form/New"
import EditForm from "./form/Edit"
import UserForm from "./form/UserForm"

export default (store, history) => (
  <Router history={syncHistoryWithStore(history, store)}>
    <Route path="/">
      <IndexRoute component={Login}/>
      <Route path="form">
        <Route path=":formID" component={UserForm}/>
      </Route>
      <Route component={mainApp}>
        <Route path="notify" component={Notifications} />
        <Route path="logout" component={Loggingout}/>
        <Route path="formControll">
          <Route path="new">
            <Route path=":tableName" component={NewForm} />
          </Route>
          <Route path="edit">
            <Route path=":formID" component={EditForm} />
          </Route>
        </Route>
        <Route path="data" component={DataController}>
          <Route path="overview">
            <Route path=":tableName" component={DataOverview} />
          </Route>
          <Route path="analyze">
            <Route path=":tableName" component={DataAnalyze} />
          </Route>
        </Route>
        <Route path="admin" component={Admin}>
          <IndexRoute component={NotificationControll}/>
          <Route path="notify" component={NotificationControll} />
          <Route path="users" component={UserControll} />
        </Route>
      </Route>
    </Route>
  </Router>
)
