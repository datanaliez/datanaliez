import React, { Component } from 'react'
import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import { Row, Col, Button, Icon, message, Collapse, Select, Input, Tabs, Checkbox} from 'antd';
import Highcharts from "react-highcharts";

import {
  fetchDataInTable,
  getHistogramData,
  checkTypeOfColumn
} from '../../actions/centralData'
import {
  saveState,
  UpdatedData
} from "../../actions/analyze";
// import {

// } from '../actions/login'
const Panel = Collapse.Panel
const Option = Select.Option
const TabPane = Tabs.TabPane
const CheckboxGroup = Checkbox.Group

import '../../css/main.css'
import { camelize } from 'humps';

class DataAnalyze extends Component {
  state = {
    columnName:"",
    minBound:0,
    interval:0,
    displayChartType:["spline","column"]
  }
  componentWillMount(){
    this.state = this.props.dataAnalyze
    this.componentWillReceiveProps(this.props)
  }

  componentWillReceiveProps(newProps){
    if(newProps.dataAnalyze.mustUpdateData){
      this.loadHistogram()
      this.props.UpdatedData()
    }
  }
  loadHistogram = () => {
    if (this.props.centralData.currentColumnType == "NumberDecimal") {
      this.props.getHistogramData(this.props.params.tableName, {
        columnName: this.state.columnName,
        minBound: parseFloat(this.state.minBound),
        interval: parseFloat(this.state.interval)
      })
    } else {
      this.props.getHistogramData(this.props.params.tableName,{
        columnName: this.state.columnName,
      })
    }
    this.props.saveState({...this.state,minBound:parseFloat(this.state.minBound), interval:parseFloat(this.state.interval)})
  }
  render() {
    let histogramHighchartConfig = {
      title: {
          text: 'Histogram of (tableName)'
      },
      xAxis: {
          categories: [],
          crosshair: true
      },
      yAxis: {
          min: 0,
          title: {
              text: 'Frequency'
          }
      },
      tooltip: {
          headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
          pointFormat: '<tr><td style="color:{series.color};padding:0">freq: </td>' +
              '<td style="padding:0"><b>{point.y}</b></td></tr>',
          footerFormat: '</table>',
          shared: true,
          useHTML: true
      },
      plotOptions: {
      },
      series:this.state.displayChartType.map((type)=>({
          type,
          name: 'NULL',
          data: []
        }))
    }
    if(this.props.centralData.histogramData.length != 0){
      let indexFreqPair = {}
      this.props.centralData.histogramData.forEach(obj => {
        indexFreqPair[obj.id.toString()] = obj.count
      });
      histogramHighchartConfig.title.text=`Distribution of ${this.props.dataAnalyze.columnName} in table '${this.props.params.tableName}'`
      histogramHighchartConfig.series.map((obj)=>{
        obj.name=this.props.dataAnalyze.columnName
        return obj
      })
      if (this.props.centralData.currentColumnType == "NumberDecimal" ){
        const lastIndex = parseInt(this.props.centralData.histogramData[this.props.centralData.histogramData.length-1].id)
        const firstIndex = parseInt(this.props.centralData.histogramData[0].id)
        try {
          const keyArray  = Array.apply(null, {length: lastIndex - firstIndex + 1}).map((v,index) => (index + firstIndex))
          histogramHighchartConfig.series.map((obj)=>{
            obj.data=keyArray.map((index)=>(indexFreqPair[index.toString()] || 0))
            return obj
          })
          histogramHighchartConfig.xAxis.categories = keyArray.map((index)=>`${(index)*(this.props.dataAnalyze.interval)+this.props.dataAnalyze.minBound} \u2264 X < ${(index+1)*(this.props.dataAnalyze.interval)+this.props.dataAnalyze.minBound}`)
        } catch (error) {
          histogramHighchartConfig.xAxis.categories = this.props.centralData.histogramData.map((obj)=>`${(obj.id)*(this.props.dataAnalyze.interval)+this.props.dataAnalyze.minBound} \u2264 X < ${(obj.id+1)*(this.props.dataAnalyze.interval)+this.props.dataAnalyze.minBound}`)
          histogramHighchartConfig.series.map((obj)=>{
            obj.data=this.props.centralData.histogramData.map(obj=>obj.count)
            return obj
          })
        }
      }else{
        histogramHighchartConfig.xAxis.categories = this.props.centralData.histogramData.map(obj=>obj.id)
        histogramHighchartConfig.series.map((obj)=>{
          obj.data=this.props.centralData.histogramData.map(obj=>obj.count)
          return obj
        })
      }
    }
    return (
    <div>
      <Tabs defaultActiveKey="histogram">
        <TabPane tab={<span><Icon type="bar-chart" />Histogram</span>} key="histogram">
          <Row>
            <Col span={12}>
              <Collapse>
                <Panel header={<label className="blackConsole">Setting</label>}>
                  <Row justify="start" type="flex">
                    <Col span={8}>
                      <label>Column name</label>
                    </Col>
                    <Col span={16}>
                      <Select showSearch onChange={(value)=>{
                        this.state.columnName = value
                        this.props.saveState({...this.state})
                        this.props.checkTypeOfColumn(this.props.params.tableName,value)
                      }} placeholder="Column name" style={{width:"100%"}} value={this.state.columnName}>
                      {
                        this.props.centralData.allPossibleColumn.map((name)=><Option value={name} key={Math.random()}>{camelize(name)}</Option>)
                      }
                      </Select>
                    </Col>
                  </Row>
                  {
                    this.props.centralData.currentColumnType == "NumberDecimal" ? (
                    <div>
                      <Row justify="start" type="flex" style={{marginTop:10}}>
                        <Col span={8}>
                          <label>Histogram minimum bound</label>
                        </Col>
                        <Col span={8}>
                          <Input onChange={(e)=>this.setState({minBound:e.target.value})} placeholder="Min bound" style={{width:"100%"}} value={this.state.minBound}/>
                        </Col>
                      </Row>
                      <Row justify="start" type="flex" style={{marginTop:10}}>
                        <Col span={8}>
                          <label>Histogram interval</label>
                        </Col>
                        <Col span={8}>
                          <Input onChange={(e)=>this.setState({interval:e.target.value})} placeholder="Interval" style={{width:"100%"}} value={this.state.interval}/>
                        </Col>
                      </Row>
                    </div>):(<div></div>)
                  }
                  <Row justify="end" type="flex" style={{marginTop:10}}>
                      <Button onClick={this.loadHistogram} type="primary" disabled = {this.props.centralData.loadingState.filter((state)=>(/FETCH_COLUMN_TYPE/.test(state))).length > 0}> <Icon type="bar-chart" /> Build histogram</Button>
                  </Row>
                </Panel>
              </Collapse>
            </Col>
          </Row>
          <Row justify="center" style={{marginTop:20}}>
            <Row justify="start" type="flex" style={{marginTop:10}}>
              <label>Display:</label>
              <Checkbox.Group options={["spline","column"]} value={this.state.displayChartType} onChange={(value)=>this.setState({displayChartType:value})} />
            </Row>
            {
              this.props.centralData.histogramData.length != 0 ? <Highcharts config={histogramHighchartConfig}/>: <div/>
            }
          </Row>
        </TabPane>
        <TabPane tab={<span><Icon type="dot-chart" />Relational graph</span>} key="relationGraph">
            :P
        </TabPane>
      </Tabs>
    </div>)
  }
}

const mapStateToProps = state => ({...state})
export default connect(mapStateToProps,
  {
    push,
    fetchDataInTable,
    getHistogramData,
    saveState,
    UpdatedData,
    checkTypeOfColumn
  })(DataAnalyze)
