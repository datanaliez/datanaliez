import React, { Component } from 'react'
import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import { Row, Col, Button, Icon, Upload, message, Spin, Popconfirm, Modal} from 'antd';
import { Link } from 'react-router'
import { difference } from "lodash";

import { fetchColumnNameInTable } from '../../actions/centralData'
import { createForm } from '../../actions/form'
import FormForm from "../components/formForm"

class NewForm extends Component {
  componentWillMount(){
    this.props.fetchColumnNameInTable(this.props.params.tableName)
  }
  componentWillReceiveProps(newProps){
    if(this.props.formControll.newFormID != newProps.formControll.newFormID){
      this.props.push(`/formControll/edit/${newProps.formControll.newFormID}`)
    }
  }
  createNewForm = (value) =>{
    var createParams = {...value}
    if((value.autoClose || {}).includeDuration){
      const startTime = createParams.autoClose.duration.start.toISOString()
      const endTime = createParams.autoClose.duration.stop.toISOString()
      createParams = {...createParams, closeOn:{datetime:[startTime, endTime]}}
    }
    if((value.autoClose || {}).includeRegistLimit){
      createParams = {...createParams, closeOn:{...createParams.closeOn, registered:parseInt(value.autoClose.registLimit)}}
    }
    createParams.fieldList = createParams.fieldList.map((field)=>({...field, required: field.required||false}))
    this.props.createForm(this.props.params.tableName, createParams)
    .then(()=>message.success("Form created."))
  }
  render() {
    return this.props.formControll.loadingState.length == 0?(
      <div>
        <Row justify="left" type="flex" style={{marginTop:20}}>
          <Col lg={{ span: 16, offset: 4 }} xs={{ span: 20, offset: 0 }}>
            <h2 className="blackConsole">{`Create new form for table: '${this.props.params.tableName}'`}</h2>
            <FormForm tableColumns={this.props.centralData.allPossibleColumn} onFormSubmit={this.createNewForm}/>
          </Col>
        </Row>
      </div>
    ):<Modal visible={true} footer={null} closable={false} width={100}>
    <Row align="center" type="flex">
      <Col >
        <Spin tip="Loading..."/>
      </Col>
    </Row>
  </Modal>
  }
}

const mapStateToProps = state => ({...state})
export default connect(mapStateToProps,
  {
    push,
    fetchColumnNameInTable,
    createForm
  })(NewForm)
