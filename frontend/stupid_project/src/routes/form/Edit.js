import React, { Component } from 'react'
import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import { Row, Col, Button, Icon, Upload, message, Spin, Popconfirm, Tabs, Modal, Switch, Select} from 'antd';
import { Link } from 'react-router'
import Moment from 'moment'
import Highcharts from "react-highcharts"

import { fetchColumnNameInTable } from '../../actions/centralData'
import { getFormData, editForm, SwitchTable, flushStat } from '../../actions/form'
import FormForm from "../components/formForm"

const TabPane = Tabs.TabPane;
const Option = Select.Option;

class EditForm extends Component {
  state={
    groupBy:"hour",
  }
  componentWillMount(){
    this.props.getFormData(this.props.params.formID)
    .then(()=>this.props.fetchColumnNameInTable(this.props.formControll.currentFormInfo.targetCollection))
  }
  ModifyForm = (value) =>{
    var createParams = {...value}
    if((value.autoClose || {}).includeDuration){
      const startTime = createParams.autoClose.duration.start.toISOString()
      const endTime = createParams.autoClose.duration.stop.toISOString()
      createParams = {...createParams, closeOn:{...createParams.closeOn, datetime:[startTime, endTime]}}
    }else{
      createParams = {...createParams, closeOn:{...createParams.closeOn, datetime: undefined}}
    }
    if((value.autoClose || {}).includeRegistLimit){
      createParams = {...createParams, closeOn:{...createParams.closeOn, registered:parseInt(value.autoClose.registLimit)}}
    }else{
      createParams = {...createParams, closeOn:{...createParams.closeOn, registered:undefined}}
    }
    createParams.fieldList = createParams.fieldList.map((field)=>({...field, required: field.required||false}))
    this.props.editForm(this.props.params.formID, createParams)
    .then(()=>{
      message.success("Form modified.")
      window.location.reload()
    })
  }
  render() {
    if(this.props.formControll.loadingState.length == 0){
      const answerStat = _.groupBy(this.props.formControll.currentFormInfo.answered, (date)=>Moment(date).startOf(this.state.groupBy).format())
      const answerChart = {
        title: {
            text: `Form answer statistics`
        },
        subtitle: {
          text: `Answered ${this.props.formControll.currentFormInfo.answered.length} time(s)`
        },
        xAxis: {
            categories: Object.keys(answerStat),
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Frequency'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">freq: </td>' +
                '<td style="padding:0"><b>{point.y}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
        },
        series:[{
          type:"column",
          name: 'answer',
          data: Object.keys(answerStat).map((hourGroup)=>(answerStat[hourGroup].length))
        }]
      }
      const visitStat = _.groupBy(this.props.formControll.currentFormInfo.visited, (date)=>Moment(date).startOf(this.state.groupBy).format())
      const visitChart = {
        title: {
            text: 'Form visit statistics'
        },
        subtitle: {
          text: `Visited ${this.props.formControll.currentFormInfo.visited.length} time(s)`
        },
        xAxis: {
            categories: Object.keys(visitStat),
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Frequency'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">freq: </td>' +
                '<td style="padding:0"><b>{point.y}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
        },
        series:[{
          type:"column",
          name: 'visit',
          data: Object.keys(visitStat).map((hourGroup)=>(visitStat[hourGroup].length))
        }]
      }
      return <div>
        <Row justify="center" type="flex" style={{marginTop:20}}>
          <Col lg={20} xs={23}>
            <Row justify="space-between" type="flex">
              <Col>
                <h1 className="blackConsole">{`Edit form: '${this.props.formControll.currentFormInfo.title}'`}</h1>
                <p className="blackConsole">{`Table: '${this.props.formControll.currentFormInfo.targetCollection}'`}</p>
                <a href={`http://${window.location.host}/form/${this.props.formControll.currentFormInfo.formID}`}>{`http://${window.location.host}/form/${this.props.formControll.currentFormInfo.formID}`}</a>
              </Col>
              <Col>
                <Switch checked={!this.props.formControll.currentFormInfo.disabled} onChange={(e)=>{this.props.SwitchTable(this.props.params.formID, !e)}}/>
              </Col>
            </Row>
            <Row justify="center" type="flex">
              <Col lg={20} xs={23}>
                <Tabs defaultActiveKey="1" tabPosition="left" style={{marginTop:20}}>
                  <TabPane tab={<span><Icon type="tool" />Setting</span>} key="1">
                    <FormForm tableColumns={this.props.centralData.allPossibleColumn} onFormSubmit={this.ModifyForm} initialValues={this.props.formControll.currentFormInfo}/>
                  </TabPane>
                  <TabPane tab={<span><Icon type="area-chart" />Stat</span>} key="2">
                    <Row justify="start" align="middle" type="flex">
                      <label>Group by: </label>
                      <Select defaultValue="hour" style={{width:120}} onChange={(value)=>this.setState({groupBy:value})}>
                        <Option value="date">date</Option>
                        <Option value="hour">hour</Option>
                        <Option value="minute">minute</Option>
                        <Option value="second">second</Option>
                      </Select>
                    </Row>
                    <Row justify="end" type="flex">
                      <Popconfirm title="Are you sure delete answered stat?" onConfirm={()=>{this.props.flushStat(this.props.params.formID, "answered");window.location.reload()}} okText="Yes" cancelText="No">
                        <Button type="danger">Flush</Button>
                      </Popconfirm>
                    </Row>
                    <Highcharts config={answerChart}/>
                    <Row justify="end" type="flex">
                      <Popconfirm title="Are you sure delete visited stat?" onConfirm={()=>{this.props.flushStat(this.props.params.formID, "visited");window.location.reload()}} okText="Yes" cancelText="No">
                        <Button type="danger">Flush</Button>
                      </Popconfirm>
                    </Row>
                    <Highcharts config={visitChart}/>
                  </TabPane>
                </Tabs>
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
    }else{
      return <Modal visible={true} footer={null} closable={false} width={100}>
      <Row align="center" type="flex">
        <Col >
          <Spin tip="Loading..."/>
        </Col>
      </Row>
    </Modal>
    }
  }
}

const mapStateToProps = state => ({...state})
export default connect(mapStateToProps,
  {
    push,
    fetchColumnNameInTable,
    getFormData,
    editForm,
    SwitchTable,
    flushStat
  })(EditForm)
