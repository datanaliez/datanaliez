import React, { Component } from 'react'
import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import { Row, Col, Button, Icon, Upload, message, Spin, Popconfirm, Modal} from 'antd';
import { Link } from 'react-router'
import { difference } from "lodash";
import Moment from "moment";

import UserFieldsForm from "../components/userForm"
import Logo from '../../media/Vidvaplus_Logo.png'
import '../../css/form.css'

import { getFormData, FormSubmission } from '../../actions/form'
class UserForm extends Component {
  state={
    err_msg:""
  }
  componentWillMount(){
    this.props.getFormData(this.props.params.formID)
  }
  componentWillReceiveProps(newProps){
    if(newProps.publishForm.err_msg != "" && (newProps.publishForm.err_msg != this.state.err_msg)){
      message.error(newProps.publishForm.err_msg)
    }
    if(newProps.publishForm.isSubmitCompleted){
      message.success("Submission completed.")
    }
    this.setState({err_msg:newProps.publishForm.err_msg})
  }
  formSubmission = (value) =>{
    this.props.FormSubmission(this.props.params.formID, value)
  }
  formValidation = (values) => {
    var errors = this.props.publishForm.currentFormInfo.fieldList
    .filter((field)=>(field.required))
    .map((field)=>(field.name))
    .reduce((errors, field) => (values[field] ? errors : { ...errors, [field]: 'Required' }), {})
    errors = this.props.publishForm.currentFormInfo.fieldList.reduce((errors, field) => {
      if(values[field.name]){
        if(field.type == "integer"){
          if(values[field.name] != parseInt(values[field.name])){
            return { ...errors, [field.name]: 'Must be integer' }
          }else{
            values[field.name] = parseInt(values[field.name])
          }
        }
        if(field.type == "number"){
          if(values[field.name] != parseFloat(values[field.name])){
            return { ...errors, [field.name]: 'Must be decimal number' }
          }else{
            values[field.name] = parseFloat(values[field.name])
          }
        }
        if(field.validate){
          const validator = new RegExp(field.validate)
          if(!validator.test(values[field.name])){
            return { ...errors, [field.name]: 'Incorrect format' }
          }
        }
      }
      return errors
    }, errors)
    return errors
  }
  render() {
    var body = null
    var formBody = null
    const now = (new Date()).toISOString()
    if (this.props.publishForm.loadingState.length == 0) {
      if(this.props.publishForm.isSubmitCompleted){
        body = <div>
          <Row justify="center" type="flex">
            <Icon type="check-circle" style={{fontSize:30, color:"green"}}/>
          </Row>
          <Row justify="center" type="flex" style={{marginTop:10}}>
            <h1>Submission Completed!</h1>
          </Row>
          <Row justify="center" type="flex" style={{marginTop:10}}>
            <Button onClick={()=>window.location.reload()}>New submission</Button>
          </Row>
        </div>
      }else if(this.props.publishForm.currentFormInfo == null){
        body=
          <div>
            <Row justify="center" type="flex">
              <Icon type="exclamation-circle" style={{fontSize:30}}/>
            </Row>
            <Row justify="center" type="flex" style={{marginTop:10}}>
              <h1 style={{color:"red"}}>Form not found</h1>
            </Row>
            <Row justify="center" type="flex" style={{marginTop:10}}>
              <strong style={{color:"red"}}>Something went wrong, please ensure that link is correct</strong>
            </Row>
          </div>
      }else{
        document.title=`${this.props.publishForm.currentFormInfo.title}`
        body=
          <div>
            <Row justify="left" type="flex">
              <h1>{this.props.publishForm.currentFormInfo.title}</h1>
            </Row>
            <Row justify="left" type="flex">
              <p style={{whiteSpace: "pre-line"}}>{this.props.publishForm.currentFormInfo.desc}</p>
            </Row>
          </div>
        if(this.props.publishForm.currentFormInfo.disabled){
          formBody=
            <div>
              <Row justify="left" type="flex" align="middle" style={{fontSize:20}}>
                <Icon type="exclamation-circle-o" />
                <strong style={{color:"red", marginLeft:5}}>This form was disabled.</strong>
              </Row>
              <Row justify="left" type="flex">
                <label>Please contact form owner, if you think this is a mistake</label>
              </Row>
            </div>
        }else if(this.props.publishForm.currentFormInfo.registExceed){
          formBody=
            <div>
              <Row justify="left" type="flex" align="middle" style={{fontSize:20}}>
                <Icon type="exclamation-circle-o" />
                <strong style={{color:"red", marginLeft:5}}>Sorry, this form has received enough responses</strong>
              </Row>
              <Row justify="left" type="flex">
                <label>Please contact form owner, if you think this is a mistake</label>
              </Row>
            </div>
        }else if ((this.props.publishForm.currentFormInfo.closeOn.datetime !== undefined) && !(now > this.props.publishForm.currentFormInfo.closeOn.datetime[0] && now < this.props.publishForm.currentFormInfo.closeOn.datetime[1])){
          formBody=
            <div>
              <Row justify="left" type="flex" align="middle" style={{fontSize:20}}>
                <Icon type="exclamation-circle-o" />
                <strong style={{color:"red", marginLeft:5}}>Sorry, this form not available this time</strong>
              </Row>
              <Row justify="left" type="flex">
                <label>Please contact form owner, if you think this is a mistake</label>
              </Row>
            </div>
        }else{
          formBody = <UserFieldsForm fieldList={this.props.publishForm.currentFormInfo.fieldList} onFormSubmit={this.formSubmission} validate={this.formValidation}/>
        }
      }
    }else{
      return(<Modal visible={true} footer={null} closable={false} width={100}>
        <Row align="center" type="flex">
          <Col >
            <Spin tip="Loading..."/>
          </Col>
        </Row>
      </Modal>)
    }
    return(
      <div>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no"/>
        <link href="https://fonts.googleapis.com/css?family=Mitr" rel="stylesheet"/>
        {/* <div style={{paddingTop:20, paddingBottom: 20, paddingLeft:20, background:"linear-gradient(to right, white, rgba(5, 117, 230, 0.78))"}}>
          <Row justify="center" type="flex">
            <Col xs={24} md={20}>
              <Row type="flex">
                <img src={`/${Logo.replace(/^\//,"")}`} style={{objectFit:"contain", height:40, marginRight:5}}/>
                <h1 style={{fontFamily: "'Wire One', sans-serif", color:"#3205e6"}} className="white">DatanaliEZ</h1>
                <Row align="bottom" type="flex">
                  <p style={{marginLeft:5, fontSize:10}}> Data | Analyze | Easily </p>
                </Row>
              </Row>
            </Col>
          </Row>
        </div> */}
        <div style={{paddingTop:20, paddingBottom: 20, paddingLeft:20, background:"linear-gradient(to right, white, rgba(5, 117, 230, 0.78))"}}>
          <Row justify="center" type="flex">
            <Col xs={24} md={20}>
              <Row type="flex">
                <img src={`/${Logo.replace(/^\//,"")}`} style={{objectFit:"contain", height:40, marginRight:5}}/>
                <h1 style={{fontFamily: "'Mitr', sans-serif", color:"rgba(230, 222, 28, 0.63)", padding:"0px 10px", backgroundColor:"#761214", borderRadius:20}} className="white">ค่ายวิศวพลัส</h1>
                <Row align="bottom" type="flex">
                  <p style={{marginLeft:5, fontSize:10}}> กลุ่มนิสิตทุน คณะวิศวกรรมศาสตร์ จุฬาลงกรณ์มหาวิทยาลัย </p>
                </Row>
              </Row>
            </Col>
          </Row>
        </div>
        <Row justify="center" type="flex" style={{marginTop:20}}>
          <Col md={16} xs={22}>
            {body}
            <Row justify="center" type="flex" style={{marginTop:10}}>
              <Col md={16} xs={22}>
                {formBody}
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
    )
  }
}

const mapStateToProps = state => ({...state})
export default connect(mapStateToProps,
  {
    push,
    getFormData,
    FormSubmission
  })(UserForm)
