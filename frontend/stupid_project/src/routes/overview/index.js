import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Row, Col, Table, Button, Icon, Upload, Input, Select, message, Collapse, Checkbox, Alert, Modal, Popconfirm} from 'antd';
import { camelize } from 'humps'
import { push } from 'react-router-redux'
import ProperCellRenderer from '../components/tableCellComponent'
import { isEqual } from "lodash";

import { fetchDataInTable, setSelectedColumn, exportCurrentDataToXLSX, DeleteSelectData, DeleteCheckedData, EditCheckedData, EditSelectData } from '../../actions/centralData'
import { setTableProp, UpdatedData } from '../../actions/overview'
import DataSource from "../components/dataSource"
import '../../css/main.css'

const Panel = Collapse.Panel
const CheckboxGroup = Checkbox.Group
const InputGroup = Input.Group
const confirm = Modal.confirm
const Option = Select.Option

class DataOverview extends Component {
  state = {
    selectedColumn:this.props.centralData.columns.map((item)=>camelize(item)),
    indeterminate: true,
    selectAllColumn: true,
    dataSourceModal: false,
	selectedRowKeys: [],
	showModifiedModal: false,
	modifyList:[],
	modifyFrame:true,
  }
  fetchDataInTable = (tableName, opt) => this.props.fetchDataInTable(tableName, opt.currentPage,{
    sortby: opt.sortby,
    orderby: opt.orderby
  })
  onSelectColumn = (selectedColumn) => {
    this.setState({
      selectedColumn,
      indeterminate: !!selectedColumn.length && (selectedColumn.length < this.props.centralData.allPossibleColumn.length),
      selectAllColumn: selectedColumn.length ===  this.props.centralData.allPossibleColumn.length,
    });
  }
  selectAllColumnChange = (e) => {
    this.setState({
      selectedColumn: e.target.checked ? this.props.centralData.allPossibleColumn.map((item)=>(camelize(item))) : [],
      indeterminate: false,
      selectAllColumn: e.target.checked,
    });
  }
  FilterColumns = () =>{
    let pair = {}
    this.props.centralData.allPossibleColumn.map((item)=>{
      pair[camelize(item)] = item
    })
    this.props.setSelectedColumn(this.props.params.tableName,this.state.selectedColumn.map((columnName)=>({"0":pair[columnName]})))
    .then(()=>this.fetchDataInTable(this.props.params.tableName, this.props.dataOverview))
  }
  componentWillMount(){
    this.componentWillReceiveProps(this.props)
  }
  componentWillReceiveProps(newProps){
    if (newProps.dataOverview.mustUpdateData){
      this.fetchDataInTable(newProps.params.tableName, newProps.dataOverview)
      this.props.UpdatedData()
    }
    this.state.selectedColumn=newProps.centralData.columns.map((item)=>camelize(item))
  }
  handleChange = (pagination, filters, sorter) =>{
    let nextState = {}
    let pair = {}
    this.props.centralData.allPossibleColumn.map((item)=>{
      pair[camelize(item)] = item
    })
    if (sorter.column != undefined) {
      nextState.sortby = `${pair[typeof(sorter.column.title) == "string"? sorter.column.title: sorter.column.title.props.children[0]]}`
      nextState.orderby = sorter.order
    }
    if (pagination.current != this.props.dataOverview.currentPage) {
      nextState.currentPage = pagination.current
    }
    this.props.setTableProp(nextState)
  }
  onSelectedRowKeysChange = (selectedRowKeys) => {
    this.setState({ selectedRowKeys });
  }
  showModifiedModalFrame = () => {
    this.setState({
      showModifiedModal: true,
	  modifyFrame:true,
    });
  }
  showModifiedModalSelect = () => {
    this.setState({
      showModifiedModal: true,
	  modifyFrame:false,
    });
  }
  closeModifiedModal = () => {
    this.setState({
      showModifiedModal: false,
    });
  }
  handleModify = () => {
	const publicThis = this
	confirm({
      title: 'Are you sure?',
      content: 'Selected items will be modified according to your preference',
      onOk() {
        publicThis.setState({
          showModifiedModal: false,
        },() => {
			var data={
				idList: publicThis.state.selectedRowKeys,
				modifyList : publicThis.state.modifyList.filter((modify)=>(modify.columnName != ""))
			};
			var editFunction = publicThis.props.EditCheckedData
			if(publicThis.state.modifyFrame)
				editFunction = publicThis.props.EditSelectData
			editFunction(publicThis.props.params.tableName,JSON.stringify(data))
			  .then(()=>{
				message.success("Modified data");
				publicThis.fetchDataInTable(publicThis.props.params.tableName, publicThis.props.dataOverview);
			  })
		  }
		);
      },
      onCancel() {},
    });
  }
  render() {
    const publicThis = this
    const DataSourceButton = (
      <Button type="dash" onClick={()=>this.setState({dataSourceModal:true})}>
        <Icon type="database" />Datasource
      </Button>
    )
    const { selectedRowKeys } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectedRowKeysChange,
    };
    return (this.props.centralData.error && /FETCH_DATA_IN_TABLE/.test(this.props.centralData.err_msg))? <Alert type="error" message={this.props.centralData.err_msg} />:
    this.props.centralData.data.length > 0 ?(
      <div>
        <Modal visible={this.state.dataSourceModal} title="Datasource" footer={null} onCancel={()=>this.setState({dataSourceModal:false})} onOk={null}>
          <DataSource tableName={this.props.params.tableName}/>
        </Modal>
        <Row>
          <Col span={10} style={{marginTop:20}}>
            <Collapse>
              <Panel header={<label className="blackConsole">Filter</label>}>
                <label className="blackConsole">Display data in following column:</label>
                <div style={{ borderBottom: '1px solid #E9E9E9' }}>
                  <Checkbox
                    indeterminate={this.state.indeterminate}
                    onChange={this.selectAllColumnChange}
                    checked={this.state.selectAllColumn}
                  >
                    Display all
                  </Checkbox>
                </div>
                <br />
                <CheckboxGroup options={this.props.centralData.allPossibleColumn.map((item)=>(camelize(item)))} value={this.state.selectedColumn} onChange={this.onSelectColumn} />
                <Row justify="end" type="flex">
                  <Button onClick={this.FilterColumns} type="dashed">Set filter</Button>
                </Row>
              </Panel>
            </Collapse>
          </Col>
        </Row>
        <Row>
          <Col xs={24}>
            <Row justify="space-between" align="middle" type="flex" style={{marginTop:20}}>
              <Col>
                <label style={{marginRight:50}}>{`Data Count: ${this.props.centralData.dataCount}`}</label>
                {DataSourceButton}
              </Col>
              <Col xs={7}>
                <Row justify="space-between" type="flex">
                  <Col>
                    <Button type="primary" onClick={()=>this.props.exportCurrentDataToXLSX(this.props.params.tableName, {
                      sortby: this.props.dataOverview.sortby,
                      orderby: this.props.dataOverview.orderby
                    })}>
                      <Icon type="download" /> Export to file <Icon type="file-excel" />
                    </Button>
				          </Col>
				  { this.state.selectedRowKeys.length === 0 ? /* Determine delete and modify button button */
                  <Col>
                    <Row justify="center" type="flex">
                      <Button style={{width:"100%"}} type="dashed" onClick={this.showModifiedModalFrame}>Modify this data frame<Icon type="edit" /></Button>
                    </Row>
                    <Row justify="center" type="flex" style={{marginTop:10}}>
                      <Popconfirm title="Are you sure delete this data frame?" onConfirm={()=>{
                        this.props.DeleteSelectData(this.props.params.tableName)
                        .then(()=>{
                          message.success("Deleted data frame.")
                          this.fetchDataInTable(this.props.params.tableName, this.props.dataOverview)
                        })
                      }} onCancel={()=>{}} okText="Yes" cancelText="No">
                        <Button style={{width:"100%"}} type="danger" onClick={()=>{}}>
                          Delete this data frame <Icon type="delete" />
                        </Button>
                      </Popconfirm>
                    </Row>
                  </Col> :
                  <Col>
                    <Row justify="center" type="flex">
                      <Button style={{width:"100%"}} type="dashed" onClick={this.showModifiedModalSelect}>Modify selected <Icon type="edit" /></Button>
                    </Row>
                    <Row justify="center" type="flex" style={{marginTop:10}}>
                      <Popconfirm title="Are you sure delete selected data?" onConfirm={()=>{
                        this.props.DeleteCheckedData(this.props.params.tableName,this.state.selectedRowKeys.join(','))
                        .then(()=>{
                          message.success("Deleted selected data")
                          this.fetchDataInTable(this.props.params.tableName, this.props.dataOverview)
                        })
                      }} onCancel={()=>{}} okText="Yes" cancelText="No">
                        <Button style={{width:"100%"}} type="danger" onClick={()=>{}}>
                          Delete selected<Icon type="delete" />
                        </Button>
                      </Popconfirm>
                    </Row>
                  </Col>
				  }
                </Row>
              </Col>
            </Row>
          </Col>
        </Row>
        <Table
          rowSelection={rowSelection}
          style={{ marginTop: 15 }}
          columns={this.props.centralData.columns.map((name)=>({
            title:name,
            dataIndex:camelize(name),
            sorter: true,
            sortOrder:(camelize(this.props.dataOverview.sortby) === name) && (this.props.dataOverview.orderby),
            render: (value) => <ProperCellRenderer value={value} columnName={name}/>
          }))}
          dataSource={this.props.centralData.data.map((row=>{
            row.key=row.id
            return row
          }))}
          pagination={{
            pageSize: 20,
            current:this.props.dataOverview.currentPage,
            total: this.props.centralData.dataCount
          }}
          onChange={this.handleChange}
        >
      </Table>
	  <Modal
        title="Modify selected data"
        visible={this.state.showModifiedModal}
        onOk={this.handleModify}
        onCancel={this.closeModifiedModal}
	  >
        <Row justify="space-between" type="flex">
          <Button type="primary" onClick={()=>this.setState({modifyList:this.state.modifyList.concat({columnName:"",value:""})})}>
            <Icon type="plus" />
            Add column to modify
          </Button>
		</Row>
		{
		  this.state.modifyList.map((condition, index) => (
		  <InputGroup style={{marginTop:10}}>
        <Col span={9}>
          <Select showSearch onChange={(value)=>{
            this.state.modifyList[index].columnName = value
            this.forceUpdate()
            }} placeholder="Column name" style={{width:"100%"}} value={condition.columnName} mode="combobox">
          {
            this.props.centralData.allPossibleColumn.map((name)=><Option value={name} key={Math.random()}>{camelize(name)}</Option>)
          }
          </Select>
        </Col>
        <Col span={4} style={{textAlign:"center"}}>=</Col>
        <Col span={8}>
          <Input onChange={(e)=>{
            this.state.modifyList[index].value = e.target.value
            this.forceUpdate()
            }} value={condition.value}/>
        </Col>
        <Col span={3}>
          <Button type="danger" shape="circle" icon="close" onClick={()=>{
            this.state.modifyList.splice(index,1)
            this.forceUpdate()
            }}/>
        </Col>
		  </InputGroup>
		  ))
		}
      </Modal>
    </div>):(<div>
      <Modal visible={this.state.dataSourceModal} title="Datasource" footer={null} onCancel={()=>this.setState({dataSourceModal:false})} onOk={null}>
        <DataSource tableName={this.props.params.tableName}/>
      </Modal>
      {DataSourceButton}
      <h3 className="blackConsole">No Data</h3>
    </div>)
  }
}

const mapStateToProps = state => ({...state})
export default connect(mapStateToProps,
  {
    push,
    fetchDataInTable,
    setTableProp,
    UpdatedData,
    setSelectedColumn,
    exportCurrentDataToXLSX,
    DeleteSelectData,
	DeleteCheckedData,
	EditCheckedData,
	EditSelectData
  })(DataOverview)
