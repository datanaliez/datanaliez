import React, { Component } from 'react'
import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import { Breadcrumb, Icon, Alert, message, Row, Col, Spin } from 'antd'
import sha256 from 'sha256';

import Form from './components/loginForm'
import Background from '../media/background.jpg'
import {
  Login,
  ServerGreeting,
  CheckLoginStatus
} from '../actions/login'
import '../css/main.css'
class LoginPage extends Component {
  componentWillMount(){
    this.props.CheckLoginStatus()
    this.props.ServerGreeting()
  }
  onFormSubmit = (values) => {
    values.password = sha256(sha256(values.password)+this.props.challenge)
    this.props.Login(values)
  }
  componentWillReceiveProps(newProps){
    if (newProps.LoggedIn == true ){
      message.success('You have logged in.')
      newProps.push(`/notify`)
    }
  }
  render() {
    const container = (
      <div style={{backgroundImage:`url(${Background})`, backgroundPosition:"center", backgroundSize:"cover"}}>
        <Row justify="center" type="flex" style={{paddingTop:"15%", textAlign:"left"}}>
          <Col span={5}>
            {(this.props.error)?<Alert type="error" message={this.props.err_msg} />:(<div></div>)}
            <h1 className="white">User Authentication</h1>
            <hr />
          </Col>
        </Row>
        <Row justify="center" type="flex" style={{marginTop:20,paddingBottom:"20%"}}>
          <Form
            onFormSubmit={this.onFormSubmit}
          />
        </Row>
      </div>
    )
    return this.props.loading?(
      <Spin tip="Loading..." disabled>
        {container}
      </Spin>
    ):(container)
  }
}

const mapStateToProps = state => (
  {
    ...state.application
  }
)
export default connect(mapStateToProps,
  {
    Login,
    ServerGreeting,
    CheckLoginStatus,
    push
  })(LoginPage)
