import React, { Component } from 'react'
import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import { message, Spin } from 'antd'
import {Logout} from '../actions/login'
import '../css/main.css'

class LoggingOut extends Component {
  componentWillMount(){
    this.props.Logout()
  }
  componentWillReceiveProps(newProps){
    if(!newProps.application.LoggedIn){
      message.success('You have logged out.')
      newProps.push("/")
    }
  }
  render() {
    return (<Spin tip="Loading..." disabled/>)
  }
}

const mapStateToProps = state => ({...state})
export default connect(mapStateToProps,
  {
    push,
    Logout
  })(LoggingOut)
