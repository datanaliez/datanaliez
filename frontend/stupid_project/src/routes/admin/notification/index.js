import React, { Component } from 'react'
import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import { Table, Button, Icon, Popconfirm, Spin, message, Modal } from 'antd'

import {
  fetchNotification
} from '../../../actions/notification'
import {
  deleteNotification,
  editNotification,
  newNotification
} from '../../../actions/admin'
import '../../../css/main.css'
import EditableCell from "../../components/editableCell"
import NewNotificationForm from '../../components/newNotificationForm'

const { Column } = Table;

class NotificationControll extends Component {
  state={
    modal:false
  }
  componentWillMount(){
    this.props.fetchNotification()
  }
  componentWillReceiveProps(newProps){
    // ...
  }
  deleteNotification = (ID) => {
    this.props.deleteNotification(ID)
    .then(() => {
      this.props.fetchNotification()
      message.success('Notification #'+ID+' deleted')
    })
    .catch((err) =>message.error(`${err}`))
  }
  deleteButton = (text, record) => (
    <Popconfirm title="Are you sure delete this notification?" onConfirm={() =>this.deleteNotification(record.iD)} okText="Yes" cancelText="No">
      <Button type="danger">
        <Icon type="delete" />Delete
      </Button>
    </Popconfirm>
  )
  ModifyNotification = (rowID, columnName, value) => {
    this.props.editNotification(rowID, {"columnName":columnName,"value":value})
    .then(() => {
      this.props.fetchNotification()
      message.success(`Modified notification ${rowID} column named '${columnName}' to "${value}"`)
    })
    .catch((err) =>message.error(`${err}`))
  }
  NewNotification = (value) => {
    this.props.newNotification(value)
    .then(() => {
      this.props.fetchNotification()
      message.success(`New notification was created`)
    })
    .catch((err) => {
      message.error(`${err}`)
    })
    this.setState({modal:false})
  }
  render() {
    const container = (
      <div>
        <Button shape="circle" icon="plus" style={{marginLeft:20}} onClick={()=>this.setState({modal:true})}/>
        <Modal visible={this.state.modal} title="Add notification" footer={null} onCancel={()=>this.setState({modal:false})} onOk={null}>
          <NewNotificationForm onFormSubmit={this.NewNotification} initialValues={{priority:"0", body:null, headline:null}}/>
        </Modal>
        <Table dataSource={this.props.notification.notification_list} style={{marginTop:10}}>
          <Column title="ID" dataIndex="iD" key="ID" width="5%" />
          <Column title="Title" dataIndex="headline" key="headline" width="10%" render={(text, record)=>(<EditableCell type="textArea" value={text} ModifyFunc={this.ModifyNotification} modifyParam={{rowID:record.iD, columnName:"headline"}}/>)}/>
          <Column title="Content" dataIndex="body" key="body" width="50%" render={(text, record)=>(<EditableCell type="textArea" value={text} ModifyFunc={this.ModifyNotification} modifyParam={{rowID:record.iD, columnName:"body"}}/>)}/>
          <Column title="Priority" dataIndex="priority" key="priority" render={(text, record)=>(<EditableCell type="select" value={text} possibleValue={["0","1","2"]} keyPair={{"0":"Normal","1":(<strong>Warning</strong>),"2":(<strong style={{color:"red"}}>Critical</strong>)}} ModifyFunc={this.ModifyNotification} modifyParam={{rowID:record.iD, columnName:"priority"}}/>)}/>
          <Column title="Public Date" dataIndex="publicDate" key="public_date" width="15%" />
          <Column title="Operation" render={this.deleteButton} width="7%"/>
        </Table>
      </div>)
    return (this.props.notification.loading || this.props.administration.loading)?
    (<Spin tip="Loading..." disabled>
      {container}
    </Spin>):container
  }
}

const mapStateToProps = state => ({...state})
export default connect(mapStateToProps,
  {
    push,
    fetchNotification,
    deleteNotification,
    editNotification,
    newNotification
  })(NotificationControll)
