import React, { Component } from 'react'
import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import { Table, Button, Icon, Popconfirm, Spin, message, Modal } from 'antd'
import sha256 from 'sha256'

import {
  deleteUser,
  editUser,
  newUser,
  fetchUser
} from '../../../actions/admin'
import '../../../css/main.css'
import EditableCell from "../../components/editableCell"
import NewUserForm from '../../components/newUserForm'

const { Column } = Table;

class UserControll extends Component {
  state={
    modal:false
  }
  componentWillMount(){
    this.props.fetchUser()
  }
  componentWillReceiveProps(newProps){
    // ...
  }
  deleteUser = (ID) => {
    this.props.deleteUser(ID)
    .then(() => {
      this.props.fetchUser()
      message.success('User #'+ID+' deleted')
    })
    .catch((err) =>message.error(`${err}`))
  }
  deleteButton = (text, record) => (
    <Popconfirm title="Are you sure delete this user?" onConfirm={() =>this.deleteUser(record.iD)} okText="Yes" cancelText="No">
      <Button type="danger">
        <Icon type="delete" />Delete
      </Button>
    </Popconfirm>
  )
  ModifyUser = (rowID, columnName, value) => {
    if(columnName == "password"){
      value = sha256(value)
    }
    this.props.editUser(rowID, {"columnName":columnName,"value":value})
    .then(() => {
      this.props.fetchUser()
      if(columnName == "password"){
        message.success(`Password was modified`)
      }else{
        message.success(`Modified User #${rowID} column named '${columnName}' to "${value}"`)
      }
    })
    .catch((err) =>message.error(`${err}`))
  }
  NewUser = (value) => {
    value.password = sha256(value.password)
    this.props.newUser(value)
    .then(() => {
      this.props.fetchUser()
      message.success(`New user was created`)
    })
    .catch((err) => {
      message.error(`${err}`)
    })
    this.setState({modal:false})
  }
  render() {
    const container = (
      <div>
        <Button shape="circle" icon="plus" style={{marginLeft:20}} onClick={()=>this.setState({modal:true})}/>
        <Modal visible={this.state.modal} title="Add User" footer={null} onCancel={()=>this.setState({modal:false})} onOk={null}>
          <NewUserForm onFormSubmit={this.NewUser} initialValues={{permission:"0", password:null, username:null}}/>
        </Modal>
        <Table dataSource={this.props.administration.user_list} style={{marginTop:10}}>
          <Column title="ID" dataIndex="iD" key="ID"  />
          <Column title="Username" dataIndex="username" key="username" />
          <Column title="Password" key="pwd" render={(text, record)=>(<EditableCell type="password" value="*********" ModifyFunc={this.ModifyUser} modifyParam={{rowID:record.iD, columnName:"password"}}/>)}/>
          <Column title="Permission" dataIndex="permission" key="permission" render={(text, record)=>(<EditableCell type="select" value={text} possibleValue={["0","1","2"]} keyPair={{"0":"Guest","1":"User","2":"Administrator"}} ModifyFunc={this.ModifyUser} modifyParam={{rowID:record.iD, columnName:"permission"}}/>)}/>
          <Column title="Operation" render={this.deleteButton} />
        </Table>
      </div>)
    return (this.props.administration.loading)?
    (<Spin tip="Loading..." disabled>
      {container}
    </Spin>):container
  }
}

const mapStateToProps = state => ({...state})
export default connect(mapStateToProps,
  {
    push,
    fetchUser,
    deleteUser,
    editUser,
    newUser
  })(UserControll)
