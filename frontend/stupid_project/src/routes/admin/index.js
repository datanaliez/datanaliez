import React, { Component } from 'react'
import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import { Tabs } from 'antd'

import '../../css/main.css'

const TabPane = Tabs.TabPane;

class Admin extends Component {
  render() {
    const path = this.props.location.pathname.split("/")
    return (<div style={{marginTop:20}}>
      <Tabs defaultActiveKey={path[path.length - 1]} onChange={(key) => this.props.push(`/admin/${key}`)} type="card">
        <TabPane tab="Notification" key="notify">
          { path[path.length - 1] == "notify" ? this.props.children : (<div></div>) }
        </TabPane>
        <TabPane tab="User controll" key="users">
          { path[path.length - 1] == "users" ? this.props.children : (<div></div>) }
        </TabPane>
      </Tabs>
    </div>)
  }
}

const mapStateToProps = state => ({...state})
export default connect(mapStateToProps,
  {
    push
  })(Admin)
