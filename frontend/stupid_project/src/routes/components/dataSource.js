import React, { Component } from 'react'
import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import { Row, Col, Button, Icon, Upload, message, Spin, Popconfirm} from 'antd';
import { Link } from 'react-router'

import {
  fetchTableForm,
  deleteForm
} from '../../actions/form'
import { fetchDataInTable, fetchColumnNameInTable } from '../../actions/centralData'

class DataSource extends Component {
  componentWillMount(){
    this.props.fetchTableForm(this.props.tableName)
  }
  componentWillReceiveProps(newProps){
    // ...
  }
  fetchDataInTable = (tableName, opt) => this.props.fetchDataInTable(tableName, opt.currentPage,{
    sortby: opt.sortby,
    orderby: opt.orderby
  })
  render() {
    return (
      <div>
        <Row justify="start" align="middle" type="flex">
          <Col>
            <Icon type="check-circle-o" /><label>  Upload .xlsx file:</label>
          </Col>
          <Col>
            <Upload name="dataFile" action={`/api/v1/edit/uploadData/${this.props.tableName}`} onChange={(info)=>{
              if(info.file.status === 'done'){
                message.success(`${info.file.name} file uploaded successfully.`)
                this.props.fetchColumnNameInTable(this.props.tableName)
                .then(()=>{this.fetchDataInTable(this.props.tableName, this.props.dataOverview)})
              }else if (info.file.status === 'error'){
                message.error(`${info.file.name} file upload failed.`)
              }
            }} style={{marginLeft:30}}>
              <Button type="dash">
                <Icon type="upload" />Upload File
              </Button>
            </Upload>
          </Col>
        </Row>
        <Row justify="start" align="middle" type="flex">
          <Col>
            <Icon type="check-circle-o" /><label>  Publish questionnaire form</label>
          </Col>
        </Row>
        <Row justify="start" align="middle" type="flex">
          <Col offset={3}>
          {
            this.props.formControll.loadingState.length > 0 ? <Spin indicator={<Icon type="loading" spin />} />:this.props.formControll.TableForm.map(
              (formInfo)=>(<Row justify="start" align="middle" type="flex" style={{marginTop:20}}>
                <Col>
                  <label>{formInfo.title}</label>
                </Col>
                <Col>
                  <Link to={`/formControll/edit/${formInfo.formID}`} style={{marginLeft:5}}>Edit</Link>
                </Col>
                <Col>
                  <Popconfirm title="Are you sure delete this form?" onConfirm={()=>{
                    this.props.deleteForm(formInfo.formID)
                    .then(()=>this.props.fetchTableForm(this.props.tableName))
                  }} okText="Yes" cancelText="No">
                    <Link style={{marginLeft:5}}>Delete</Link>
                  </Popconfirm>
                </Col>
              </Row>)
            )
          }
            <Row justify="start" align="middle" type="flex" style={{marginTop:20}}>
              <Link to={`/formControll/new/${this.props.tableName}`}>
                <Icon type="plus-circle-o" /> Create new form
              </Link>
            </Row>
          </Col>
        </Row>
      </div>
    )
  }
}

const mapStateToProps = state => ({...state})
export default connect(mapStateToProps,
  {
    push,
    fetchDataInTable,
    fetchColumnNameInTable,
    fetchTableForm,
    deleteForm
  })(DataSource)
