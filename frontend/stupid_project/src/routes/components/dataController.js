import React, { Component } from 'react'
import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import Lodash from "lodash";

import { fetchTableList,
  setFilter,
  MustUpdateData,
  isTableExist,
  createNewTable,
  DropTable,
  setTableName,
  fetchColumnNameInTable,
  reset } from '../../actions/centralData'
import '../../css/main.css'
import { Collapse, Row, Col, Spin, Select, Button, Icon, Input, Modal, Popconfirm, Checkbox } from 'antd';
import { camelize } from 'humps';

const Panel = Collapse.Panel
const Option = Select.Option
const InputGroup = Input.Group
const CheckboxGroup = Checkbox.Group;

const difference = (object, base) => {
	function changes(object, base) {
		return Lodash.transform(object, function(result, value, key) {
			if (!Lodash.isEqual(value, base[key])) {
				result[key] = (Lodash.isObject(value) && Lodash.isObject(base[key])) ? changes(value, base[key]) : value;
			}
		});
	}
	return changes(object, base);
}
const LoadingModalExcepted = /FETCH_IS_TABLE_EXIST|FETCH_COLUMN_TYPE|DOWNLOAD_XLSX|FORM/
class DataController extends Component {
  state = {
    filterList:[],
    modal:false,
    newTableName:"",
    currentTablename:undefined,
    isTableExist:false,
    existLoading:false,
  }
  tableURL = (tableName) => (`${this.props.location.pathname.split("/").slice(0,-1).join("/")}${tableName ? `/${tableName}`:""}`)
  shouldComponentUpdate(newProps){
    if(newProps.params.tableName !== this.state.currentTablename){
      this.state.currentTablename = newProps.params.tableName
      this.props.fetchTableList()
      .then(()=>{
        if(newProps.params.tableName !== undefined){
          this.props.fetchColumnNameInTable(newProps.params.tableName)
          this.state.filterList = JSON.parse(localStorage.getItem(`filter.${this.state.currentTablename}`)) || []
          this.setFilter()
        }
      })
    // }else if(newProps.centralData.loadingState.reduce((result, state)=>result || /FETCH.*DATA/.test(state),false)){
    // // }else if (!Lodash.isEqual(this.props.data, newProps.data) ||
    // //           !Lodash.isEqual(this.props.histogramData, newProps.histogramData) ){
    //   console.log("DataController: Nope.")
    //   return false
    }else if(newProps.params.tableName === undefined && !(newProps.centralData.loadingState.includes("FETCH_TABLELIST"))){
      if(newProps.centralData.tableList.length != 0){
        newProps.push(`${newProps.location.pathname}/${newProps.centralData.tableList[0].tablename}`)
      }
    }
    return true
  }
  componentWillMount(){
    this.props.fetchTableList()
  }
  switchTable = (tableName) => {
    this.state.filterList = []
    this.props.push(this.tableURL(tableName))
  }
  newTable = () => {
    this.setState({modal:false})
    this.props.createNewTable(this.state.newTableName)
    .then(()=>{
      this.props.push(this.tableURL(this.state.newTableName))
    })
  }
  dropCurrentTable = () => {
    localStorage.removeItem(`filter.${this.state.currentTablename}`)
    this.props.DropTable(this.props.params.tableName)
    .then(()=>{
      this.props.push(this.tableURL())
    })
  }
  setFilter = () => {
    this.state.filterList = this.state.filterList.map((condition)=>({
      ...condition,
      // value:condition.expression != "like"?condition.value = parseFloat(condition.value):condition.value
    })).filter((condition)=>(condition.columnName != "" && condition.expression != ""))
    localStorage.setItem(`filter.${this.state.currentTablename}`, JSON.stringify(this.state.filterList))
    this.props.setFilter(this.state.currentTablename,this.state.filterList)
    .then(()=>this.props.MustUpdateData())
  }
  render() {
    // Refactor Here
    return this.props.centralData.loadingState.filter((state)=>!(LoadingModalExcepted.test(state))).length > 0 ? <Modal visible={true} footer={null} closable={false} width={100}>
      <Row align="center" type="flex">
        <Col >
          <Spin tip="Loading..."/>
        </Col>
      </Row>
    </Modal>:(<div>
        <Modal visible={this.state.modal || this.props.centralData.tableList.length == 0} title="Add Table" footer={null} onCancel={()=>this.setState({modal:false})} onOk={null}>
          <Input onChange={(e)=>{this.props.isTableExist(e.target.value);this.setState({newTableName:e.target.value})}}
                onPressEnter={()=> this.props.centralData.isTableExist ||
                  this.props.centralData.loadingState.filter((state)=>(/FETCH_IS_TABLE_EXIST/.test(state))).length > 0 ?
                 function(){}:this.newTable()
                }/>
          {
            this.props.centralData.isTableExist &&
            !this.props.centralData.loadingState.filter((state)=>(/FETCH_IS_TABLE_EXIST/.test(state))).length > 0 ? <span style={{ color: 'red' }}>Table name exist</span>:null
          }
          {
            /\//.test(this.state.newTableName)? <span style={{ color: 'red' }}>Table name can't contain "/" character</span>:null
          }
          <Row justify="end" type="flex" style={{marginTop:20}}>
            <Button type="dashed" disabled={this.props.centralData.isTableExist ||
               this.props.centralData.loadingState.filter((state)=>(/FETCH_IS_TABLE_EXIST/.test(state))).length > 0} onClick={this.newTable}>
              <Icon type="plus" />
              Submit
            </Button>
          </Row>
        </Modal>
        <Row justify="center" type="flex" style={{marginTop:20}}>
          <Col span={24}>
            <label className="blackConsole">Table:</label>
            <Select showSearch defaultValue={this.props.params.tableName} onChange={this.switchTable} style={{width:300}} >
            {
              this.props.centralData.tableList.map((value) => (<Option value={value.tablename} key={Math.random()}>{value.tablename}</Option>))
            }
            </Select>
            <Popconfirm title="Are you sure delete this table?" onConfirm={this.dropCurrentTable} okText="Yes" cancelText="No">
              <Button style={{marginLeft:20}} type="danger"><Icon type="close" /> Drop Table</Button>
            </Popconfirm>
            <Button shape="circle" icon="plus" style={{marginLeft:20}} onClick={()=>this.setState({modal:true})}/>
          </Col>
        </Row>
        <Row justify="left" type="flex" style={{marginTop:20}}>
          <Col span={16}>
            <Collapse>
              <Panel header={<label className="blackConsole">Condition</label>}>
                <Row justify="space-between" type="flex">
                  <Button type="primary" onClick={()=>this.setState({filterList:this.state.filterList.concat({columnName:"",expression:"",value:""})})}>
                    <Icon type="plus" />
                    Add Condition
                  </Button>
                  {
                    this.state.filterList.length != 0?<Button type="danger" onClick={()=>{
                      this.state.filterList = []
                      this.setFilter()
                    }}>
                      <Icon type="close" />
                      Clear Condition
                    </Button>:<div />
                  }
                </Row>
                {
                  this.state.filterList.map((condition, index) => (
                  <InputGroup style={{marginTop:10}}>
                    <Col span={9}>
                      <Select showSearch onChange={(value)=>{
                        this.state.filterList[index].columnName = value
                        this.forceUpdate()
                      }} placeholder="Column name" style={{width:"100%"}} value={condition.columnName}>
                      {
                        this.props.centralData.allPossibleColumn.map((name)=><Option value={name} key={Math.random()}>{camelize(name)}</Option>)
                      }
                      </Select>
                    </Col>
                    <Col span={4}>
                      <Select showSearch onChange={(value)=>{
                        this.state.filterList[index].expression = value
                        this.forceUpdate()
                      }} placeholder="Expression" style={{width:"100%"}} value={condition.expression}>
                      {
                        [["=","eq"],[">","gt"],["<","lt"],["\u2265","ge"],["\u2264","le"],["LIKE","like"]].map((item)=><Option value={item[1]} key={Math.random()}>{item[0]}</Option>)
                      }
                      </Select>
                    </Col>
                    <Col span={8}>
                      <Input onChange={(e)=>{
                          this.state.filterList[index].value = e.target.value
                        this.forceUpdate()
                      }} value={condition.value}/>
                    </Col>
                    <Col span={3}>
                      <Button type="danger" shape="circle" icon="close" onClick={()=>{
                        this.state.filterList.splice(index,1)
                        this.forceUpdate()
                      }}/>
                    </Col>
                  </InputGroup>
                  ))
                }
                {
                  this.state.filterList.length > 0 ?
                  (<Row justify="end" type="flex" style={{marginTop:20}}>
                    <Button type="primary" onClick={this.setFilter}>
                      <Icon type="check" />
                      Set Condition
                    </Button>
                  </Row>) : (<div></div>)
                }
              </Panel>
            </Collapse>
          </Col>
        </Row>
        <Row justify="center" type="flex" style={{marginTop:20}}>
          <Col span={24}>
            {this.props.centralData.loadingState.filter((state)=>!(LoadingModalExcepted.test(state))).length? null:this.props.children}
          </Col>
        </Row>
      </div>)
  }
}

const mapStateToProps = state => ({...state})
export default connect(mapStateToProps,
  {
    push,
    fetchTableList,
    setFilter,
    MustUpdateData,
    isTableExist,
    createNewTable,
    DropTable,
    setTableName,
    fetchColumnNameInTable,
    reset,
  })(DataController)
