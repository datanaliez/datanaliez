import React from 'react'
import { reduxForm, Field } from 'redux-form'
import { Input, Select, Icon, Row, Col } from 'antd'
import 'antd/dist/antd.css'

const Option = Select.Option
const validate = (values) => {
  const errors = ['username', 'password','permission']
    .reduce((errors, field) => (values[field] ? errors : { ...errors, [field]: 'Required' }), {})
  return errors
}
const renderInputText = ({ input, placeholder, type, meta: { touched, error } }) => (
  <div>
    <label style={{fontFamily: '"Wire One", sans-serif'}}>{placeholder}</label> <Input {...input} placeholder={placeholder} type={type}/>
    {touched && error && <span style={{ color: 'red' }}>{'Required'}</span>}
  </div>
)
const prioritySelect = ({ input, placeholder, meta: { touched, error } }) => (
  <div style={{ width: 500, marginTop:10 }}>
    <label style={{fontFamily: '"Wire One", sans-serif'}}>{placeholder}</label>
    <Select {...input} placeholder={placeholder}>
      <Option value="0">Guest</Option>
      <Option value="1">User</Option>
      <Option value="2">Administrator</Option>
    </Select>
    {touched && error && <span style={{ color: 'red' }}>{'Required'}</span>}
  </div>
)
const NewUserForm = ({
  handleSubmit,
  onFormSubmit,
  goBack
}) => (
  <form onSubmit={handleSubmit(onFormSubmit)}>
    <Row justify="center" type="flex">
      <Col span={10}>
        <Field name="username" component={renderInputText} type="text" placeholder="Username" />
      </Col>
      <Col span={4}/>
      <Col span={10}>
        <Field name="password" component={renderInputText} type="password" placeholder="Password"/>
      </Col>
    </Row>
    <Field name="permission" component={prioritySelect} placeholder="Permission" defaultValue="0" />
    <br />
    <button type="submit" style={{ textAlign: 'left', marginRight: 10 }} className="ant-btn ant-btn-primary"><Icon type="user-add" />Add User</button>
  </form>
)
export default reduxForm({
  form: 'NewUserForm',
  validate
})(NewUserForm)
