import React from 'react'
import { reduxForm, Field, FieldArray } from 'redux-form'
import { Input, Select, Icon, Row, Col, Checkbox, DatePicker, InputNumber, Button, Card } from 'antd'
import 'antd/dist/antd.css'

const CheckboxGroup = Checkbox.Group;
const Option = Select.Option;

const validate = (values) => {
  var errors = ['title', 'desc'].reduce((errors, field) => (values[field] ? errors : { ...errors, [field]: 'Required' }), {})
  if((values.autoClose || {}).includeDuration){
    if(values.autoClose.duration === undefined || !(values.autoClose.duration.start < values.autoClose.duration.stop)){
      errors = {...errors, autoClose:{includeDuration:"Duration is incorrect."}}
    }
  }
  if((values.autoClose || {}).includeRegistLimit){
    if(!(values.autoClose.registLimit > 0) || (values.autoClose.registLimit != parseInt(values.autoClose.registLimit))){
      errors = {...errors, autoClose:{includeRegistLimit:"Limit number is incorrect."}}
    }
  }
  if (!(values.fieldList instanceof Array) || values.fieldList.length<1){
    errors = {...errors, other:"Must have at least 1 field"}
  }else{
    errors = values.fieldList.reduce((currentErrors, field, index)=>{
      var newErrors = currentErrors
      if (index == 0){
        newErrors.fieldList = [{}]
      }else{
        newErrors.fieldList.push({})
      }
      if(!field.name){
        newErrors.fieldList[index].name = "Required"
      }else{
        if (/\s|^_id$/.test(field.name)){
          newErrors.fieldList[index].name = "Can't contain space and can't = '_id'"
        }else if(values.fieldList.reduce((isExist, eachField, testingIndex) =>
            isExist||(eachField.name == field.name && testingIndex != index), false)){
          newErrors.fieldList[index].name = "Can't duplicate field name"
        }
      }
      if(!field.label){
        newErrors.fieldList[index].label = "Required"
      }
      if(!field.type){
        newErrors.fieldList[index].type = "Required"
      }
      if(["checkbox","choice"].includes(field.type)){
        if(!(field.option instanceof Array) || field.option.length < 1){
          newErrors.fieldList[index].type = "Option must have at least one options"
        }
      }
      return newErrors
    }, errors)
  }
  // Validate data format
  // Validate for required field
  return errors
}
const renderNormalInput = ({placeholder, style, input, meta: { touched, error }})=> (
  <Input {...input} placeholder={placeholder} style={{marginTop:5, ...style}}/>
)
const renderNumberInput = ({placeholder, style, input})=> (
  <InputNumber {...input} min={1} defaultValue={0} placeholder={placeholder} style={{marginTop:5, ...style}}/>
)
const renderInputText = ({ style, placeholder, inputSize, input, label, labelStyle, inputType, meta: { touched, error } }) => (
  <div style={{marginTop:10, ...style}}>
    <Row justify="space-between" type="flex" align="middle">
      <Col>
        <strong style={{...labelStyle, fontFamily: '"Wire One", sans-serif', color:(touched && error)?"red":null}}>{label}</strong>
      </Col>
      <Col xs={14}>
        <Input {...input} {...{placeholder, size:inputSize, type:inputType}} style={{width:"100%", borderColor:(touched && error)?"red":null}}/>
      </Col>
    </Row>
    <Row justify="left" type="flex" align="middle">
      {touched && error && <span style={{ color: 'red' }}>{'Required'}</span>}
    </Row>
  </div>
)

const renderTextarea = ({ style, placeholder, input, label, labelStyle, inputType, meta: { touched, error } }) => (
  <div style={{marginTop:10, ...style}}>
    <Row justify="space-between" type="flex" align="middle">
      <Col>
        <strong style={{...labelStyle, fontFamily: '"Wire One", sans-serif', color:(touched && error)?"red":null}}>{label}</strong>
      </Col>
      <Col xs={14}>
        <Input.TextArea {...input} autosize={true} {...{placeholder}} style={{width:"100%", borderColor:(touched && error)?"red":null}} />
      </Col>
    </Row>
    <Row justify="left" type="flex" align="middle">
      {touched && error && <span style={{ color: 'red' }}>{'Required'}</span>}
    </Row>
  </div>
)
const renderInputSelect = ({ style, placeholder, input, label, tableColumns, meta: { touched, error } }) => (
  <div style={{marginTop:10, ...style}}>
    <Row justify="space-between" type="flex" align="middle">
      <Col>
        <strong style={{fontFamily: '"Wire One", sans-serif', color:(touched && error)?"red":null}}>{label}</strong>
      </Col>
      <Col xs={14}>
        <Select
          {...input}
          style={{width:"100%", borderColor:(touched && error)?"red":null}}
          placeholder={placeholder}
          mode="combobox"
          filterOption={true}
        >
          {
            tableColumns.map((columnName)=>(<Option value={columnName}>{columnName}</Option>))
          }
        </Select>
      </Col>
    </Row>
  </div>
)
const renderDatePicker = ({ input, placeholder, style, meta: { touched, error } }) => (
  <DatePicker showTime format="YYYY-MM-DD HH:mm:ss" placeholder={placeholder}
    disabledDate={(date)=>{
      var currentDate = new Date()
      currentDate.setDate(currentDate.getDate()-1)
      return date<currentDate
    }}
    style={style} {...input} />
)
const renderAutoCloseOnDuration = ({ input, meta: { touched, error } }) => (
  <Checkbox {...input} style={{width:"100%"}} >
    {touched && error? <label style={{ color: 'red' }}>{`Duration: ${error}`}</label>:<label>Duration</label>}
    <Row justify="center" type="flex">
      <Field name="autoClose.duration.start" placeholder="Allowed since " component={renderDatePicker} />
    </Row>
    <Row justify="center" type="flex" style={{marginTop:5}}>
      <strong> To </strong>
    </Row>
    <Row justify="center" type="flex" style={{marginTop:5}}>
      <Field name="autoClose.duration.stop" placeholder="Allowed until " component={renderDatePicker}/>
    </Row>
  </Checkbox>
)
const renderAutoCloseOnRegislimit = ({ input, meta: { touched, error } }) => (
  <Checkbox {...input} style={{width:"100%"}}>
  {touched && error? <label style={{ color: 'red' }}>{`Registration count limit: ${error}`}</label>:<label>Registration count limit</label>}
    <Row justify="center" type="flex">
      <Field name="autoClose.registLimit" placeholder="limit" component={renderNumberInput} style={{width:100}} />
    </Row>
  </Checkbox>
)
const renderTypeSelection = ({style, input, meta: { touched, error }})=> (
  <div style={{marginTop:10, ...style}}>
    <Row justify="space-between" type="flex" align="middle">
      <Col>
        <strong style={{fontFamily: '"Wire One", sans-serif', color:(touched && error)?"red":null}}>Data Type</strong>
      </Col>
      <Col xs={14}>
        <Select placeholder="Data type" style={{ width: "100%", borderColor:(touched && error)?"red":null }} {...input}>
          <Option value="string">Text</Option>
          <Option value="lg_string">Large Text</Option>
          <Option value="integer">Integer</Option>
          <Option value="number">Decimal</Option>
          <Option value="checkbox">Check List</Option>
          <Option value="choice">Choice</Option>
        </Select>
      </Col>
    </Row>
    <Row justify="left" type="flex" align="middle">
      {touched && error && <span style={{ color: 'red' }}>{error}</span>}
    </Row>
  </div>
)
const renderIsRequired = ({input})=> (
  <Checkbox {...input}>
    <label>Required</label>
  </Checkbox>
)
const renderChoiceOptions = ({ fields, input }) => (
  <div style={{marginTop:10}}>
    <Row justify="start" type="flex" align="middle">
      <strong style={{fontFamily: '"Wire One", sans-serif'}}>Options:</strong>
    </Row>
    <Row justify="center" type="flex">
      <Col offset={1} xs={23}>
      {
        fields.map((Option, index)=>(
          <Row justify="space-between" type="flex" align="middle">
            <Col xs={20}>
              <Field
                key={index}
                name={Option}
                component={renderNormalInput}
                placeholder="Value"
                />
            </Col>
            <Col xs={2}>
              <Button icon="close" shape="circle" type="danger" onClick={()=>fields.remove(index)}/>
            </Col>
          </Row>
        ))
      }
      </Col>
    </Row>
    <Row justify="end" type="flex" style={{marginTop:10}}>
      <Button onClick={()=>fields.push("")}>
        <Icon type="plus"/>
        New option
      </Button>
    </Row>
  </div>
)
const renderFormFields = ({ fields, input, tableColumns }) => (
  <Row justify="end" type="flex">
    {
      fields.map((fieldConfig, index)=>(
        <Card title={
            <Field
              name={`${fieldConfig}.name`}
              component={renderInputSelect}
              label="Data for column:"
              placeholder="Can't contain space and can't = '_id'"
              tableColumns={tableColumns.filter(column=>!fields.getAll().map(field=>field.name).includes(column))}
              style={{marginTop:5}}
            />
        } style={{ width: "100%", marginTop:20 }} key={index} extra={
          <div>
            <Button icon="minus" shape="circle" type="danger" onClick={()=>fields.remove(index)}/>
            {
              (index > 0) ? <Button icon="arrow-up" shape="circle" onClick={()=>fields.move(index, index-1)}/>:null
            }
            {
              (index < fields.length-1) ? <Button icon="arrow-down" shape="circle" onClick={()=>fields.move(index, index+1)}/>:null
            }
          </div>
        }>
          <Field
              name={`${fieldConfig}.required`}
              component={renderIsRequired}
              style={{marginTop:5}}
              type="checkbox"
            />
          <Field
              name={`${fieldConfig}.label`}
              component={renderInputText}
              label="Label"
              placeholder="Label"
              style={{marginTop:5}}
            />
          <Field
              name={`${fieldConfig}.desc`}
              component={renderTextarea}
              label="Description"
              placeholder="Description"
              style={{marginTop:5}}
            />
          <Field
              name={`${fieldConfig}.type`}
              component={renderTypeSelection}
              style={{marginTop:5}}
              type="select"
            />
          {
            ["integer", "number", "string", "lg_string"].includes(fields.get(index).type)?
              <Field
                  name={`${fieldConfig}.validate`}
                  component={renderInputText}
                  label="Regex Validator [Advance, optional]"
                  placeholder="Validator"
                  style={{marginTop:20}}
                />:null
          }
          {
            ["checkbox","choice"].includes(fields.get(index).type)?
              <FieldArray
                name={`${fieldConfig}.option`}
                component={renderChoiceOptions} />:null
          }
        </Card>
      ))
    }
    <Button onClick={()=>fields.push({})} style={{marginTop:20}}>
      <Icon type="file-add"/>
      New field
    </Button>
  </Row>
)
// const renderAutoCloseOnRegist = ({ input, meta: { touched, error } }) => (

// )
const FormForm = ({
  handleSubmit,
  onFormSubmit,
  tableColumns
}) => (
  <form onSubmit={handleSubmit(onFormSubmit)} style={{marginBottom:50}}>
    <Field name="title" component={renderInputText} label="Form title" placeholder="Title" size="large" labelStyle={{fontSize:15}}/>
    <Field name="desc" component={renderTextarea} label="Form description" placeholder="Description" inputSize="large" labelStyle={{fontSize:15}}/>
    <Row justify="space-between" type="flex" align="top" style={{marginTop:10}}>
      <Col xs={6}>
        <strong style={{fontFamily: '"Wire One", sans-serif', fontSize:15}}>Close form automatically when:</strong>
      </Col>
      <Col xs={14}>
        <Row>
          <Col>
            <Field name="autoClose.includeDuration" component={renderAutoCloseOnDuration} type="checkbox"/>
          </Col>
        </Row>
        <Row>
          <Col>
            <Field name="autoClose.includeRegistLimit" component={renderAutoCloseOnRegislimit} type="checkbox"/>
          </Col>
        </Row>
      </Col>
    </Row>
    <Row justify="start" type="flex" align="top" style={{marginTop:10}}>
      <strong style={{fontFamily: '"Wire One", sans-serif', fontSize:15}}>Field list:</strong>
    </Row>
    <Row justify="center" type="flex" align="top" style={{marginTop:10}}>
      <Col xs={18}>
        <FieldArray name="fieldList" component={renderFormFields} tableColumns={tableColumns}/>
      </Col>
    </Row>
    <Field name="other" component={({meta: { error }})=>(error ? (<span style={{ color: 'red' }}>{error}</span>):null)}/>
    <br />
    <Row justify="end" type="flex" style={{marginTop:10}}>
      <button type="submit" style={{ textAlign: 'center', marginRight: 10 }} className="ant-btn ant-btn-primary">Submit</button>
    </Row>
  </form>
)
export default reduxForm({
  form: 'FormForm',
  validate
})(FormForm)
