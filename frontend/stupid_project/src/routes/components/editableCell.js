import React, { Component } from 'react'
import { Input, Icon, Select, Row } from 'antd';

const TextArea = Input.TextArea
const Option = Select.Option
export default class EditableCell extends React.Component {
  state = {
    value: this.props.value,
    isEditing: false,
  }
  handleChange = (e) => {
    const value = typeof(e) == "string"? e:e.target.value;
    this.setState({ value });
  }
  check = () => {
    this.setState({ isEditing: false });
    if (this.props.ModifyFunc) {
      this.props.ModifyFunc(this.props.modifyParam.rowID, this.props.modifyParam.columnName, this.state.value);
    }
  }
  edit = () => {
    this.setState({ isEditing: true });
  }
  InputComponent = () => {
    switch (this.props.type) {
      case "textArea":
        return (<TextArea value={this.state.value} onChange={this.handleChange} autosize/>)
      case "select":
        return(<Select defaultValue={this.state.value} onChange={this.handleChange}>
          {
            this.props.possibleValue.map((value) => (<Option value={value}>{this.props.keyPair[value]}</Option>))
          }
        </Select>)
      case "password":
        return (<Input type="password" onChange={this.handleChange} onPressEnter={this.check}/>)
      case "input":
        return (<Input type="text" value={this.state.value} onChange={this.handleChange} onPressEnter={this.check}/>)
      default:
        break;
    }
  }
  render() {
    const { value, isEditing } = this.state;
    return (
      <div className="editable-cell">
        {
          isEditing ?
            <Row type="flex" justify="space-between">
                {this.InputComponent()}
                <a>
                  <Icon
                    type="check"
                    onClick={this.check}
                  />
                </a>
            </Row>
            :
            <Row type="flex" justify="space-between">
              {this.props.keyPair? this.props.keyPair[value] : <div dangerouslySetInnerHTML={{__html:value}} /> || ' '}
              <a>
                <Icon
                  type="edit"
                  onClick={this.edit}
                />
              </a>
            </Row>
        }
      </div>
    );
  }
}
