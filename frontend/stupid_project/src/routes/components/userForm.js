import React from 'react'
import { reduxForm, Field, FieldArray } from 'redux-form'
import { Input, Select, Icon, Row, Col, Checkbox, InputNumber, Button, Radio } from 'antd'
import 'antd/dist/antd.css'

const CheckboxGroup = Checkbox.Group;
const Option = Select.Option;

const renderInputText = ({ input, label, desc, required, meta: { touched, error } }) => (
  <div style={{marginTop:10}}>
    <Row justify="space-between" type="flex" align="middle">
      <Col md={8} xs={24}>
        <strong style={{fontSize:18, color:(touched && error)?"red":null}}>{`${label}${required?"*":""}`}</strong>
        <p style={{color:(touched && error)?"red":null}}>{desc}</p>
      </Col>
      <Col md={14} xs={24}>
        <Input className="user-form-input" {...input} size="large" placeholder={label} style={{width:"100%", borderColor:(touched && error)?"red":null}}/>
      </Col>
    </Row>
    <Row justify="left" type="flex" align="middle">
      {touched && error && <span style={{ color: 'red' }}>{error}</span>}
    </Row>
  </div>
)

const renderLargeInputText = ({ input, label, desc, required, meta: { touched, error } }) => (
  <div style={{marginTop:10}}>
    <Row justify="space-between" type="flex" align="middle">
      <Col md={8} xs={24}>
        <strong style={{fontSize:18, color:(touched && error)?"red":null}}>{`${label}${required?"*":""}`}</strong>
        <p style={{color:(touched && error)?"red":null}}>{desc}</p>
      </Col>
      <Col md={14} xs={24}>
        <Input.TextArea className="user-form-input" autosize={true} {...input} placeholder={label} style={{width:"100%", borderColor:(touched && error)?"red":null}}/>
      </Col>
    </Row>
    <Row justify="left" type="flex" align="middle">
      {touched && error && <span style={{ color: 'red' }}>{error}</span>}
    </Row>
  </div>
)

const renderRadioInput = ({ input, label, desc, required, option, meta: { touched, error } }) => (
  <div style={{marginTop:10}}>
    <Row justify="space-between" type="flex" align="middle">
      <Col md={8} xs={24}>
        <strong style={{fontSize:18, color:(touched && error)?"red":null}}>{`${label}${required?"*":""}`}</strong>
        <p style={{color:(touched && error)?"red":null}}>{desc}</p>
      </Col>
      <Col md={14} xs={24}>
        <Radio.Group className="user-form-input" size="large" {...input} placeholder={label} style={{width:"100%", borderColor:(touched && error)?"red":null}}>
        {
          option.map((opt)=>(<Radio style={{display:"block"}} value={opt.value}>{opt.label}</Radio>))
        }
        </Radio.Group>
      </Col>
    </Row>
    <Row justify="left" type="flex" align="middle">
      {touched && error && <span style={{ color: 'red' }}>{error}</span>}
    </Row>
  </div>
)
const renderCheckListInput = ({ input, label, desc, required, option, meta: { touched, error } }) => (
  <div style={{marginTop:10}}>
    <Row justify="space-between" type="flex" align="middle">
      <Col md={8} xs={24}>
        <strong style={{fontSize:18, color:(touched && error)?"red":null}}>{`${label}${required?"*":""}`}</strong>
        <p style={{color:(touched && error)?"red":null}}>{desc}</p>
      </Col>
      <Col md={14} xs={24}>
        <CheckboxGroup className="user-form-input" {...input} options={option} placeholder={label} style={{width:"100%", borderColor:(touched && error)?"red":null}}/>
      </Col>
    </Row>
    <Row justify="left" type="flex" align="middle">
      {touched && error && <span style={{ color: 'red' }}>{error}</span>}
    </Row>
  </div>
)

const renderInputNumber = ({ input, label, desc, required, meta: { touched, error } }) => (
  <div style={{marginTop:10}}>
    <Row justify="space-between" type="flex" align="middle">
      <Col md={8} xs={24}>
        <strong style={{fontSize:18, color:(touched && error)?"red":null}}>{`${label}${required?"*":""}`}</strong>
        <p style={{color:(touched && error)?"red":null}}>{desc}</p>
      </Col>
      <Col md={14} xs={24}>
        <InputNumber className="user-form-input" defaultValue={0} {...input} placeholder={label} style={{width:"50%", borderColor:(touched && error)?"red":null}}/>
      </Col>
    </Row>
    <Row justify="left" type="flex" align="middle">
      {touched && error && <span style={{ color: 'red' }}>{error}</span>}
    </Row>
  </div>
)

const UserFieldsForm = ({
  handleSubmit,
  onFormSubmit,
  fieldList
}) => (
  <form onSubmit={handleSubmit(onFormSubmit)} style={{marginBottom:50}}>
    {
      fieldList.map((field, index)=>{
        if(field.type == "string"){
          return <Field key={index} component={renderInputText} {...field} validate={undefined}/>
        }else if(field.type == "lg_string"){
          return <Field key={index} component={renderLargeInputText} {...field} validate={undefined}/>
        }else if(field.type == "choice"){
          return <Field key={index} component={renderRadioInput} {...field} validate={undefined}/>
        }else if(field.type == "checkbox"){
          return <Field key={index} component={renderCheckListInput} {...field} validate={undefined}/>
        }else if(field.type == "integer" || field.type == "number"){
          return <Field key={index} component={renderInputNumber} {...field} validate={undefined}/>
        }
      })
    }
    <Row justify="end" type="flex" style={{marginTop:10}}>
      <button type="submit" style={{ textAlign: 'center', marginRight: 10 }} className="ant-btn ant-btn-primary">Submit</button>
    </Row>
  </form>
)
export default reduxForm({
  form: 'UserFieldsForm',
})(UserFieldsForm)
