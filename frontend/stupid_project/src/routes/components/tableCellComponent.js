import React, { Component } from 'react'
import { Tree } from 'antd';

const TreeNode = Tree.TreeNode

const RenderingDecision = ({columnName, value}) => {
  try {
    if(/Object|Array/.test(value.constructor.name)){
      return <Tree showLine>{Renderer({src:{[columnName]:value}, subfield:columnName})}</Tree>
    }
  } catch (error) {
    console.log(error)
  }
  return <label>{`${value}`}</label>
}

const Renderer = ({
  src,
  subfield
}) => {
  const parent = Object.keys(src)[0]
  const children = src[Object.keys(src)[0]]
  if (children != undefined && /Object|Array/.test(children.constructor.name)){
    return (<TreeNode title={parent} key={Math.random()} filterTreeNode={function(){}}>{
      Object.keys(children).map((keys)=>Renderer({src:{[keys]:children[keys]}, callback:childCallback, subfield:`${subfield}.${keys}`}))
    }</TreeNode>)
  }else{
    return <TreeNode title={`${parent}:${JSON.stringify(children)}`} key={Math.random()} filterTreeNode={function(){}} />
  }
}

export default RenderingDecision
