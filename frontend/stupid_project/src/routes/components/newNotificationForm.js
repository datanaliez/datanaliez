import React from 'react'
import { reduxForm, Field } from 'redux-form'
import { Input, Select, Icon } from 'antd'
import 'antd/dist/antd.css'

const Option = Select.Option
const validate = (values) => {
  const errors = ['headline', 'body','priority']
    .reduce((errors, field) => (values[field] ? errors : { ...errors, [field]: 'Required' }), {})
  return errors
}
const renderInputText = ({ input, placeholder, type, meta: { touched, error } }) => (
  <div style={{ width: 500, marginTop:10 }}>
    <label style={{fontFamily: '"Wire One", sans-serif'}}>{placeholder}</label> <Input {...input} placeholder={placeholder} type={type}/>
    {touched && error && <span style={{ color: 'red' }}>{'Required'}</span>}
  </div>
)
const prioritySelect = ({ input, placeholder, meta: { touched, error } }) => (
  <div style={{ width: 500, marginTop:10 }}>
    <label style={{fontFamily: '"Wire One", sans-serif'}}>{placeholder}</label>
    <Select {...input} placeholder={placeholder}>
      <Option value="0">Normal</Option>
      <Option value="1"><strong>Warning</strong></Option>
      <Option value="2"><strong style={{color:"red"}}>Critical</strong></Option>
    </Select>
    {touched && error && <span style={{ color: 'red' }}>{'Required'}</span>}
  </div>
)
const renderTextArea = ({ input, placeholder, type, meta: { touched, error } }) => (
  <div style={{ width: 500, marginTop:10 }}>
    <label style={{fontFamily: '"Wire One", sans-serif'}}>{placeholder}</label> <Input.TextArea {...input} placeholder={placeholder} type={type} autosize/>
    {touched && error && <span style={{ color: 'red' }}>{'Required'}</span>}
  </div>
)
const NewNotificationForm = ({
  handleSubmit,
  onFormSubmit,
  goBack
}) => (
  <form onSubmit={handleSubmit(onFormSubmit)}>
    <Field name="headline" component={renderInputText} type="text" placeholder="Title" />
    <Field name="body" component={renderTextArea} type="text" placeholder="Content <Can insert HTML code in.>" defaultValue="Con"/>
    <Field name="priority" component={prioritySelect} placeholder="Priority" defaultValue="0" />
    <br />
    <button type="submit" style={{ textAlign: 'left', marginRight: 10 }} className="ant-btn ant-btn-primary">Submit</button>
  </form>
)
export default reduxForm({
  form: 'NewNotificationForm',
  validate
})(NewNotificationForm)
