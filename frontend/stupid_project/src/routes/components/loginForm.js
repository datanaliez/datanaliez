import React from 'react'
import { reduxForm, Field } from 'redux-form'
import { Input, Select, Icon } from 'antd'
import 'antd/dist/antd.css'

const validate = (values) => {
  const errors = ['username', 'password']
    .reduce((errors, field) => (values[field] ? errors : { ...errors, [field]: 'Required' }), {})
  return errors
}
const renderInputText = ({ input, placeholder, type, meta: { touched, error } }) => (
  <div style={{ width: 300, marginTop:10 }}>
    <label className="white">{placeholder}</label> <Input {...input} placeholder={placeholder} type={type}/>
    {touched && error && <span style={{ color: 'red' }}>{'Required'}</span>}
  </div>
)

const LoginForm = ({
  handleSubmit,
  onFormSubmit
}) => (
  <form onSubmit={handleSubmit(onFormSubmit)}>
    <Field name="username" component={renderInputText} type="text" placeholder="Username" />
    <Field name="password" component={renderInputText} type="password" placeholder="Password"/>
    <br />
    <button type="submit" style={{ textAlign: 'left', marginRight: 10 }} className="ant-btn ant-btn-primary">Login</button>
  </form>
)
export default reduxForm({
  form: 'loginForm',
  validate
})(LoginForm)
