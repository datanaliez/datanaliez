import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { browserHistory } from 'react-router'
import { LocaleProvider } from 'antd';
import enUS from 'antd/lib/locale-provider/en_US';

import routes from './routes'
import configureStore from './stores/configure_store'

export const store = configureStore(browserHistory)

render(
  <LocaleProvider locale={enUS}>
    <Provider store={store}>
      {routes(store, browserHistory)}
    </Provider>
  </LocaleProvider>
  , document.getElementById('root')
)
