import { createStore, applyMiddleware } from 'redux'
import { routerMiddleware } from 'react-router-redux'
import thunk from 'redux-thunk'
import promise from 'redux-promise'
import promiseMiddleware from 'redux-promise-middleware'
import rootReducer from '../reducers'

export default (history) => {
  const middlewares = [routerMiddleware(history), thunk, promise, promiseMiddleware()]
  const middlewareEnhancer = applyMiddleware(...middlewares)
  const store = createStore(rootReducer, middlewareEnhancer)

  return store
}
