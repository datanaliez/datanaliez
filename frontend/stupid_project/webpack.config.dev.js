const webpack = require('webpack')
const path = require('path')
require("babel-core/register");
require("babel-polyfill");

const devServerPort = 8080

module.exports = {
  // watch: true,
  // watchOptions: {
  //   poll: true
  // },
  devtool: 'eval',
  entry: {
    dev: [
      'react-hot-loader/patch',
      'webpack/hot/only-dev-server',
      `webpack-dev-server/client?http://localhost:${devServerPort}`,
    ],
    app: ['./src/index.js']
  },
  output: {
    publicPath: '/public/',
    path: path.join(__dirname, 'public/'),
    filename: '[name].js'
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: ['es2015', 'stage-0', 'react', 'react-hmre'],
              plugins: [
                'react-hot-loader/babel',
                ['import', { libraryName: 'antd', style: 'css' }]
              ]
            }
          }
        ]
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader'
        ]
      },
      {
        test: /\.less$/,
        use: [
          'style-loader',
          'css-loader',
          'less-loader'
        ]
      },
      {
        test: /\.scss$|\.sass$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              sourceMap: true,
              module: true,
              localIdentName: '[local]___[hash:base64:5]'
            }
          },
          'sass-loader',
        ]
      },
      {
        test: /\.(svg|png|jpg|gif)$/,
        loader: 'file-loader'
      }
    ]
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.DefinePlugin({
      'process.env': { NODE_ENV: JSON.stringify('development') }
    })
  ],
  devServer: {
    historyApiFallback: true,
    hot: true,
    inline: true,
    port: devServerPort,
    host: '0.0.0.0',
    proxy: {
      '/api/v1/*': {
        target: 'http://localhost:3010'
      },
      '/api/mock/*': {
        target: 'http://localhost:3000'
      }
    }
  }
}
