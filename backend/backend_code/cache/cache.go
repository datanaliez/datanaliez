package cache

import (
	"encoding/json"
	"reflect"
	"time"

	"../util"
	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis"
	"github.com/go-redis/redis_rate"
)

// SystemCache : Cache for backend
var SystemCache *Cache

// Debug variable
var Cached, Uncache = 0, 0

// Cache system using redisDB, consist of table detail (Field name and type), form info
type Cache struct {
	redisConn                       *redis.Client
	formInfoCachingTrigger          *redis_rate.Limiter
	CacheFormWhenRequestRateExceed  int64 // Per seconds
	UncacheFormWhenRequestRateBelow int64 // Per minute
}

const dbHost = "mongo-mongodb-replicaset:27017"

// Initiate by connect to redisDB
func Initiate(config *redis.Options) *Cache {
	newCache := new(Cache)
	newCache.redisConn = redis.NewClient(config)
	newCache.formInfoCachingTrigger = redis_rate.NewLimiter(newCache.redisConn)
	newCache.CacheFormWhenRequestRateExceed = 15
	newCache.UncacheFormWhenRequestRateBelow = 500
	return newCache
}

// Set value to redisDB
func (cache *Cache) interfaceSet(key string, value interface{}, expiration int64) {
	var stringifiedValue string
	if reflect.TypeOf(value).Kind() == reflect.String {
		stringifiedValue = value.(string)
	} else {
		byteStringifiedValue, _ := json.Marshal(value)
		stringifiedValue = string(byteStringifiedValue)
	}
	cache.redisConn.Set(key, stringifiedValue, time.Duration(expiration))
}

// Get value from redisDB
func (cache *Cache) interfaceGet(key string) gin.H {
	result, err := cache.redisConn.Get(key).Result()
	if err != nil {
		return nil
	}
	return util.RequestFormatter([]byte(result), err)
}
