package cache

import (
	"testing"

	"../util"
	"github.com/gin-gonic/gin"
	"github.com/influxdata/influxdb/pkg/testing/assert"
)

func TestGetTableProperty(t *testing.T) {
	tbProperty := cache.GetTableProperty("b10e3b22fc28533ece5e24ffb2e010e9a059c3")
	assert.Equal(t, tbProperty["fullname"], "String")
	assert.Equal(t, tbProperty["skills"], "Array")
	assert.Equal(t, tbProperty["sex"], "Boolean")
	assert.Equal(t, tbProperty["gpax"], "NumberDecimal")
	assert.Equal(t, tbProperty["birthDate"], "Date")
	assert.Equal(t, tbProperty["_id"], "ObjectId")
}

func BenchmarkGetTableProperty(b *testing.B) {
	for n := 0; n < b.N; n++ {
		cache.GetTableProperty("b10e3b22fc28533ece5e24ffb2e010e9a059c3")
	}
}
func TestGetTableFields(t *testing.T) {
	fieldList := cache.GetTableFields("b10e3b22fc28533ece5e24ffb2e010e9a059c3")
	assert.Equal(t, util.StringInSlice("fullname", fieldList), true)
	assert.Equal(t, util.StringInSlice("skills", fieldList), true)
	assert.Equal(t, util.StringInSlice("sex", fieldList), true)
	assert.Equal(t, util.StringInSlice("gpax", fieldList), true)
	assert.Equal(t, util.StringInSlice("birthDate", fieldList), true)
	assert.Equal(t, util.StringInSlice("_id", fieldList), true)
}
func TestUpdateTableProperty(t *testing.T) {
	cache.UpdateTableProperty("b10e3b22fc28533ece5e24ffb2e010e9a059c3", gin.H{
		"fullname":  gin.H{"String": 300, "Array": 500},
		"skills":    gin.H{"String": 600, "Array": -60},
		"sex":       gin.H{"NumberDecimal": 200, "Boolean": 50},
		"birthDate": gin.H{"String": 600, "Date": 10},
		"newCol":    gin.H{"String": 700, "NumberDecimal": 400},
	})
	cache.UpdateTableProperty("b10e3b22fc28533ece5e24ffb2e010e9a059c3", gin.H{
		"newCol": gin.H{"NumberDecimal": 40},
	})
	tbProperty := cache.interfaceGet("collectionStruct.b10e3b22fc28533ece5e24ffb2e010e9a059c3")
	assert.Equal(t, tbProperty["fullname"].(map[string]interface{})["String"], float64(1300))
	assert.Equal(t, tbProperty["fullname"].(map[string]interface{})["Array"], float64(500))
	assert.Equal(t, tbProperty["skills"].(map[string]interface{})["String"], float64(600))
	assert.Equal(t, tbProperty["skills"].(map[string]interface{})["Array"], float64(940))
	assert.Equal(t, tbProperty["sex"].(map[string]interface{})["NumberDecimal"], float64(200))
	assert.Equal(t, tbProperty["sex"].(map[string]interface{})["Boolean"], float64(1050))
	assert.Equal(t, tbProperty["birthDate"].(map[string]interface{})["String"], float64(600))
	assert.Equal(t, tbProperty["birthDate"].(map[string]interface{})["Date"], float64(1010))
	assert.Equal(t, tbProperty["gpax"].(map[string]interface{})["NumberDecimal"], float64(1000))
	assert.Equal(t, tbProperty["newCol"].(map[string]interface{})["String"], float64(700))
	assert.Equal(t, tbProperty["newCol"].(map[string]interface{})["NumberDecimal"], float64(440))
}
