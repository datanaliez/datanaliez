package cache

import (
	"../util"
	"github.com/gin-gonic/gin"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func (cache *Cache) getTableProperty(tableName string) gin.H {
	queryResult := []bson.M{}
	collectionStructCached := cache.interfaceGet("collectionStruct." + tableName)
	if collectionStructCached == nil {
		collectionStructCached = gin.H{}
		mdb, err := mgo.Dial(dbHost)

		defer mdb.Close()
		util.CheckErr(err)
		db := mdb.DB("StupidProject2017")
		db.Run(bson.M{
			"mapreduce": tableName,
			"map":       "function() {for (var key in this) { emit(key, {[this[key]?this[key].constructor.name:'null']:1}); }}",
			"reduce":    "function(key, stuff) { return stuff.slice(1).reduce((acc,type)=>{acc[Object.keys(type)[0]]=acc[Object.keys(type)[0]]+1||1; return acc},stuff[0]) }",
			"out":       tableName + "_keys",
		}, nil)
		db.C(tableName + "_keys").Find(bson.M{}).All(&queryResult)
		for _, fieldInfo := range queryResult {
			collectionStructCached[fieldInfo["_id"].(string)] = map[string]interface{}{}
			for fieldType, freq := range fieldInfo["value"].(bson.M) {
				collectionStructCached[fieldInfo["_id"].(string)].(map[string]interface{})[fieldType] = freq.(float64)
			}
		}
		cache.interfaceSet("collectionStruct."+tableName, collectionStructCached, 0)
	}
	return collectionStructCached
}

// GetTableProperty : Get field name and type of target collection
func (cache *Cache) GetTableProperty(tableName string) map[string]string {
	collectionStructCached := cache.getTableProperty(tableName)
	// fmt.Printf("%v", collectionStructCached)
	collectionStructure := map[string]string{}
	for fieldName, fieldProperties := range collectionStructCached {
		mostFreq, mostFreqType := -1, ""
		for fieldType, freq := range fieldProperties.(map[string]interface{}) {
			if int(freq.(float64)) > mostFreq {
				mostFreq = int(freq.(float64))
				mostFreqType = fieldType
			}
		}
		collectionStructure[fieldName] = mostFreqType
	}
	return collectionStructure
}

// GetTableFields : Get field name and type of target collection
func (cache *Cache) GetTableFields(tableName string) []string {
	collectionStructCached := cache.getTableProperty(tableName)
	Fields := []string{}
	for fieldName := range collectionStructCached {
		Fields = append(Fields, fieldName)
	}
	return Fields
}

// UpdateTableProperty : Update field name and type of collection in redisDB
func (cache *Cache) UpdateTableProperty(tableName string, newDataProperty gin.H) {
	insertedCount := -1
	tbProperty := cache.getTableProperty(tableName)
	for fieldName, fieldProperties := range newDataProperty {
		//sumFreq - use to determined change value
		sumFreq := 0
		//totalsumFreq - use to determine total number even after plus
		totalSumFreq := 0
		for fieldType, freq := range fieldProperties.(gin.H) {
			sumFreq += freq.(int)
			if oldFreq := tbProperty[fieldName]; oldFreq == nil {
				tbProperty[fieldName] = map[string]interface{}{
					fieldType: float64(freq.(int)),
				}
			} else if oldFreq.(map[string]interface{})[fieldType] == nil {
				tbProperty[fieldName].(map[string]interface{})[fieldType] = float64(freq.(int))
			} else {
				tbProperty[fieldName].(map[string]interface{})[fieldType] =
					float64(int(tbProperty[fieldName].(map[string]interface{})[fieldType].(float64)) + freq.(int))
			}
			totalSumFreq += int(tbProperty[fieldName].(map[string]interface{})[fieldType].(float64))
		}
		if sumFreq > insertedCount {
			insertedCount = sumFreq
		}
		if totalSumFreq == 0 {
			delete(tbProperty,fieldName)
		}
	}
	if _, exist := tbProperty["_id"]; !exist {
		tbProperty["_id"] = map[string]interface{}{"ObjectId": insertedCount}
	}
	cache.interfaceSet("collectionStruct."+tableName, tbProperty, 0)
}

// FlushTableProperty : Delete table property from redisDB
func (cache *Cache) FlushTableProperty(tableName string) {
	cache.redisConn.Del("collectionStruct." + tableName)
}
