package cache

import (
	"testing"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/influxdata/influxdb/pkg/testing/assert"
	"gopkg.in/mgo.v2/bson"
)

// This is not pure unit test!
func TestFetchFormInfo(t *testing.T) {
	// --- Test fetch from mongoDB --- //
	uncachedFormInfo, isOwner := cache.FetchFormInfo(MockFormID, []string{"recruitVidvaplus2018", "CCTER5094"}, false)
	assert.Equal(t, isOwner, true)
	assert.Equal(t, uncachedFormInfo["disabled"], true)
	assert.Equal(t, uncachedFormInfo["targetCollection"], "recruitVidvaplus2018")
	assert.Equal(t, len(uncachedFormInfo["answered"].([]interface{})), 3)
	assert.Equal(t, len(uncachedFormInfo["fieldList"].([]interface{})), 8)
	assert.Equal(t, uncachedFormInfo["fieldList"].([]interface{})[0].(gin.H)["type"], "choice")
	// --- Test force caching to redisDB --- //
	for index := 0; index < 1000; index++ {
		cache.FetchFormInfo(MockFormID, []string{"recruitVidvaplus2018", "CCTER5094"}, false)
	}
	println("Testing is cached")
	assert.NotEqual(t, cache.interfaceGet("formInfo."+MockFormID), nil)
	cachedFormInfo, isOwner := cache.FetchFormInfo(MockFormID, []string{"recruitVidvaplus2018", "CCTER5094"}, false)
	assert.Equal(t, isOwner, true)
	assert.Equal(t, cachedFormInfo["disabled"], true)
	assert.Equal(t, cachedFormInfo["targetCollection"], "recruitVidvaplus2018")
	assert.Equal(t, len(cachedFormInfo["visited"].([]interface{})), 1007)
	assert.Equal(t, len(cachedFormInfo["answered"].([]interface{})), 3)
	assert.Equal(t, len(cachedFormInfo["fieldList"].([]interface{})), 8)
	assert.Equal(t, cachedFormInfo["fieldList"].([]interface{})[0].(map[string]interface{})["type"], "choice")
	// Cool down -- Uncaching formInfo
	for index := 0; index < 75; index++ {
		time.Sleep(500 * time.Millisecond)
		cache.FetchFormInfo(MockFormID, []string{"recruitVidvaplus2018", "CCTER5094"}, false)
	}
	for index := 0; index < 75; index++ {
		time.Sleep(500 * time.Millisecond)
		cache.FetchFormInfo(MockFormID, []string{"CCTER5094", false})
	}
	assert.Equal(t, cache.interfaceGet("formInfo."+MockFormID), gin.H(nil))
	// --- Re-Test fetch from mongoDB --- //
	uncachedFormInfo, isOwner = cache.FetchFormInfo(MockFormID, []string{"CCTER5094"}, false)
	assert.Equal(t, isOwner, false)
	assert.Equal(t, uncachedFormInfo["disabled"], true)
	assert.Equal(t, uncachedFormInfo["targetCollection"], "recruitVidvaplus2018")
	assert.Equal(t, len(uncachedFormInfo["visited"].([]interface{})), 1083)
	assert.Equal(t, len(uncachedFormInfo["answered"].([]interface{})), 3)
	assert.Equal(t, len(uncachedFormInfo["fieldList"].([]interface{})), 8)
	assert.Equal(t, uncachedFormInfo["fieldList"].([]interface{})[0].(gin.H)["type"], "choice")
}

// แบ่งเทสเป็นสองส่วน คือ cached, mongo only
// เอาไปครอบตัว assert อีกที

func TestFormSubmissionMark(t *testing.T) {
	if burnState {
		burnState = false
		time.Sleep(time.Minute)
	}
	burnState = true
	time.Sleep(1 * time.Second)
	cache.FormSubmissionMark(MockFormID)
	FormInfo, isOwner := cache.FetchFormInfo(MockFormID, []string{"CCTER5094"}, false)
	assert.Equal(t, isOwner, false)
	assert.Equal(t, len(FormInfo["answered"].([]interface{})), 4)
}

func TestFormEdit(t *testing.T) {
	if burnState {
		burnState = false
		time.Sleep(time.Minute)
	}
	burnState = true
	time.Sleep(1 * time.Second)
	cache.FormEdit(MockFormID, bson.M{"targetCollection": "recruitVidvaplus2019", "disabled": false})
	newVersionNumber, _ := cache.redisConn.Get("formInfo." + MockFormID + ".mongoVersion").Result()
	assert.Equal(t, newVersionNumber, "1")
	FormInfo, isOwner := cache.FetchFormInfo(MockFormID, []string{"CCTER5094"}, false)
	assert.Equal(t, isOwner, false)
	assert.Equal(t, FormInfo["disabled"], false)
	assert.Equal(t, FormInfo["targetCollection"], "recruitVidvaplus2019")
}
func TestFormDelete(t *testing.T) {
	if burnState {
		burnState = false
		time.Sleep(time.Minute)
	}
	burnState = true
	time.Sleep(1 * time.Second)
	cache.FormDelete(MockFormID)
	assert.Equal(t, cache.interfaceGet("formInfo."+MockFormID), gin.H(nil))
	FormInfo, isOwner := cache.FetchFormInfo(MockFormID, []string{"CCTER5094"}, false)
	assert.Equal(t, isOwner, false)
	assert.Equal(t, FormInfo, gin.H(nil))
}
