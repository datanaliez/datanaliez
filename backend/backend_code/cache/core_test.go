package cache

import (
	"testing"
	"time"

	"../util"
	"gopkg.in/mgo.v2/bson"

	"encoding/json"

	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis"
	"github.com/icrowley/fake"
	mgo "gopkg.in/mgo.v2"
)

var cache *Cache

const MockFormID = "5a420ffb900d32c3b72f0b0ee7495967bbef9c806d3a6c92af82b4016a33625f"

var burnState = false

func BurnUp() {
	for {
		if burnState {
			cache.FetchFormInfo(MockFormID, []string{"CCTER5094"}, false)
		}
		time.Sleep(10 * time.Millisecond)
	}
}
func TestMain(m *testing.M) {
	mockedFormInfo := gin.H{}
	cache = Initiate(&redis.Options{
		Addr:     "localhost:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})
	cache.redisConn.FlushAll()
	mdb, _ := mgo.Dial("localhost:27017")
	defer mdb.Close()
	mockedCollection := mdb.DB("StupidProject2017").C("b10e3b22fc28533ece5e24ffb2e010e9a059c3")
	mockedCollection.DropCollection()
	mdb.DB("StupidProject2017").C("b10e3b22fc28533ece5e24ffb2e010e9a059c3_keys").DropCollection()
	formInfoCollections := mdb.DB("StupidProject2017").C("DATANALIEZ_TableFormInfo")
	formInfoCollections.Remove(bson.M{"formID": MockFormID})
	parsed, _ := bson.ParseDecimal128("2.14")
	for index := 0; index < 1000; index++ {
		mockedCollection.Insert(bson.M{
			"fullname":  fake.FullName(),
			"skills":    []string{fake.JobTitle(), fake.JobTitle(), fake.JobTitle(), fake.JobTitle()},
			"sex":       true,
			"gpax":      parsed,
			"birthDate": time.Now(),
		})
	}
	err := json.Unmarshal([]byte("{\"disabled\":true,\"targetCollection\":\"recruitVidvaplus2018\",\"formID\":\"5a420ffb900d32c3b72f0b0ee7495967bbef9c806d3a6c92af82b4016a33625f\",\"fieldList\":[{\"type\":\"choice\",\"required\":true,\"desc\":\"\",\"option\":[{\"label\":\"นาย\",\"value\":\"นาย\"},{\"label\":\"นางสาว\",\"value\":\"นางสาว\"}],\"name\":\"เพศ\",\"label\":\"คำนำหน้าชื่อ\"},{\"name\":\"ชื่อจริง\",\"label\":\"ชื่อ-นามสกุล\",\"type\":\"string\",\"required\":true,\"desc\":\"\"},{\"name\":\"ชื่อเล่น\",\"label\":\"ชื่อเล่น\",\"type\":\"string\",\"required\":false,\"desc\":\"\",\"validate\":\"\"},{\"name\":\"เบอร์\",\"label\":\"เบอร์โทรศัพท์มือถือ\",\"type\":\"string\",\"required\":true,\"desc\":\"\",\"validate\":\"^(06|08|09)[0-9]{8}$\"},{\"label\":\"ไลน์\",\"type\":\"string\",\"required\":true,\"desc\":\"\",\"validate\":\"\",\"name\":\"LINE_ID\"},{\"type\":\"choice\",\"required\":true,\"desc\":\"ยึดปัจจุบัน\",\"option\":[{\"label\":\"นิสิตทุน\",\"value\":\"นิสิตทุน\"},{\"label\":\"นิสิตปกติ\",\"value\":\"นิสิตปกติ\"}],\"name\":\"ประเภท\",\"label\":\"ประเภทนิสิต\"},{\"name\":\"specialRequirement\",\"label\":\"สิ่งที่ค่ายจำเป็นต้องทราบ\",\"type\":\"lg_string\",\"required\":false,\"desc\":\"เช่น อาหารที่ แพ้/ไม่สามารถรับประทานได้, โรคประจำตัว ฯลฯ\",\"validate\":\"\"},{\"label\":\"กิจกรรมที่อยากทำในค่าย \",\"type\":\"checkbox\",\"required\":true,\"desc\":\"\",\"option\":[{\"label\":\"ซ่อมแซมพัดลม/เปลี่ยนหลอดไฟ/ติดตั้งก๊อกน้ำ\",\"value\":\"ซ่อมแซมพัดลม/เปลี่ยนหลอดไฟ/ติดตั้งก๊อกน้ำ\"},{\"label\":\"ทาสีภายในห้องและภายนอกอาคาร\",\"value\":\"ทาสีภายในห้องและภายนอกอาคาร\"},{\"label\":\"สวัสดิการ\",\"value\":\"สวัสดิการ\"}],\"name\":\"งาน\"}],\"closeOn\":{},\"title\":\"แบบฟอร์มสมัครค่ายวิศวพลัส ปีการศึกษา 2560 ภาคเรียนที่ 2\",\"desc\":\"โครงการกิจกรรมบำเพ็ญประโยชน์เพื่อสังคม ค่ายวิศวพลัส \"}"), &mockedFormInfo)
	// [\"2018-05-02T22:48:47.141Z\",\"2018-05-03T13:17:13.826Z\",\"2018-05-03T15:37:53.756Z\"]
	util.CheckErr(err)
	mockedFormInfo["answered"] = []time.Time{time.Now(), time.Now(), time.Now()}
	mockedFormInfo["visited"] = []time.Time{time.Now(), time.Now(), time.Now(), time.Now(), time.Now(), time.Now()}
	formInfoCollections.Insert(mockedFormInfo)
	go BurnUp()
	m.Run()
}
