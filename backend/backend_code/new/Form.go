package new

import (
	"errors"
	"fmt"
	"regexp"
	"strconv"
	"time"

	"../cache"
	"../util"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func CreateForm(c *gin.Context) {
	session := sessions.Default(c)
	if util.StringInSlice(c.Param("tablename"), session.Get("permitTableName").([]string)) {
		newFormInfoParams := util.RequestFormatter(c.GetRawData())
		newFormInfo, err := util.PrepareFormInfo(newFormInfoParams)
		if err != nil {
			c.AbortWithError(500, err)
		}
		newFormInfo["targetCollection"] = c.Param("tablename")
		mdb, err := mgo.Dial("mongo-mongodb-replicaset:27017")
		
		defer mdb.Close()
		util.CheckErr(err)
		formInfoCollections := mdb.DB("StupidProject2017").C("DATANALIEZ_TableFormInfo")
		formID := util.SHA256(util.RandomString(15))
		for {
			if count, _ := formInfoCollections.Find(bson.M{"formID": formID}).Count(); count > 0 {
				formID = util.SHA256(util.RandomString(15))
			} else {
				break
			}
		}
		newFormInfo["formID"] = formID
		formInfoCollections.Insert(newFormInfo)
		c.JSON(200, gin.H{
			"status": "success",
			"formID": formID,
		})
	} else {
		c.AbortWithStatus(403)
	}
}

func isValueExistInOption(search string, options []interface{}) bool {
	for _, option := range options {
		option := option.(map[string]interface{})
		if option["value"].(string) == search {
			return true
		}
	}
	return false
}
func FormSubmission(c *gin.Context) {
	var formInfo gin.H
	collectionStructureTransaction := gin.H{}
	// formInfoCollections := mdb.DB("StupidProject2017").C("DATANALIEZ_TableFormInfo")
	// targetForm := formInfoCollections.Find(bson.M{"formID": c.Param("formID")})
	formInfo, _ = cache.SystemCache.FetchFormInfo(c.Param("formID"), []string{}, true)
	if formInfo == nil {
		c.AbortWithStatus(404)
		return
	}
	// Is form is expired or disbled?
	// targetForm.One(&formInfo)
	isFormExpired := formInfo["disabled"].(bool)
	closeOn := formInfo["closeOn"].(map[string]interface{})
	if closeOn["datetime"] != nil {
		notValidBefore := closeOn["datetime"].([]interface{})[0].(time.Time)
		notValidAfter := closeOn["datetime"].([]interface{})[1].(time.Time)
		isFormExpired = isFormExpired || time.Now().After(notValidAfter)
		isFormExpired = isFormExpired || time.Now().Before(notValidBefore)
	}
	if closeOn["registered"] != nil {
		registrationLimit := int(closeOn["registered"].(float64))
		answeredLog := formInfo["answered"].([]interface{})
		isFormExpired = isFormExpired || (len(answeredLog) >= registrationLimit)
	}
	if isFormExpired {
		c.AbortWithStatusJSON(403, gin.H{
			"success": false,
			"error":   "expired",
		})
		return
	}
	// Validate value
	newDataRecord := bson.M{}
	userData := util.RequestFormatter(c.GetRawData())
	for _, fieldInfo := range formInfo["fieldList"].([]interface{}) {
		fieldInfo := fieldInfo.(map[string]interface{})
		userInputValue := userData[fieldInfo["name"].(string)]
		if userInputValue == nil && fieldInfo["required"].(bool) {
			c.AbortWithError(500, errors.New(fieldInfo["name"].(string)+" is required, but users not input anything."))
		} else if userInputValue != nil {
			switch fieldInfo["type"].(string) {
			case "choice":
				if isValueExistInOption(userInputValue.(string), fieldInfo["option"].([]interface{})) {
					newDataRecord[fieldInfo["name"].(string)] = userInputValue.(string)
				} else {
					c.AbortWithError(500, errors.New(fieldInfo["name"].(string)+" not exist in options."))
					return
				}
				collectionStructureTransaction[fieldInfo["name"].(string)] = gin.H{"String": 1}
			case "checkbox":
				for _, selectedOption := range userInputValue.([]interface{}) {
					selectedOption := selectedOption.(string)
					if !isValueExistInOption(selectedOption, fieldInfo["option"].([]interface{})) {
						c.AbortWithError(500, errors.New(fieldInfo["name"].(string)+" have value that not exist in options."))
						return
					}
				}
				newDataRecord[fieldInfo["name"].(string)] = userInputValue
				collectionStructureTransaction[fieldInfo["name"].(string)] = gin.H{"Array": 1}
			case "number":
				inputNumber := userInputValue.(float64)
				if validator := fieldInfo["validate"]; validator != nil {
					validator := validator.(string)
					if match, _ := regexp.MatchString(validator, strconv.FormatFloat(inputNumber, 'f', -1, 64)); !match {
						c.AbortWithError(500, errors.New(fieldInfo["name"].(string)+" not valid."))
						return
					}
				}
				newDataRecord[fieldInfo["name"].(string)], _ = bson.ParseDecimal128(strconv.FormatFloat(inputNumber, 'f', -1, 64))
				collectionStructureTransaction[fieldInfo["name"].(string)] = gin.H{"NumberDecimal": 1}
			case "integer":
				inputInt := userInputValue.(float64)
				if inputInt != float64(int(inputInt)) {
					c.AbortWithError(500, errors.New(fieldInfo["name"].(string)+" is not integer."))
					return
				}
				if validator := fieldInfo["validate"]; validator != nil && inputInt == float64(int(inputInt)) {
					validator := validator.(string)
					if match, _ := regexp.MatchString(validator, strconv.Itoa(int(inputInt))); !match {
						c.AbortWithError(500, errors.New(fieldInfo["name"].(string)+" not valid."))
						return
					}
				}
				newDataRecord[fieldInfo["name"].(string)], _ = bson.ParseDecimal128(strconv.Itoa(int(inputInt)))
				collectionStructureTransaction[fieldInfo["name"].(string)] = gin.H{"NumberDecimal": 1}
			case "string", "lg_string":
				inputString := userInputValue.(string)
				if validator := fieldInfo["validate"]; validator != nil {
					validator := validator.(string)
					if match, _ := regexp.MatchString(validator, fmt.Sprint(inputString)); !match {
						c.AbortWithError(500, errors.New(fieldInfo["name"].(string)+" not valid."))
						return
					}
				}
				newDataRecord[fieldInfo["name"].(string)] = inputString
				collectionStructureTransaction[fieldInfo["name"].(string)] = gin.H{"String": 1}
			}
		}
	}
	// Insert to collection with timestamp and insert to answered
	newDataRecord[c.Param("formID")+"_Timestamp"] = time.Now()
	mdb, err := mgo.Dial("mongo-mongodb-replicaset:27017")
	util.CheckErr(err)
	
	defer mdb.Close()
	mdb.DB("StupidProject2017").C(formInfo["targetCollection"].(string)).Insert(newDataRecord)
	cache.SystemCache.FormSubmissionMark(c.Param("formID"))
	cache.SystemCache.UpdateTableProperty(formInfo["targetCollection"].(string), collectionStructureTransaction)
	c.JSON(200, gin.H{
		"success": true,
	})
}
