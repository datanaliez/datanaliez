package new

import (
	// "fmt"
	"database/sql"
	"regexp"

	"../util"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
	"gopkg.in/gorp.v1"
	"gopkg.in/mgo.v2"
)

var (
	Dbmap *gorp.DbMap
	db    *sql.DB
)

func Init() {
	Dbmap, db = util.InitDb()
}

type UserTable struct {
	Username  string `db:"username"`
	Tablename string `db:"tablename"`
}

func CreateTable(c *gin.Context) {
	Init()
	session := sessions.Default(c)
	mdb, err := mgo.Dial("mongo-mongodb-replicaset:27017");
	defer mdb.Close()
	util.CheckErr(err)
	allCollectionsName, err := mdb.DB("StupidProject2017").CollectionNames()
	if !regexp.MustCompile(`[/\s]`).MatchString(c.Param("tablename")) &&
		regexp.MustCompile(`[\x20-\x7E]`).MatchString(c.Param("tablename")) &&
		!util.StringInSlice(c.Param("tablename"), allCollectionsName) {
		Dbmap.AddTableWithName(UserTable{}, "user_table")
		Dbmap.CreateTablesIfNotExists()
		err := Dbmap.Insert(&UserTable{
			session.Get("username").(string),
			c.Param("tablename"),
		})
		util.CheckErr(err)
		newTable := mdb.DB("StupidProject2017").C(c.Param("tablename"))
		newTable.Create(&mgo.CollectionInfo{})
		session.Set("permitTableName", append(session.Get("permitTableName").([]string), c.Param("tablename")))
		db.Close()
		c.JSON(200, gin.H{
			"result": "OK",
		})
	} else {
		c.JSON(200, gin.H{
			"result": "Error",
			"msg":    "Table name exist or prohibited",
		})
	}
}
