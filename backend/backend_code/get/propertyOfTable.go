package get

import (
	"../cache"
	"../util"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
)

func GetAllColumnName(c *gin.Context) {
	session := sessions.Default(c)
	if util.StringInSlice(c.Param("tablename"), session.Get("permitTableName").([]string)) {
		c.JSON(200, gin.H{
			"result": cache.SystemCache.GetTableFields(c.Param("tablename")),
		})
	} else {
		c.JSON(403, gin.H{
			"error": "Permission denied.",
		})
	}
}
func GetTypeOfColumn(c *gin.Context) {
	session := sessions.Default(c)
	if util.StringInSlice(c.Param("tablename"), session.Get("permitTableName").([]string)) {
		// c.Data(200, "text/plain", []byte(cache.SystemCache.GetTableProperty(c.Param("tablename"))[c.Param("columnName")]))
		c.JSON(200, gin.H{"type": cache.SystemCache.GetTableProperty(c.Param("tablename"))[c.Param("columnName")]})
	} else {
		c.JSON(403, gin.H{"error": "permission denied"})
	}
}
