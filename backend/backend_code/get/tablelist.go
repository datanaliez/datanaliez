package get

import (
	// "fmt"

	"../util"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
)

func TableList(c *gin.Context) {
	SelectedColumn := gin.H{}
	session := sessions.Default(c)
	Init()
	rows, err := db.Query("SELECT `ID`,`last_viewed`,`tablename` FROM user_table WHERE username = ? ORDER BY last_viewed DESC", session.Get("username").(string))
	util.CheckErr(err)
	result := util.ConvertRawBytesToMap(rows)
	var permitTableName []string
	permitTableName = make([]string, 0)
	for _, row := range result {
		permitTableName = append(permitTableName, row["tablename"])
		SelectedColumn[row["tablename"]] = []string{}
	}
	session.Set("permitTableName", permitTableName)
	session.Set("SelectedColumn", SelectedColumn)
	session.Save()
	c.JSON(200, gin.H{"tableList": result})
	db.Close()
}
