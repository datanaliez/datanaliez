package get

import (
	"encoding/json"
	"os"
	"path/filepath"
	"strconv"

	"strings"

	"../cache"
	"../util"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"github.com/tealeg/xlsx"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func DataInTable(c *gin.Context) {
	session := sessions.Default(c)
	var mdb *mgo.Session
	var result []gin.H
	var err error
	dataCount := -1
	if util.StringInSlice(c.Param("tablename"), session.Get("permitTableName").([]string)) {
		go func() {
			Init()
			stmt, err := db.Prepare("UPDATE `user_table` SET `last_viewed`=CURRENT_TIMESTAMP WHERE `tablename` = ?")
			util.CheckErr(err)
			_, err = stmt.Exec(c.Param("tablename"))
			util.CheckErr(err)
			stmt.Close()
			db.Close()
		}()
		mdb, err = mgo.Dial("mongo-mongodb-replicaset:27017");
		defer mdb.Close()
		util.CheckErr(err)
		jsonParams := util.RequestFormatter(c.GetRawData())
		userTable := mdb.DB("StupidProject2017").C(c.Param("tablename"))
		pageNo, err := strconv.ParseInt(c.Param("page"), 0, 32)
		util.CheckErr(err)
		aggregate := util.NewAggregator(cache.SystemCache.GetTableProperty(c.Param("tablename")))
		filterList := session.Get("filterList")
		thisTableFilter := []gin.H{nil}
		if filterList != nil {
			dereferenceFilterList := *(filterList.(*gin.H))
			dereferenceThisTableFilter := dereferenceFilterList[c.Param("tablename")]
			if dereferenceThisTableFilter != nil {
				thisTableFilter = *(dereferenceThisTableFilter.(*[]gin.H))
			}
		}
		aggregate.SetFilter(thisTableFilter).SetLimit(int(pageNo-1)*20, 20)
		dereferenceSelectedColumn := *(session.Get("SelectedColumn").(*gin.H))
		thisTableSelectedColumn := (dereferenceSelectedColumn[c.Param("tablename")].([]string))
		if len(thisTableSelectedColumn) > 0 {
			aggregate.SelectColumn(thisTableSelectedColumn)
		} else {
			aggregate.SelectColumn(cache.SystemCache.GetTableFields(c.Param("tablename")))
		}
		go func(aggregation []bson.M) {
			var countResult gin.H
			mdb2 := mdb.Clone()
			userTableThread := mdb2.DB("StupidProject2017").C(c.Param("tablename"))
			util.CheckErr(userTableThread.Pipe(aggregation).One(&countResult))
			if countResult["count"] == nil {
				dataCount = 0
			} else {
				dataCount = countResult["count"].(int)
			}
			mdb2.Close()
		}(aggregate.Finalize("dataCount"))

		if jsonParams["sortby"] != nil && jsonParams["sortby"] != "" {
			util.CheckErr(userTable.Pipe(aggregate.Sorting(jsonParams["sortby"].(string), jsonParams["orderby"].(string)).Finalize("data")).All(&result))
		} else {
			util.CheckErr(userTable.Pipe(aggregate.Finalize("data")).All(&result))
		}
	} else {
		result = append(result, gin.H{"error": "permission denied"})
	}
	for dataCount == -1 {
		println(".")
	}
	// fmt.Printf("%v", result)
	c.JSON(200, gin.H{
		"count": dataCount,
		"body":  result,
	})
}

func SetFilter(c *gin.Context) {
	session := sessions.Default(c)
	var result []gin.H
	var newFilterList gin.H
	// var dataCount int
	if util.StringInSlice(c.Param("tablename"), session.Get("permitTableName").([]string)) {
		jsonParams := util.ArrayRequestFormatter(c.GetRawData())
		oldFilterList := session.Get("filterList")
		if oldFilterList != nil {
			newFilterList = *(oldFilterList.(*gin.H))
		} else {
			newFilterList = gin.H{}
		}
		newFilterList[c.Param("tablename")] = jsonParams
		session.Set("filterList", newFilterList)
		util.CheckErr(session.Save())
	} else {
		result = append(result, gin.H{"error": "permission denied"})
	}
	c.JSON(200, gin.H{
		"result": "OK",
	})
}
func SelectSomeColumn(c *gin.Context) {
	session := sessions.Default(c)
	selectedColumn := []string{}
	if util.StringInSlice(c.Param("tablename"), session.Get("permitTableName").([]string)) {
		jsonParams := util.ArrayRequestFormatter(c.GetRawData())
		for _, columnName := range jsonParams {
			selectedColumn = append(selectedColumn, columnName["0"].(string))
		}
		dereferenceSelectedColumn := *(session.Get("SelectedColumn").(*gin.H))
		dereferenceSelectedColumn[c.Param("tablename")] = selectedColumn
		session.Set("SelectedColumn", dereferenceSelectedColumn)
		session.Save()
		c.JSON(200, gin.H{
			"result": "OK",
		})
	} else {
		c.AbortWithStatus(403)
	}
}

func ExportDataAsXLSX(c *gin.Context) {
	session := sessions.Default(c)
	var mdb *mgo.Session
	var result []gin.H
	var err error
	if util.StringInSlice(c.Param("tablename"), session.Get("permitTableName").([]string)) {
		mdb, err = mgo.Dial("mongo-mongodb-replicaset:27017");
		defer mdb.Close()
		util.CheckErr(err)
		jsonParams := util.RequestFormatter(c.GetRawData())
		userTable := mdb.DB("StupidProject2017").C(c.Param("tablename"))
		util.CheckErr(err)
		aggregate := util.NewAggregator(cache.SystemCache.GetTableProperty(c.Param("tablename")))
		filterList := session.Get("filterList")
		thisTableFilter := []gin.H{nil}
		if filterList != nil {
			dereferenceFilterList := *(filterList.(*gin.H))
			dereferenceThisTableFilter := dereferenceFilterList[c.Param("tablename")]
			if dereferenceThisTableFilter != nil {
				thisTableFilter = *(dereferenceThisTableFilter.(*[]gin.H))
			}
		}
		dereferenceSelectedColumn := *(session.Get("SelectedColumn").(*gin.H))
		thisTableSelectedColumn := (dereferenceSelectedColumn[c.Param("tablename")].([]string))
		if len(thisTableSelectedColumn) > 0 {
			aggregate.SetFilter(thisTableFilter).SelectColumn(thisTableSelectedColumn)
		} else {
			aggregate.SetFilter(thisTableFilter).SelectColumn(cache.SystemCache.GetTableFields(c.Param("tablename")))
		}
		if jsonParams["sortby"] != nil && jsonParams["sortby"] != "" {
			util.CheckErr(userTable.Pipe(aggregate.Sorting(jsonParams["sortby"].(string), jsonParams["orderby"].(string)).Finalize("data")).All(&result))
		} else {
			util.CheckErr(userTable.Pipe(aggregate.Finalize("data")).All(&result))
		}
		file := xlsx.NewFile()
		excelSheet, _ := file.AddSheet("Sheet1")
		header := excelSheet.AddRow()
		columnNames := cache.SystemCache.GetTableFields(c.Param("tablename"))
		for _, columnName := range columnNames {
			headerCell := header.AddCell()
			headerCell.Value = columnName
		}
		for _, row := range result {
			newRow := excelSheet.AddRow()
			for _, columnName := range columnNames {
				newCell := newRow.AddCell()
				stringifiedValue, err := json.Marshal(row[columnName])
				util.CheckErr(err)
				newCell.Value = strings.Trim(string(stringifiedValue), "\"")
			}
		}
		fileName := util.SHA256(util.RandomString(15)) + ".xlsx"
		file.Save(filepath.Join("downloadTMP/", fileName))
		c.Header("Content-Description", "File Transfer")
		c.Header("Content-Transfer-Encoding", "binary")
		c.Header("Content-Disposition", "attachment; filename="+c.Param("tablename")+"_export.xlsx")
		c.Header("Content-Type", "application/octet-stream")
		c.File(filepath.Join("downloadTMP/", fileName))
		os.Remove(filepath.Join("downloadTMP/", fileName))
	} else {
		c.JSON(200, gin.H{
			"error": "Permission denied.",
		})
	}
}
