package get

import (
	"../cache"
	"../util"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func Histogram(c *gin.Context) {
	session := sessions.Default(c)
	var mdb *mgo.Session
	var result []gin.H
	var err error
	var minBound, interval float64
	jsonParams := util.RequestFormatter(c.GetRawData())
	if util.StringInSlice(c.Param("tablename"), session.Get("permitTableName").([]string)) {
		go func() {
			Init()
			stmt, err := db.Prepare("UPDATE `user_table` SET `last_viewed`=CURRENT_TIMESTAMP WHERE `tablename` = ?")
			util.CheckErr(err)
			_, err = stmt.Exec(c.Param("tablename"))
			util.CheckErr(err)
			stmt.Close()
			db.Close()
		}()
		mdb, err = mgo.Dial("mongo-mongodb-replicaset:27017");
		defer mdb.Close()
		util.CheckErr(err)
		userTable := mdb.DB("StupidProject2017").C(c.Param("tablename"))
		util.CheckErr(err)
		aggregate := util.NewAggregator(cache.SystemCache.GetTableProperty(c.Param("tablename")))
		thisTableFilter := []gin.H{nil}
		filterList := session.Get("filterList")
		if filterList != nil {
			dereferenceFilterList := *(filterList.(*gin.H))
			dereferenceThisTableFilter := dereferenceFilterList[c.Param("tablename")]
			if dereferenceThisTableFilter != nil {
				thisTableFilter = *(dereferenceThisTableFilter.(*[]gin.H))
			}
		}
		aggregate.SetFilter(thisTableFilter)
		thisTableSelectedColumn := jsonParams["column_name"].(string)
		_minBound, exist1 := jsonParams["min_bound"]
		if exist1 {
			minBound = _minBound.(float64)
		}
		_interval, exist2 := jsonParams["interval"]
		if exist2 {
			interval = _interval.(float64)
		}
		if exist1 && exist2 {
			aggregate.Filter = append(aggregate.Filter, bson.M{thisTableSelectedColumn: bson.M{"$type": 19}})
			aggregate.Histogroup("$"+thisTableSelectedColumn, bson.M{
				"$floor": bson.M{
					"$divide": []interface{}{
						bson.M{
							"$subtract": []interface{}{"$" + thisTableSelectedColumn, minBound},
						},
						interval,
					},
				},
			})
		} else {
			aggregate.Histogroup("$"+thisTableSelectedColumn, "$"+thisTableSelectedColumn)
		}
		// res, _ := json.Marshal(aggregate.Finalize("histogram"))
		// print(string(res))
		err = userTable.Pipe(aggregate.Finalize("histogram")).All(&result)
		util.CheckErr(err)
	} else {
		result = append(result, gin.H{"error": "permission denied"})
	}
	c.JSON(200, gin.H{
		"histogram": result,
	})
}
