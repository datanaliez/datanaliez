package get

import (
	"regexp"

	"../util"
	"github.com/gin-gonic/gin"
	mgo "gopkg.in/mgo.v2"
)

type IsExist struct {
	Exist int `json:"exist" db:"exist"`
}

func IsTableNameExist(c *gin.Context) {
	Init()
	var result IsExist
	mdb, err := mgo.Dial("mongo-mongodb-replicaset:27017")

	defer mdb.Close()
	util.CheckErr(err)
	allCollectionsName, err := mdb.DB("StupidProject2017").CollectionNames()
	if !regexp.MustCompile(`[/\s]`).MatchString(c.Param("tablename")) &&
		regexp.MustCompile(`[\x20-\x7E]`).MatchString(c.Param("tablename")) &&
		!util.StringInSlice(c.Param("tablename"), allCollectionsName) {
		result.Exist = 0
	} else {
		result.Exist = 1
	}
	// if !regexp.MustCompile(`^(?:(?![/]|DATANALIEZ_TableFormInfo|_keys$)[\x20-\x7E])+$`).MatchString(c.Param("tablename")) {
	// 	result.Exist = 1
	// } else {
	// 	err := Dbmap.SelectOne(&result, "SELECT IF(EXISTS(SELECT * FROM `user_table` WHERE `tablename` = ?),1,0) AS exist", c.Param("tablename"))
	// 	util.CheckErr(err)
	// }
	c.JSON(200, result)
	db.Close()
}
