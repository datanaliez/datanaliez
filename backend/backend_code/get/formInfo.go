package get

import (
	"../cache"
	"../util"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func FormData(c *gin.Context) {
	var result gin.H
	var isFormOwner bool
	session := sessions.Default(c)
	permittedTableNameList, isLoggedIn := session.Get("permitTableName").([]string)
	if isLoggedIn {
		result, isFormOwner = cache.SystemCache.FetchFormInfo(c.Param("formID"), permittedTableNameList, false)
	} else {
		result, isFormOwner = cache.SystemCache.FetchFormInfo(c.Param("formID"), []string{}, false)
	}
	if result == nil {
		c.AbortWithStatus(404)
	}
	result["registExceed"] = false
	if result["closeOn"].(map[string]interface{})["registered"] != nil {
		result["registExceed"] = (len(result["answered"].([]interface{})) >= int(result["closeOn"].(map[string]interface{})["registered"].(float64)))
	}
	if !isFormOwner {
		delete(result, "targetCollection")
		delete(result, "visited")
		delete(result, "answered")
	}
	c.JSON(200, gin.H{
		"result": result,
	})
}

func TableForm(c *gin.Context) {
	var result []gin.H
	session := sessions.Default(c)
	if util.StringInSlice(c.Param("tablename"), session.Get("permitTableName").([]string)) {
		mdb, err := mgo.Dial("mongo-mongodb-replicaset:27017")
		
		defer mdb.Close()
		util.CheckErr(err)
		mdb.DB("StupidProject2017").C("DATANALIEZ_TableFormInfo").Find(bson.M{
			"targetCollection": c.Param("tablename"),
		}).Select(bson.M{
			"formID": 1,
			"title":  1,
			"_id":    0,
		}).All(&result)
		c.JSON(200, gin.H{
			"result": result,
		})
	} else {
		c.AbortWithStatus(403)
	}
}

func CacheReporter(c *gin.Context) {
	c.JSON(200, gin.H{
		"Cached":   cache.Cached,
		"Uncached": cache.Uncache,
	})
}
