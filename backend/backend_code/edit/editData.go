package edit

import (
	"encoding/json"
	"reflect"
	"time"
	"gopkg.in/mgo.v2/bson"
	"../cache"
	"../util"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	mgo "gopkg.in/mgo.v2"
	"github.com/spf13/cast"
)
type Modify struct {
  ColumnName string
  Value string
}
type EditQuery struct {
  IdList []string
  ModifyList []Modify
}

//edit by current filter , scheduled to be refactored!!
func DataInTable(c *gin.Context) {
	session := sessions.Default(c)
	if util.StringInSlice(c.Param("tablename"), session.Get("permitTableName").([]string)) {
		var query EditQuery
		filterList := session.Get("filterList")
		thisTableFilter := []gin.H{nil}
		json.Unmarshal([]byte(c.Param("data")), &query)
		if filterList != nil {
			dereferenceFilterList := *(filterList.(*gin.H))
			dereferenceThisTableFilter := dereferenceFilterList[c.Param("tablename")]
			if dereferenceThisTableFilter != nil {
				thisTableFilter = *(dereferenceThisTableFilter.(*[]gin.H))
			}
		}
		EditFromFilter(c.Param("tablename"),thisTableFilter,query.ModifyList)
	} else {
		c.JSON(403, gin.H{"error": "permission denied"})
		return
	}
	c.JSON(200, gin.H{
		"result": "success",
	})
}

//edit by input id list
func DataByIdList(c *gin.Context) {
	session := sessions.Default(c)
	if util.StringInSlice(c.Param("tablename"), session.Get("permitTableName").([]string)) {
		var query EditQuery
		var thisTableFilter []gin.H
		json.Unmarshal([]byte(c.Param("data")), &query)
		objId := util.StringToOidList(query.IdList)
		thisTableFilter = append(thisTableFilter, gin.H{
			"column_name":"_id",
			"expression":"in",
			"value":objId,
		})
		EditFromFilter(c.Param("tablename"),thisTableFilter,query.ModifyList)
	} else {
		c.JSON(403, gin.H{"error": "permission denied"})
		return
	}
	c.JSON(200, gin.H{
		"result": "success",
	})
}

func EditFromFilter(tableName string,thisTableFilter []gin.H,modifyList []Modify) {
	var mdb *mgo.Session
	var result []gin.H
	var err error
	collectionStructureTransaction := gin.H{}
	
	nilSafeEval := func(a interface{}, b int) int {
		if a == nil {
			return b
		}
		return a.(int) + b
	}
	
	mdb, err = mgo.Dial("mongo-mongodb-replicaset:27017");
	defer mdb.Close()
	util.CheckErr(err)
	userTable := mdb.DB("StupidProject2017").C(tableName)
	aggregate := util.NewAggregator(cache.SystemCache.GetTableProperty(tableName))
	userTable.Pipe(aggregate.SetFilter(thisTableFilter).Finalize("deleteData")).All(&result)
	for _, record := range result {
		
		updateValue := make(map[string]interface{})
		normalParam := make(map[string]interface{})
		allStringParam := make(map[string]interface{})
		allDecimalParam := make(map[string]interface{})
		//map params for govaluate
		for field, value := range record {
			switch value.(type) {
			case bson.ObjectId:
				normalParam[field]= cast.ToString(value)
			case bool:
				normalParam[field]= cast.ToString(value)
			case time.Time:
				normalParam[field]= cast.ToString(value)
			case bson.Decimal128:
				normalParam[field]= cast.ToFloat64(cast.ToString(value))
			default:
				if _, err = bson.ParseDecimal128(value.(string)); err == nil {
					normalParam[field]= cast.ToFloat64(cast.ToString(value))
				} else {
					normalParam[field]= cast.ToString(value)
				}
			}
			allDecimalParam[field] = cast.ToFloat64(cast.ToString(value))
			allStringParam[field] = cast.ToString(value)
		}			
		for _, modifier := range modifyList  {
			field := modifier.ColumnName
			if field == "_id" {
				continue
			}
			if _, exist := collectionStructureTransaction[field]; !exist {
				collectionStructureTransaction[field] = gin.H{}
			}
			Expression := util.NewExpression(modifier.Value)
			if(Expression == nil){
				continue
			}
			//normal first priority
			expresult, err := Expression.Evaluate(normalParam)
			if err==nil {
				switch expresult.(type) {
				case float64:
					expresult, _ = bson.ParseDecimal128(cast.ToString(expresult))
					collectionStructureTransaction[field].(gin.H)["NumberDecimal"] = nilSafeEval(collectionStructureTransaction[field].(gin.H)["NumberDecimal"], 1)
				case bool:
					collectionStructureTransaction[field].(gin.H)["Boolean"] = nilSafeEval(collectionStructureTransaction[field].(gin.H)["Boolean"], -1)
				default:
					collectionStructureTransaction[field].(gin.H)["String"] = nilSafeEval(collectionStructureTransaction[field].(gin.H)["String"], 1)
				}
				goto pass
			}
			//number next priority
			expresult, err = Expression.Evaluate(allDecimalParam)
			if err==nil {
				expresult, _ = bson.ParseDecimal128(cast.ToString(expresult))
				collectionStructureTransaction[field].(gin.H)["NumberDecimal"] = nilSafeEval(collectionStructureTransaction[field].(gin.H)["NumberDecimal"], 1)
				goto pass
			}
			//string
			expresult, err = Expression.Evaluate(allStringParam)
			if err==nil {
				collectionStructureTransaction[field].(gin.H)["String"] = nilSafeEval(collectionStructureTransaction[field].(gin.H)["String"], 1)
				goto pass
			}
			//not a type error but some other error
			continue
			
			pass:
			if _, exist := record[field]; exist {
				value := record[field]
				switch value.(type) {
				case bson.ObjectId:
					collectionStructureTransaction[field].(gin.H)["ObjectId"] = nilSafeEval(collectionStructureTransaction[field].(gin.H)["ObjectId"], -1)
				case bool:
					collectionStructureTransaction[field].(gin.H)["Boolean"] = nilSafeEval(collectionStructureTransaction[field].(gin.H)["Boolean"], -1)
				case time.Time:
					collectionStructureTransaction[field].(gin.H)["Date"] = nilSafeEval(collectionStructureTransaction[field].(gin.H)["Date"], -1)
				case bson.Decimal128:
					collectionStructureTransaction[field].(gin.H)["NumberDecimal"] = nilSafeEval(collectionStructureTransaction[field].(gin.H)["NumberDecimal"], -1)
				default:
					if reflect.ValueOf(value).Kind() == reflect.Slice {
						collectionStructureTransaction[field].(gin.H)["Array"] = nilSafeEval(collectionStructureTransaction[field].(gin.H)["Array"], -1)
					} else if _, err = bson.ParseDecimal128(value.(string)); err == nil {
						collectionStructureTransaction[field].(gin.H)["NumberDecimal"] = nilSafeEval(collectionStructureTransaction[field].(gin.H)["NumberDecimal"], -1)
					} else {
						collectionStructureTransaction[field].(gin.H)["String"] = nilSafeEval(collectionStructureTransaction[field].(gin.H)["String"], -1)
					}
				}
			}
			updateValue[field]=expresult
		}
		for key, value := range updateValue {
			record[key] = value
		}
		userTable.Update(bson.M{"_id": record["_id"].(bson.ObjectId)}, record)
	}
	cache.SystemCache.UpdateTableProperty(tableName, collectionStructureTransaction)
}
