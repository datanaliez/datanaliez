package edit

import (
	"strconv"

	"../cache"
	"../util"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
)

func EditForm(c *gin.Context) {
	session := sessions.Default(c)
	_, isOwner := cache.SystemCache.FetchFormInfo(c.Param("formID"), session.Get("permitTableName").([]string), false)
	if isOwner {
		updateFormInfoParams := util.RequestFormatter(c.GetRawData())
		updateFormInfo, err := util.PrepareFormInfo(updateFormInfoParams)
		delete(updateFormInfo, "answered")
		delete(updateFormInfo, "visited")
		if err != nil {
			c.AbortWithError(500, err)
		}
		cache.SystemCache.FormEdit(c.Param("formID"), updateFormInfo)
		c.JSON(200, gin.H{
			"status": "success",
		})
	}
}

func SwitchFormEoDisable(c *gin.Context) {
	session := sessions.Default(c)
	_, isOwner := cache.SystemCache.FetchFormInfo(c.Param("formID"), session.Get("permitTableName").([]string), false)
	if isOwner {
		flag, err := strconv.ParseBool(c.Param("flag"))
		if err == nil {
			cache.SystemCache.FormEdit(c.Param("formID"), bson.M{"disabled": flag})
			c.JSON(200, gin.H{
				"status":       "success",
				"formDisabled": flag,
			})
		} else {
			c.JSON(200, gin.H{
				"status": "failed",
				"err":    "Flag isn't correct",
			})
		}
	}
}

func FlushFormStat(c *gin.Context) {
	session := sessions.Default(c)
	_, isOwner := cache.SystemCache.FetchFormInfo(c.Param("formID"), session.Get("permitTableName").([]string), false)
	if isOwner {
		switch c.Param("statType") {
		case "visited":
			cache.SystemCache.FormStatFlushing(c.Param("formID"), "visited")
		case "answered":
			cache.SystemCache.FormStatFlushing(c.Param("formID"), "answered")
		}
	}
}
