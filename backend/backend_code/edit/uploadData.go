package edit

import (
	"io"
	"os"

	"../cache"
	"../util"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"github.com/tealeg/xlsx"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func UploadData(c *gin.Context) {
	session := sessions.Default(c)
	var columnName []string
	tmpData := make(bson.M, 0)
	collectionStructureTransaction := gin.H{}
	nilSafeEval := func(a interface{}, b int) int {
		if a == nil {
			return b
		}
		return a.(int) + b
	}
	if util.StringInSlice(c.Param("tablename"), session.Get("permitTableName").([]string)) {
		msession, err := mgo.Dial("mongo-mongodb-replicaset:27017")
		defer msession.Close()
		util.CheckErr(err)
		userTB := msession.DB("StupidProject2017").C(c.Param("tablename"))
		file, _, err := c.Request.FormFile("dataFile")
		if err != nil {
			c.AbortWithStatus(500)
		}
		tmpFileName := "./uploadTMP/" + util.SHA256(util.RandomString(15)) + ".tmp"
		tmpFile, _ := os.Create(tmpFileName)
		io.Copy(tmpFile, file)
		tmpFile.Close()
		xlFile, err := xlsx.OpenFile(tmpFileName)
		if err != nil {
			c.AbortWithStatus(500)
		}
		for _, sheet := range xlFile.Sheets {
			for rowIndex, row := range sheet.Rows {
				for cellIndex, cell := range row.Cells {
					if rowIndex == 0 {
						columnName = append(columnName, cell.String())
					} else {
						field := columnName[cellIndex]
						if _, exist := collectionStructureTransaction[field]; !exist {
							collectionStructureTransaction[field] = gin.H{}
						}
						//special case: _id provided
						if(field == "_id") {
							if(bson.IsObjectIdHex(cell.String())) {
								tmpData[field] = bson.ObjectIdHex(cell.String())
							}
						} else {
							switch cell.Type() {
							case xlsx.CellTypeNumeric:
								tmpData[field], _ = bson.ParseDecimal128(cell.String())
								collectionStructureTransaction[field].(gin.H)["NumberDecimal"] = nilSafeEval(collectionStructureTransaction[field].(gin.H)["NumberDecimal"], 1)
							case xlsx.CellTypeBool:
								tmpData[field] = cell.Bool()
								collectionStructureTransaction[field].(gin.H)["Boolean"] = nilSafeEval(collectionStructureTransaction[field].(gin.H)["Boolean"], 1)
							default:
								tmpData[field] = cell.String()
								collectionStructureTransaction[field].(gin.H)["String"] = nilSafeEval(collectionStructureTransaction[field].(gin.H)["String"], 1)
							}
						}
					}
				}
				if rowIndex != 0 {
					userTB.Insert(tmpData)
				}
			}
		}
		// fmt.Printf("%v", collectionStructureTransaction)
		cache.SystemCache.UpdateTableProperty(c.Param("tablename"), collectionStructureTransaction)
		tmpFile.Close()
		err = os.Remove(tmpFileName)
		util.CheckErr(err)
	}
}
