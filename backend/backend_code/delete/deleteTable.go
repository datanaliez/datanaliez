package delete

import (
	"database/sql"

	"../cache"
	"../util"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"gopkg.in/gorp.v1"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

var (
	Dbmap *gorp.DbMap
	db    *sql.DB
)

func Init() {
	Dbmap, db = util.InitDb()
}
func DeleteTable(c *gin.Context) {
	session := sessions.Default(c)
	var mdb *mgo.Session
	var err error
	if util.StringInSlice(c.Param("tablename"), session.Get("permitTableName").([]string)) {
		go func() {
			Init()
			stmt, err := db.Prepare("DELETE FROM `user_table` WHERE `tablename` = ?")
			util.CheckErr(err)
			_, err = stmt.Exec(c.Param("tablename"))
			util.CheckErr(err)
			db.Close()
		}()
		mdb, err = mgo.Dial("mongo-mongodb-replicaset:27017");
		defer mdb.Close()
		util.CheckErr(err)
		err = mdb.DB("StupidProject2017").C(c.Param("tablename")).DropCollection()
		util.CheckErr(err)
		err = mdb.DB("StupidProject2017").C(c.Param("tablename") + "_keys").DropCollection()
		util.CheckErr(err)
		err = mdb.DB("StupidProject2017").C("DATANALIEZ_TableFormInfo").Remove(bson.M{"targetCollection": c.Param("tablename")})
		cache.SystemCache.FlushTableProperty(c.Param("tablename"))
		util.CheckErr(err)
		newPermitTableName := make([]string, 0)
		for _, permitTableName := range session.Get("permitTableName").([]string) {
			if permitTableName != c.Param("tablename") {
				newPermitTableName = append(newPermitTableName, permitTableName)
			}
		}
		session.Set("permitTableName", newPermitTableName)
		c.JSON(200, gin.H{
			"result": "OK",
		})
	} else {
		c.JSON(200, gin.H{
			"error": "permission denied",
		})
	}
}
