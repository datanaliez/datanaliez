package delete

import (
	"reflect"
	"time"
	"strings"
	"gopkg.in/mgo.v2/bson"
	"../cache"
	"../util"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	mgo "gopkg.in/mgo.v2"
)
//delete by current filter , scheduled to be refactored!!
func DataInTable(c *gin.Context) {
	session := sessions.Default(c)
	if util.StringInSlice(c.Param("tablename"), session.Get("permitTableName").([]string)) {
		
		filterList := session.Get("filterList")
		thisTableFilter := []gin.H{nil}
		if filterList != nil {
			dereferenceFilterList := *(filterList.(*gin.H))
			dereferenceThisTableFilter := dereferenceFilterList[c.Param("tablename")]
			if dereferenceThisTableFilter != nil {
				thisTableFilter = *(dereferenceThisTableFilter.(*[]gin.H))
			}
		}
		DeleteFromFilter(c.Param("tablename"),thisTableFilter)
	} else {
		c.JSON(403, gin.H{"error": "permission denied"})
		return
	}
	c.JSON(200, gin.H{
		"result": "success",
	})
}
//delete by input id list
func DataByIdList(c *gin.Context) {
	session := sessions.Default(c)
	if util.StringInSlice(c.Param("tablename"), session.Get("permitTableName").([]string)) {
		var thisTableFilter []gin.H
		idList := strings.Split(c.Param("idList"),",")
		objId := util.StringToOidList(idList)
		thisTableFilter = append(thisTableFilter, gin.H{
			"column_name":"_id",
			"expression":"in",
			"value":objId,
		})
		DeleteFromFilter(c.Param("tablename"),thisTableFilter)
	} else {
		c.JSON(403, gin.H{"error": "permission denied"})
		return
	}
	c.JSON(200, gin.H{
		"result": "success",
	})
}
//send in filter and tablename and be happy
func DeleteFromFilter(tableName string,thisTableFilter []gin.H) {
	var mdb *mgo.Session
	var result []gin.H
	var err error
	collectionStructureTransaction := gin.H{}
	nilSafeEval := func(a interface{}, b int) int {
		if a == nil {
			return b
		}
		return a.(int) + b
	}
	mdb, err = mgo.Dial("mongo-mongodb-replicaset:27017");
	defer mdb.Close()
	util.CheckErr(err)
	userTable := mdb.DB("StupidProject2017").C(tableName)
	aggregate := util.NewAggregator(cache.SystemCache.GetTableProperty(tableName))
	userTable.Pipe(aggregate.SetFilter(thisTableFilter).Finalize("deleteData")).All(&result)
	for _, record := range result {
		for field, value := range record {
			if _, exist := collectionStructureTransaction[field]; !exist {
				collectionStructureTransaction[field] = gin.H{}
			}
			switch value.(type) {
			case bson.ObjectId:
				collectionStructureTransaction[field].(gin.H)["ObjectId"] = nilSafeEval(collectionStructureTransaction[field].(gin.H)["ObjectId"], -1)
			case bool:
				collectionStructureTransaction[field].(gin.H)["Boolean"] = nilSafeEval(collectionStructureTransaction[field].(gin.H)["Boolean"], -1)
			case time.Time:
				collectionStructureTransaction[field].(gin.H)["Date"] = nilSafeEval(collectionStructureTransaction[field].(gin.H)["Date"], -1)
			case bson.Decimal128:
				collectionStructureTransaction[field].(gin.H)["NumberDecimal"] = nilSafeEval(collectionStructureTransaction[field].(gin.H)["NumberDecimal"], -1)
			default:
				if reflect.ValueOf(value).Kind() == reflect.Slice {
					collectionStructureTransaction[field].(gin.H)["Array"] = nilSafeEval(collectionStructureTransaction[field].(gin.H)["Array"], -1)
				} else if _, err = bson.ParseDecimal128(value.(string)); err == nil {
					collectionStructureTransaction[field].(gin.H)["NumberDecimal"] = nilSafeEval(collectionStructureTransaction[field].(gin.H)["NumberDecimal"], -1)
				} else {
					collectionStructureTransaction[field].(gin.H)["String"] = nilSafeEval(collectionStructureTransaction[field].(gin.H)["String"], -1)
				}
			}
		}
		userTable.RemoveId(record["_id"].(bson.ObjectId))
	}
	cache.SystemCache.UpdateTableProperty(tableName, collectionStructureTransaction)
}
