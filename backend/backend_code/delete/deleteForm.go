package delete

import (
	"../cache"
	"../util"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
)

func DeleteForm(c *gin.Context) {
	session := sessions.Default(c)
	_, isOwner := cache.SystemCache.FetchFormInfo(c.Param("formID"), session.Get("permitTableName").([]string), false)
	if isOwner {
		err := cache.SystemCache.FormDelete(c.Param("formID"))
		if util.CheckErr(err) {
			c.JSON(200, gin.H{
				"status": "error",
				"errMSG": err,
			})
		} else {
			c.JSON(200, gin.H{
				"status": "success",
			})
		}
	} else {
		c.AbortWithStatus(404)
	}
}
