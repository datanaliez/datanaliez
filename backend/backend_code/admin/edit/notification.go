package adminEdit

import (
	"database/sql"
	"gopkg.in/gorp.v1"
	"github.com/gin-gonic/gin"
	_"github.com/go-sql-driver/mysql"
	"../../util"
)
var (
	Dbmap *gorp.DbMap
	db *sql.DB
)
func Init()  {
	Dbmap, db = util.InitDb()
}
func Notification(c *gin.Context) {
	Init()
	jsonParams:=util.RequestFormatter(c.GetRawData())
	if util.StringInSlice(jsonParams["column_name"].(string), []string{"pubilc_date","headline", "body", "priority"}){
		_, err := db.Query("UPDATE `notification` SET `"+jsonParams["column_name"].(string)+"` = ? WHERE `notification`.`ID` = ?", jsonParams["value"], c.Param("ID"))
		if (!util.CheckErr(err)){
			c.JSON(200, gin.H{"result":"OK"})
		}else {
			c.JSON(500, gin.H{"result":err.Error()})
		}
		db.Close()
	}else{
		c.JSON(500, gin.H{"result":"Column name not found in table!"})
	}
}
