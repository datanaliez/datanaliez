package adminDelete

import (
	"database/sql"
	"gopkg.in/gorp.v1"
	"github.com/gin-gonic/gin"
	_"github.com/go-sql-driver/mysql"
	"../../util"
)
var (
	Dbmap *gorp.DbMap
	db *sql.DB
)
func Init()  {
	Dbmap, db = util.InitDb()
}
func Notification(c *gin.Context) {
	Init()
	_, err := db.Query("DELETE FROM notification WHERE ID = ?", c.Param("ID"))
	if (!util.CheckErr(err)){
		c.JSON(200, gin.H{"result":"OK"})
	}else {
		c.JSON(200, gin.H{"result":"Failed"})
	}
	db.Close()
}
