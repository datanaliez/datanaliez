package adminDelete

import (
	"github.com/gin-gonic/gin"
	_"github.com/go-sql-driver/mysql"
	"../../util"
)
func User(c *gin.Context) {
	Init()
	_, err := db.Query("DELETE FROM user WHERE ID = ?", c.Param("ID"))
	if (!util.CheckErr(err)){
		c.JSON(200, gin.H{"result":"OK"})
	}else {
		c.JSON(200, gin.H{"result":"Failed"})
	}
	db.Close()
}
