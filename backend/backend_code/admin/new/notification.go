package adminNew

import (
	"database/sql"

	"../../util"
	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
	"gopkg.in/gorp.v1"
)

var (
	Dbmap *gorp.DbMap
	db    *sql.DB
)

func Init() {
	Dbmap, db = util.InitDb()
}
func Notification(c *gin.Context) {
	Init()
	jsonParams := util.RequestFormatter(c.GetRawData())
	_, err := db.Query("INSERT INTO `notification` (`public_date`, `headline`, `body`, `priority`) VALUES (CURRENT_TIMESTAMP, ?, ?, ?)", jsonParams["headline"], jsonParams["body"], jsonParams["priority"])
	if !util.CheckErr(err) {
		c.JSON(200, gin.H{"result": "OK"})
	} else {
		c.JSON(500, gin.H{"result": err.Error()})
	}
	db.Close()
}
