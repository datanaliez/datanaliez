package adminGet

import (
	"database/sql"
	"gopkg.in/gorp.v1"
	"github.com/gin-gonic/gin"
	_"github.com/go-sql-driver/mysql"
	"../../util"
)
var (
	Dbmap *gorp.DbMap
	db *sql.DB
)
func Init()  {
	Dbmap, db = util.InitDb()
}
func User(c *gin.Context) {
	Init()
	rows, err := db.Query("SELECT `ID`,`permission`,`username` FROM user ORDER BY `ID` ASC")
	util.CheckErr(err)
	result:=util.ConvertRawBytesToMap(rows)
	c.JSON(200, result)
	db.Close()
}
