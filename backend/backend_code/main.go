package main

import (
	"encoding/gob"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"./admin/delete"
	"./admin/edit"
	"./admin/get"
	"./admin/new"
	"./cache"
	"./delete"
	"./edit"
	"./get"
	"./login"
	"./new"
	"./util"
	"github.com/gin-contrib/cors"
	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/static"
	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis"
	_ "github.com/go-sql-driver/mysql"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

var minimumAuthenLevel = map[string]int{
	"get":    0,
	"new":    1,
	"edit":   1,
	"delete": 1,
	"admin":  2,
}

func middleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		var requestedOperation string
		session := sessions.Default(c)
		if len(strings.Split(c.Request.RequestURI, "/")) > 3 {
			requestedOperation = strings.Trim(strings.Split(c.Request.RequestURI, "/")[3], " ")
		} else {
			requestedOperation = ""
		}
		requiredLevel, authenRequired := minimumAuthenLevel[requestedOperation]
		var userLevel int64
		var isOk error
		if userLevel = -1; session.Get("permission") != nil {
			userLevel, isOk = strconv.ParseInt(session.Get("permission").(string), 0, 64)
			if isOk != nil {
				panic(isOk)
				return
			}
		}
		if authenRequired && (session.Get("permission") == nil || int(userLevel) < requiredLevel) {
			c.AbortWithStatus(403)
		} else {
			c.Next()
		}
	}
}
func setup() *gin.Engine {
	gob.Register(&gin.H{})
	gob.Register(&bson.M{})
	gob.Register(&[]gin.H{})
	gob.Register(&bson.RegEx{})
	cache.SystemCache = cache.Initiate(&redis.Options{
		Addr:     "redis-redis-ha-master-svc:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})
	go cache.SystemCache.FormCacheSentinel()
	r := gin.Default()
	store, err := sessions.NewRedisStore(100, "tcp", "redis-redis-ha-master-svc:6379", "", []byte("etr54083"))
	util.CheckErr(err)
	r.NoRoute(func(c *gin.Context) {
		indexPath, _ := filepath.Abs("static/index.html")
		c.File(indexPath)
	})
	r.Use(static.Serve("/", static.LocalFile("./static", true)), sessions.Sessions("mysession", store), cors.Default(), middleware())
	api := r.Group("api/v1")
	{
		api.POST("login", login.CheckLogin)
		api.GET("loginStatus", login.LoginStatus)
		api.GET("logout", login.Logout)
		api.GET("greeting", login.GenerateChallenge)
		formAPI := api.Group("form")
		formAPI.GET("debug", get.CacheReporter)
		formAPI.GET("info/:formID", get.FormData)
		formAPI.POST("submit/:formID", new.FormSubmission)
		editAPI := api.Group("edit")
		editAPI.POST("uploadData/:tablename", edit.UploadData)
		editAPI.POST("form/:formID", edit.EditForm)
		editAPI.GET("formSwitch/:formID/:flag", edit.SwitchFormEoDisable)
		editAPI.GET("flushFormStat/:formID/:statType", edit.FlushFormStat)
		editAPI.POST("editCheckedData/:tablename/:data", edit.DataByIdList)
		editAPI.POST("editSelectData/:tablename/:data", edit.DataInTable)
		newAPI := api.Group("new")
		newAPI.GET("createTable/:tablename", new.CreateTable)
		newAPI.POST("createForm/:tablename", new.CreateForm)
		deleteAPI := api.Group("delete")
		deleteAPI.GET("deleteTable/:tablename", delete.DeleteTable)
		deleteAPI.GET("deleteSelectData/:tablename", delete.DataInTable)
		deleteAPI.GET("deleteCheckedData/:tablename/:idList", delete.DataByIdList)
		deleteAPI.GET("form/:formID", delete.DeleteForm)
		getAPI := api.Group("get")
		getAPI.GET("notification", get.Notification)
		getAPI.GET("tableList", get.TableList)
		getAPI.GET("isTableExist/:tablename", get.IsTableNameExist)
		getAPI.GET("allColumnName/:tablename", get.GetAllColumnName)
		getAPI.GET("columnType/:tablename/:columnName", get.GetTypeOfColumn)
		getAPI.GET("tableForm/:tablename", get.TableForm)
		getAPI.POST("export/:tablename", get.ExportDataAsXLSX)
		getAPI.POST("histogram/:tablename", get.Histogram)
		getAPI.POST("data/:tablename/:page", get.DataInTable)
		getAPI.POST("setFilter/:tablename", get.SetFilter)
		getAPI.POST("setSelectedColumn/:tablename", get.SelectSomeColumn)
		adminAPI := api.Group("admin")
		adminDeleteAPI := adminAPI.Group("delete")
		adminDeleteAPI.GET("notification/:ID", adminDelete.Notification)
		adminDeleteAPI.GET("user/:ID", adminDelete.User)
		adminEditAPI := adminAPI.Group("edit")
		adminEditAPI.POST("notification/:ID", adminEdit.Notification)
		adminEditAPI.POST("user/:ID", adminEdit.User)
		adminNewAPI := adminAPI.Group("new")
		adminNewAPI.POST("notification", adminNew.Notification)
		adminNewAPI.POST("user", adminNew.User)
		adminGetAPI := adminAPI.Group("get")
		adminGetAPI.GET("user", adminGet.User)
	}
	return r
}
func setupDB() {
	mdb, err := mgo.Dial("mongo-mongodb-replicaset:27017")
	defer mdb.Close()
	util.CheckErr(err)
	mdb.DB("StupidProject2017").C("Init").Insert(bson.M{
		"Timestamp": time.Now(),
	})
}
func main() {
	setupDB()
	setup().Run(":3010")
}
