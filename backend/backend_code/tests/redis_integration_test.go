package tests

import (
	"testing"

	"encoding/json"

	"../util"
	"github.com/go-redis/redis"
	"github.com/stretchr/testify/assert"
)

var redisConn *redis.Client

func TestMain(m *testing.M) {
	redisConn = redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})
	m.Run()
}
func TestBasicStowage(t *testing.T) {
	redisConn.Set("foo", 5, 0)
	result, err := redisConn.Get("foo").Result()
	assert.Equal(t, nil, err)
	assert.Equal(t, "5", result)
	redisConn.Del("foo")
}

func TestMapStowage(t *testing.T) {
	sample := map[string]string{
		"_id":       "objectId",
		"fullname":  "string",
		"skills":    "array",
		"sex":       "bool",
		"birthdate": "date",
		"gpax":      "decimal",
	}
	stringifiedData, _ := json.Marshal(sample)
	redisConn.Set("collectionStructure", stringifiedData, 0)
	result, err := redisConn.Get("collectionStructure").Result()
	assert.Equal(t, nil, err)
	formattedOut := util.RequestFormatter([]byte(result), nil)
	for key, value := range sample {
		assert.Equal(t, value, formattedOut[key])
	}
	redisConn.Del("collectionStructure")
}
func TestHashingBasicStowage(t *testing.T) {
	redisConn.HSet("dbStructure", "MidtermScore2017", "cto2018")
	result, err := redisConn.HGet("dbStructure", "MidtermScore2017").Result()
	assert.Equal(t, nil, err)
	assert.Equal(t, "cto2018", result)
	redisConn.Del("dbStructure")
}

func TestGetNothing(t *testing.T) {
	_, err := redisConn.Get("fd9s8938434dsfa").Result()
	assert.NotEqual(t, nil, err)
	// assert.Equal(t, nil, result)
}
