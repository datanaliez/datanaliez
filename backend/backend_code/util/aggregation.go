package util

import (
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
)

const sortKey = "6731940c49a028ca776b834221b2de90ebacd2721d37d3f3f37e75919c645abc"

type Aggregator struct {
	FirstProjection bson.M
	Filter          []bson.M
	Orderby         int
	LastProjection  bson.M
	Grouper         []bson.M
	Limit           []bson.M
	DBStructure     map[string]string
}

func typeConversion(value string, expectedType string) interface{} {
	var parsed interface{}
	var err error
	defer func() {
		recover()
	}()
	switch expectedType {
	case "Array":
		parsed, err = strconv.Atoi(value)
	case "ObjectId":
		parsed = bson.ObjectIdHex(value)
	case "Date":
		parsed, err = time.Parse(time.RFC3339, value)
	default:
		if parsed, err = strconv.ParseBool(value); err != nil {
			parsed, err = bson.ParseDecimal128(value)
		}
	}
	if CheckErr(err) {
		return value
	}
	return parsed
}
func NewAggregator(dbStructure map[string]string) *Aggregator {
	aggregate := new(Aggregator)
	aggregate.DBStructure = dbStructure
	aggregate.FirstProjection = bson.M{
		"$addFields": bson.M{
			sortKey: "_id",
		},
	}
	aggregate.LastProjection = bson.M{sortKey: 0}
	aggregate.Grouper = []bson.M{}
	aggregate.Filter = []bson.M{}
	aggregate.Limit = []bson.M{}
	aggregate.Orderby = 1
	return aggregate
}

func (aggregate *Aggregator) SetFilter(FilterConfig []gin.H) *Aggregator {
	for _, condition := range FilterConfig {
		conditionColumnName := condition["column_name"].(string)
		conditionColumnType := aggregate.DBStructure[conditionColumnName]
		if conditionColumnType == "array" {
			conditionColumnName += "_4bd098f2776ba934cf2c7c181837068f4ae6f2ca"
			aggregate.FirstProjection["$addFields"].(bson.M)[conditionColumnName] = bson.M{"$size": "$" + conditionColumnName}
			aggregate.LastProjection[conditionColumnName] = 0
		}
		switch condition["expression"] {
		case "eq":
			aggregate.Filter = append(aggregate.Filter, bson.M{conditionColumnName: typeConversion(condition["value"].(string), conditionColumnType)})
		case "gt":
			aggregate.Filter = append(aggregate.Filter, bson.M{conditionColumnName: bson.M{"$gt": typeConversion(condition["value"].(string), conditionColumnType)}})
		case "lt":
			aggregate.Filter = append(aggregate.Filter, bson.M{conditionColumnName: bson.M{"$lt": typeConversion(condition["value"].(string), conditionColumnType)}})
		case "ge":
			aggregate.Filter = append(aggregate.Filter, bson.M{conditionColumnName: bson.M{"$gte": typeConversion(condition["value"].(string), conditionColumnType)}})
		case "le":
			aggregate.Filter = append(aggregate.Filter, bson.M{conditionColumnName: bson.M{"$lte": typeConversion(condition["value"].(string), conditionColumnType)}})
		case "like":
			stringifyProjection := bson.M{
				"$cond": []interface{}{
					bson.M{"$eq": []interface{}{bson.M{"$type": "$" + conditionColumnName}, "array"}},
					"$" + conditionColumnName,
					bson.M{"$substr": []interface{}{"$" + conditionColumnName, 0, -1}},
				},
			}
			aggregate.FirstProjection["$addFields"].(bson.M)[conditionColumnName] = stringifyProjection
			aggregate.Filter = append(aggregate.Filter, bson.M{conditionColumnName: bson.RegEx{condition["value"].(string), ""}})
                //for now please only use this for _id
		case "in":
			aggregate.Filter = append(aggregate.Filter, bson.M{conditionColumnName: bson.M{"$in": condition["value"].([]bson.ObjectId)}})
		}
	}
	return aggregate
}

func (aggregate *Aggregator) Sorting(sortby string, orderby string) *Aggregator {
	sortKeyProjection := bson.M{
		"$cond": []interface{}{
			bson.M{"$eq": []interface{}{bson.M{"$type": "$" + sortby}, "array"}},
			bson.M{"$size": "$" + sortby},
			"$" + sortby,
		},
	}
	aggregate.FirstProjection["$addFields"].(bson.M)[sortKey] = sortKeyProjection
	if orderby == "descend" {
		aggregate.Orderby = -1
	} else {
		aggregate.Orderby = 1
	}
	return aggregate
}

func (aggregate *Aggregator) SelectColumn(selectedColumns []string) *Aggregator {
	aggregate.LastProjection["_id"] = 0
	if len(selectedColumns) > 0 {
		for _, column := range selectedColumns {
			aggregate.LastProjection[column] = bson.M{
				"$cond": []interface{}{
					bson.M{"$eq": []interface{}{bson.M{"$type": "$" + column}, "decimal"}},
					bson.M{"$substr": []interface{}{"$" + column, 0, -1}},
					"$" + column,
				},
			}
		}
		delete(aggregate.LastProjection, sortKey)
	}
	return aggregate
}

func (aggregate *Aggregator) SetLimit(skip int, limit int) *Aggregator {
	aggregate.Limit = []bson.M{bson.M{"$skip": skip}, bson.M{"$limit": limit}}
	return aggregate
}

func (aggregate *Aggregator) Histogroup(columnName string, groupBy interface{}) *Aggregator {
	aggregate.Grouper = []bson.M{
		bson.M{"$unwind": columnName},
		bson.M{
			"$group": bson.M{
				"_id": groupBy,
				"count": bson.M{
					"$sum": 1,
				},
			},
		},
	}
	return aggregate
}

// Flow model
//								  |-> HistoGroup
// Project -> Match |-> Sorting -> Limit -> Select Column (Project Again)

// db.TestArraySorting.aggregate([ {  "$addFields":{ "6731940c49a028ca776b834221b2de90ebacd2721d37d3f3f37e75919c645abc":{ "$cond":[{"$eq":[{"$type":"$job"},"array"]},{"$size":"$job"},"$job"]} } } ,{"$sort":{"6731940c49a028ca776b834221b2de90ebacd2721d37d3f3f37e75919c645abc":-1}},{"$project":{"6731940c49a028ca776b834221b2de90ebacd2721d37d3f3f37e75919c645abc":0}}])
func (aggregate *Aggregator) Finalize(role string) []bson.M {
	compliedAggregration := []bson.M{aggregate.FirstProjection}
	if len(aggregate.Filter) > 0 {
		compliedAggregration = append(compliedAggregration, bson.M{
			"$match": bson.M{
				"$and": aggregate.Filter,
			},
		})
	}
	switch role {
	case "data":
		compliedAggregration = append(compliedAggregration, bson.M{
			"$sort": bson.M{sortKey: aggregate.Orderby, "_id": 1},
		})
		if len(aggregate.Limit) > 0 {
			compliedAggregration = append(compliedAggregration, aggregate.Limit...)
		}
		compliedAggregration = append(compliedAggregration, bson.M{
			"$project": aggregate.LastProjection,
		})
	case "dataCount":
		compliedAggregration = append(compliedAggregration, bson.M{"$count": "count"})
	case "histogram":
		compliedAggregration = append(compliedAggregration, aggregate.Grouper...)
		compliedAggregration = append(compliedAggregration, bson.M{
			"$project": bson.M{
				"count": 1,
				"_id": bson.M{
					"$cond": []interface{}{
						bson.M{"$eq": []interface{}{bson.M{"$type": "$_id"}, "decimal"}},
						bson.M{"$substr": []interface{}{"$_id", 0, -1}},
						"$_id",
					},
				},
				// bson.M{"$substr": []interface{}{"$_id", 0, -1}},
			},
		})
		compliedAggregration = append(compliedAggregration, bson.M{"$sort": bson.M{"_id": 1}})
	case "deleteData":
		compliedAggregration = append(compliedAggregration, bson.M{
			"$project": aggregate.LastProjection,
		})
	}
	return compliedAggregration
}
