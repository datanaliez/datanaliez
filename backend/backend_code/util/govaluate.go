package util

import (
	"github.com/Knetic/govaluate"
	"math"
)

func NewExpression(expression string) *govaluate.EvaluableExpression {
	
	functions := map[string]govaluate.ExpressionFunction {
		"strlen": func(args ...interface{}) (interface{}, error) {
			length := len(args[0].(string))
			return (float64)(length), nil
		},
		"abs": func(args ...interface{}) (interface{}, error) {
			return (float64)(math.Abs(args[0].(float64))), nil
		},
		"acos": func(args ...interface{}) (interface{}, error) {
			return (float64)(math.Acos(args[0].(float64))), nil
		},
		"acosh": func(args ...interface{}) (interface{}, error) {
			return (float64)(math.Acosh(args[0].(float64))), nil
		},
		"asin": func(args ...interface{}) (interface{}, error) {
			return (float64)(math.Asin(args[0].(float64))), nil
		},
		"asinh": func(args ...interface{}) (interface{}, error) {
			return (float64)(math.Asinh(args[0].(float64))), nil
		},
		"atan": func(args ...interface{}) (interface{}, error) {
			return (float64)(math.Atan(args[0].(float64))), nil
		},
		"atanh": func(args ...interface{}) (interface{}, error) {
			return (float64)(math.Atanh(args[0].(float64))), nil
		},
		"cbrt": func(args ...interface{}) (interface{}, error) {
			return (float64)(math.Cbrt(args[0].(float64))), nil
		},
		"ceil": func(args ...interface{}) (interface{}, error) {
			return (float64)(math.Ceil(args[0].(float64))), nil
		},
		"cos": func(args ...interface{}) (interface{}, error) {
			return (float64)(math.Cos(args[0].(float64))), nil
		},
		"cosh": func(args ...interface{}) (interface{}, error) {
			return (float64)(math.Cosh(args[0].(float64))), nil
		},
		"exp": func(args ...interface{}) (interface{}, error) {
			return (float64)(math.Exp(args[0].(float64))), nil
		},
		"exp2": func(args ...interface{}) (interface{}, error) {
			return (float64)(math.Exp2(args[0].(float64))), nil
		},
		"floor": func(args ...interface{}) (interface{}, error) {
			return (float64)(math.Floor(args[0].(float64))), nil
		},
		"log": func(args ...interface{}) (interface{}, error) {
			return (float64)(math.Log(args[0].(float64))), nil
		},
		"log10": func(args ...interface{}) (interface{}, error) {
			return (float64)(math.Log10(args[0].(float64))), nil
		},
		"log2": func(args ...interface{}) (interface{}, error) {
			return (float64)(math.Log2(args[0].(float64))), nil
		},
		"max": func(args ...interface{}) (interface{}, error) {
			return (float64)(math.Max(args[0].(float64),args[1].(float64))), nil
		},
		"min": func(args ...interface{}) (interface{}, error) {
			return (float64)(math.Min(args[0].(float64),args[1].(float64))), nil
		},
		"pow": func(args ...interface{}) (interface{}, error) {
			return (float64)(math.Pow(args[0].(float64),args[1].(float64))), nil
		},
		"sin": func(args ...interface{}) (interface{}, error) {
			return (float64)(math.Sin(args[0].(float64))), nil
		},
		"sinh": func(args ...interface{}) (interface{}, error) {
			return (float64)(math.Sinh(args[0].(float64))), nil
		},
		"sqrt": func(args ...interface{}) (interface{}, error) {
			return (float64)(math.Sqrt(args[0].(float64))), nil
		},
		"tan": func(args ...interface{}) (interface{}, error) {
			return (float64)(math.Tan(args[0].(float64))), nil
		},
		"tanh": func(args ...interface{}) (interface{}, error) {
			return (float64)(math.Tanh(args[0].(float64))), nil
		},
		"pi": func(args ...interface{}) (interface{}, error) {
			return (float64)(math.Pi), nil
		},
	}
	
	newExpression, err := govaluate.NewEvaluableExpressionWithFunctions(expression, functions)
	CheckErr(err)
	return newExpression
}

