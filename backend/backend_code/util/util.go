package util

import (
	"crypto/sha256"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"math/rand"
	"reflect"
	"time"

	"github.com/gin-gonic/gin"
	"gopkg.in/gorp.v1"
	"gopkg.in/mgo.v2/bson"
)

const charset = "abcdefghijklmnopqrstuvwxyz" +
	"ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

var seededRand *rand.Rand = rand.New(
	rand.NewSource(time.Now().UnixNano()))

func DereferenceStructure(pointer *gin.H) gin.H {
	main := make(gin.H)
	for key, val := range *pointer {
		if reflect.ValueOf(val).Kind() != reflect.Ptr {
			main[key] = val
		} else {
			ginObj, err := val.(*gin.H)
			if err {
				main[key] = DereferenceStructure(ginObj)
			} else {
				main[key] = *(val.(*bson.RegEx))
			}
		}
	}
	return main
}

func Columns(queryResult []gin.H) []string {
	var columns []string
	columnGathering := make(map[string]bool, 1)
	for _, row := range queryResult {
		for name, _ := range row {
			columnGathering[name] = true
		}
	}
	for columnName, _ := range columnGathering {
		columns = append(columns, columnName)
	}
	return columns
}

func StringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

func ConvertMapArrayToJson(table []map[string]string) gin.H {
	var result gin.H = make(gin.H, 1)
	for index := 0; index < len(table); index++ {
		result[fmt.Sprintf("%d", index)] = table[index]
	}
	return result
}

func RequestFormatter(bytecode []byte, err error) gin.H {
	var jsonStructure gin.H
	if err != nil {
		fmt.Printf("Error when read raw request")
	}
	json.Unmarshal(bytecode, &jsonStructure)
	return jsonStructure
}

func ArrayRequestFormatter(bytecode []byte, err error) []gin.H {
	var jsonStructure []gin.H
	if err != nil {
		fmt.Printf("Error when read raw request")
	}
	json.Unmarshal(bytecode, &jsonStructure)
	return jsonStructure
}

func InitDb() (*gorp.DbMap, *sql.DB) {
	db, err := sql.Open("mysql", "root:en0n1gm0us@tcp(mysql:3306)/stupid_project")
	CheckErr(err)
	dbmap := &gorp.DbMap{Db: db, Dialect: gorp.MySQLDialect{"InnoDB", "UTF8"}}
	return dbmap, db
}

func SHA256(password string) string {
	hasher := sha256.New()
	hasher.Write([]byte(password))
	return fmt.Sprintf("%x", hasher.Sum(nil))
}

func CheckErr(err error) bool {
	if err != nil {
		fmt.Printf("[! - Error]:%s\n", err)
		return true
	}
	return false
}

func ConvertRawBytesToMap(rows *sql.Rows) []map[string]string {
	var table []map[string]string
	var row = make(map[string]string)
	columns, err := rows.Columns()
	CheckErr(err)
	eachRows := make([]sql.RawBytes, len(columns))
	scanArgs := make([]interface{}, len(columns))
	for i := range eachRows {
		scanArgs[i] = &eachRows[i]
	}
	for index := 0; rows.Next(); index++ {
		CheckErr(rows.Scan(scanArgs...))
		for i, col := range eachRows {
			row[columns[i]] = string(col)
		}
		table = append(table, row)
		row = make(map[string]string)
	}
	return table
}

func RandomStringWithCharset(length int, charset string) string {
	b := make([]byte, length)
	for i := range b {
		b[i] = charset[seededRand.Intn(len(charset))]
	}
	return string(b)
}

func RandomString(length int) string {
	return RandomStringWithCharset(length, charset)
}
func PrepareFormInfo(newFormInfoParams gin.H) (bson.M, error) {
	newFormInfo := bson.M{
		"title":     newFormInfoParams["title"].(string),
		"desc":      newFormInfoParams["desc"].(string),
		"visited":   []time.Time{},
		"answered":  []time.Time{},
		"fieldList": []gin.H{},
		"closeOn":   gin.H{},
		"disabled": func(disabled interface{}) bool {
			if disabled != nil {
				return disabled.(bool)
			}
			return false
		}(newFormInfoParams["disabled"]),
	}
	for _, field := range newFormInfoParams["field_list"].([]interface{}) {
		field := field.(map[string]interface{})
		fieldInfo := gin.H{
			"name":     field["name"].(string),
			"label":    field["label"].(string),
			"type":     field["type"].(string),
			"required": field["required"].(bool),
			"desc": func(desc interface{}) string {
				if desc != nil {
					return desc.(string)
				}
				return ""
			}(field["desc"]),
		}
		switch fieldInfo["type"].(string) {
		case "choice", "checkbox":
			fieldInfo["option"] = []gin.H{}
			for _, option := range field["option"].([]interface{}) {
				option := option.(string)
				fieldInfo["option"] = append(fieldInfo["option"].([]gin.H), gin.H{
					"label": option,
					"value": option,
				})
			}
		case "number", "string", "lg_string", "integer":
			fieldInfo["validate"] = func(validate interface{}) string {
				if validate != nil {
					return validate.(string)
				}
				return ""
			}(field["validate"])
		}
		newFormInfo["fieldList"] = append(newFormInfo["fieldList"].([]gin.H), fieldInfo)
	}
	if autoCloseCondition, success := newFormInfoParams["close_on"].(map[string]interface{}); success {
		if autoCloseCondition["datetime"] != nil {
			if datetime := autoCloseCondition["datetime"].([]interface{}); len(datetime) == 2 {
				var err error
				notBeforeDate, err := time.Parse(time.RFC3339, datetime[0].(string))
				if CheckErr(err) {
					notBeforeDate = time.Unix(0, 0)
				}
				notAfterDate, err := time.Parse(time.RFC3339, datetime[1].(string))
				if CheckErr(err) {
					notAfterDate = time.Now().AddDate(5, 0, 0)
				}
				CheckErr(err)
				newFormInfo["closeOn"].(gin.H)["datetime"] = []time.Time{notBeforeDate, notAfterDate}
			}
		}
		if autoCloseCondition["registered"] != nil {
			registeredLimit := autoCloseCondition["registered"].(float64)
			if registeredLimit != float64(int(registeredLimit)) {
				return nil, errors.New("'registered' is not integer")
			}
			newFormInfo["closeOn"].(gin.H)["registered"] = int(registeredLimit)
		}
	}
	return newFormInfo, nil
}
//string array to objectid array
func StringToOidList(IdList []string) []bson.ObjectId {
	var objId []bson.ObjectId
	for _, element := range IdList {
		if(bson.IsObjectIdHex(element)) {
			objId = append(objId,bson.ObjectIdHex(element))
		}
	}
	return objId
}
