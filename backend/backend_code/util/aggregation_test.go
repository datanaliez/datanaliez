package util

import (
	"reflect"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/influxdata/influxdb/pkg/testing/assert"
	"gopkg.in/mgo.v2/bson"
)

var mockDBStructure = map[string]string{
	"_id":       "ObjectId",
	"fullname":  "String",
	"skills":    "Array",
	"sex":       "Boolean",
	"birthdate": "Date",
	"gpax":      "NumberDecimal",
}

// Setup Test
func TestMain(m *testing.M) {
	m.Run()
}

// -------- Utility func Test -------- //

// -------- TypeConversion func Test -------- //
func TestTypeConversionForArrayDataType(t *testing.T) {
	assert.Equal(t, typeConversion("1287", "Array"), 1287)
	assert.Equal(t, typeConversion("1287er", "Array"), "1287er")
}
func TestTypeConversionForObjectIDDataType(t *testing.T) {
	assert.Equal(t, typeConversion("5aec91b624d60be1c6a01965", "ObjectId"), bson.ObjectIdHex("5aec91b624d60be1c6a01965"))
	assert.Equal(t, typeConversion("5aec91b624d60b", "ObjectId"), nil)
}
func TestTypeConversionForBooleanDataType(t *testing.T) {
	assert.Equal(t, typeConversion("true", "Boolean"), true)
	assert.Equal(t, typeConversion("false", "String"), false)
	assert.Equal(t, typeConversion("fuck", "Boolean"), "fuck")
}
func TestTypeConversionForDateDataType(t *testing.T) {
	assert.Equal(t, reflect.TypeOf(typeConversion("2018-05-04T16:17:44.841Z", "Date")).String(), "time.Time")
	assert.Equal(t, typeConversion("2018-05-04", "Date"), "2018-05-04")
}
func TestTypeConversionForStringDataType(t *testing.T) {
	assert.Equal(t, typeConversion("2018-05-04", "String"), "2018-05-04")
	p, _ := bson.ParseDecimal128("4")
	assert.Equal(t, typeConversion("04", "String"), p)
}
func TestTypeConversionForDecimalDataType(t *testing.T) {
	assert.Equal(t, reflect.TypeOf(typeConversion("365", "NumberDecimal")).String(), "bson.Decimal128")
	assert.Equal(t, reflect.TypeOf(typeConversion("365", "String")).String(), "bson.Decimal128")
	assert.Equal(t, typeConversion("365fg", "NumberDecimal"), "365fg")
}

// -------- Aggregration framework Test -------- //

// ----- Preparation Test ----- //

func TestPrepareDataFilter(t *testing.T) {
	// should be re-design test case
	mockAggregrator := NewAggregator(mockDBStructure)
	mockAggregrator.SetFilter(
		[]gin.H{
			gin.H{"column_name": "_id", "expression": "gt", "value": "5aec91b624d60be1c6a01965"},
			gin.H{"column_name": "_id", "expression": "lt", "value": "5aec91b624d60be1c6b01965"},
			gin.H{"column_name": "fullname", "expression": "gt", "value": "Tanakorn"},
			gin.H{"column_name": "gpax", "expression": "ge", "value": "2.5"},
			gin.H{"column_name": "birthdate", "expression": "eq", "value": "2000-10-16T10:27:44.841Z"},
		},
	)
	assert.Equal(t, len(mockAggregrator.FirstProjection), 1)
	assert.Equal(t, len(mockAggregrator.Filter), 5)
	assert.Equal(t, mockAggregrator.Orderby, 1)
	assert.Equal(t, len(mockAggregrator.LastProjection), 1)
	assert.Equal(t, len(mockAggregrator.Grouper), 0)
	assert.Equal(t, len(mockAggregrator.Limit), 0)
}
func TestPrepareDataSelectColumn(t *testing.T) {
	mockAggregrator := NewAggregator(mockDBStructure)
	mockAggregrator.SelectColumn([]string{"_id", "fullname", "gpax"})
	assert.Equal(t, len(mockAggregrator.LastProjection), 3)
	assert.Equal(t, mockAggregrator.Orderby, 1)
	assert.Equal(t, len(mockAggregrator.Grouper), 0)
	assert.Equal(t, len(mockAggregrator.Limit), 0)
}
func TestPrepareDataSorting(t *testing.T) {
	const sortKey = "6731940c49a028ca776b834221b2de90ebacd2721d37d3f3f37e75919c645abc"
	mockAggregrator := NewAggregator(mockDBStructure)
	mockAggregrator.Sorting("fullname", "descend")
	assert.Equal(t, len(mockAggregrator.FirstProjection["$addFields"].(bson.M)), 1)
	assert.Equal(t, mockAggregrator.Orderby, -1)
	assert.Equal(t, len(mockAggregrator.Grouper), 0)
	assert.Equal(t, len(mockAggregrator.Limit), 0)
	mockAggregrator.Sorting("fullname", "ascend")
	assert.Equal(t, len(mockAggregrator.FirstProjection["$addFields"].(bson.M)), 1)
	assert.Equal(t, mockAggregrator.Orderby, 1)
	assert.Equal(t, len(mockAggregrator.Grouper), 0)
	assert.Equal(t, len(mockAggregrator.Limit), 0)
}
func TestPrepareDataLimit(t *testing.T) {
	mockAggregrator := NewAggregator(mockDBStructure)
	mockAggregrator.SetLimit(0, 20)
	assert.Equal(t, len(mockAggregrator.FirstProjection), 1)
	assert.Equal(t, len(mockAggregrator.Filter), 0)
	assert.Equal(t, mockAggregrator.Orderby, 1)
	assert.Equal(t, len(mockAggregrator.LastProjection), 1)
	assert.Equal(t, len(mockAggregrator.Grouper), 0)
	assert.Equal(t, mockAggregrator.Limit[0]["$skip"], 0)
	assert.Equal(t, mockAggregrator.Limit[1]["$limit"], 20)
}
func TestPrepareDataHistoGroup(t *testing.T) {
	mockAggregrator := NewAggregator(mockDBStructure)
	mockAggregrator.Histogroup("$gpax", "$gpax")
	assert.Equal(t, mockAggregrator.Grouper[0]["$unwind"], "$gpax")
	assert.Equal(t, mockAggregrator.Grouper[1]["$group"].(bson.M)["_id"].(string), "$gpax")
	assert.Equal(t, mockAggregrator.Grouper[1]["$group"].(bson.M)["count"].(bson.M)["$sum"], 1)
}

// ----- Compilation Test ----- //
func TestCompileDataModule(t *testing.T) {
	mockAggregrator := NewAggregator(mockDBStructure)
	mockAggregrator.Filter = []bson.M{bson.M{"mock1": nil}, bson.M{"mock2": nil}, bson.M{"mock3": nil}}
	assert.Equal(t, len(mockAggregrator.Finalize("data")), 4)
	mockAggregrator.Limit = []bson.M{bson.M{"$skip": 0}, bson.M{"$limit": 20}}
	assert.Equal(t, len(mockAggregrator.Finalize("data")), 6)
}
func TestCompileDataCountModule(t *testing.T) {
	mockAggregrator := NewAggregator(mockDBStructure)
	mockAggregrator.Filter = []bson.M{bson.M{"mock1": nil}, bson.M{"mock2": nil}, bson.M{"mock3": nil}}
	assert.Equal(t, len(mockAggregrator.Finalize("dataCount")), 3)
}
func TestCompileHistogramModule(t *testing.T) {
	mockAggregrator := NewAggregator(mockDBStructure)
	mockAggregrator.Filter = []bson.M{bson.M{"mock1": nil}, bson.M{"mock2": nil}, bson.M{"mock3": nil}}
	mockAggregrator.Grouper = []bson.M{
		bson.M{"$unwind": "$columnName"},
		bson.M{
			"$group": bson.M{
				"_id": "$columnName",
				"count": bson.M{
					"$sum": 1,
				},
			},
		},
	}
	assert.Equal(t, len(mockAggregrator.Finalize("histogram")), 5)
}
func TestCompileDeleteDataModule(t *testing.T) {
	mockAggregrator := NewAggregator(mockDBStructure)
	mockAggregrator.Filter = []bson.M{bson.M{"mock1": nil}, bson.M{"mock2": nil}, bson.M{"mock3": nil}}
	assert.Equal(t, len(mockAggregrator.Finalize("deleteData")), 2)
}
