package login

import (
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
)
func Logout(c *gin.Context)  {
	session := sessions.Default(c)
	session.Clear()
	session.Save()
	c.JSON(200, gin.H{
		"logout":"OK",
	})
}
