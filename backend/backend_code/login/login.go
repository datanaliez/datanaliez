package login

import (
	"database/sql"

	"../util"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
	"gopkg.in/gorp.v1"
)

var (
	Dbmap *gorp.DbMap
	db    *sql.DB
)

func Init() {
	Dbmap, db = util.InitDb()
}
func CheckLogin(c *gin.Context) {
	Init()
	session := sessions.Default(c)
	params := util.RequestFormatter(c.GetRawData())
	rows, err := db.Query("SELECT * FROM user WHERE username=?", params["username"])
	util.CheckErr(err)
	result := util.ConvertRawBytesToMap(rows)
	permission := "-1"
	username := ""
	if len(result) > 0 {
		if util.SHA256(result[0]["password"]+session.Get("challenge").(string)) == params["password"] {
			permission = result[0]["permission"]
			username = result[0]["username"]
			session.Set("username", result[0]["username"])
			session.Set("permission", result[0]["permission"])
			session.Save()
		}
	}
	c.JSON(200, gin.H{
		"permission": permission,
		"username":   username,
	})
	db.Close()
}
func GenerateChallenge(c *gin.Context) {
	session := sessions.Default(c)
	challenge := util.RandomString(15)
	session.Set("challenge", challenge)
	session.Save()
	c.JSON(200, gin.H{"challenge": challenge})
}

func LoginStatus(c *gin.Context) {
	session := sessions.Default(c)
	c.JSON(200, gin.H{
		"permission": session.Get("permission"),
		"username":   session.Get("username"),
	})
}
