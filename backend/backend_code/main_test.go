package main

import (
	"testing"

	"./util"
)

var TestSessionCookie = ""

const testUser = "intatester"
const testPassword = "sadMonkeyLikePanda"

func TestMain(m *testing.M) {
	_, db := util.InitDb()
	db.Query("INSERT INTO `user` (`username`, `password`, `permission`) VALUES (?,?, 2)", testUser, testPassword)
	m.Run()
	// httptest.NewRecorder().Body.
}
